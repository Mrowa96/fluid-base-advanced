<?php
    $config = [
        'id' => 'fs-console',
        'basePath' => dirname(__DIR__),
        'bootstrap' => ['log', 'gii'],
        'controllerNamespace' => 'console\controllers',
        'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
        'components' => [
            'db' => require_once dirname(dirname(__DIR__)).'/common/config/db.php',
            'cache' => [
                'class' => 'yii\caching\ApcCache',
            ],
            'log' => [
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
            'i18n' => [
                'translations' => [
                    '*' => [
                        'class' => 'backend\utilities\DbMessageSource',
                        'forceTranslation' => true,
                    ],
                ],
            ],
        ],
    ];

    if (!YII_ENV_TEST) {
        $config['bootstrap'][] = 'gii';
        $config['modules']['gii'] = [
            'class' => 'yii\gii\Module',
            'generators' => [
                'crud' => [
                    'class' => 'yii\gii\generators\crud\Generator',
                    'templates' => [
                        'customCrud' => '@common/utilities/crud/default',
                    ]
                ],
                'model' => [
                    'class' => 'yii\gii\generators\model\Generator',
                    'templates' => [
                        'customModel' => '@common/utilities/model/default',
                    ]
                ]
            ],
        ];
    }

    return $config;
