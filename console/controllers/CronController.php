<?php
    namespace console\controllers;

    use common\utilities\Mailer;
    use common\utilities\Utility;
    use Yii;
    use yii\base\Exception;
    use yii\helpers\Json;
    use yii\console\Controller;
    use backend\models\CronQueue;
    use common\models\Settings;
    use backend\utilities\Allegro;
    use common\models\Settings\Smtp;
    use Swift_Mailer;
    use Swift_Message;
    use Swift_SmtpTransport;
    use common\utilities\ElasticSearch;
    use backend\models\OrderInvoice;
    use common\models\Order;
    use common\models\User;
    use common\models\MailSubscriber;
    use backend\models\Mail;
    use backend\models\MailMessage;
    use backend\models\MailSubscriberData;

    class CronController extends Controller
    {
        public function actionIndex()
        {
            system("clear");
            echo "Fluid Base Advanced Cron Service" . PHP_EOL;
            echo "________________________________" . PHP_EOL;

            $class = new \ReflectionClass($this);
            $index = 1;

            foreach ($class->getMethods(\ReflectionMethod::IS_PUBLIC) AS $method){
                if ($method->class == $class->getName()){
                    $methodName = Utility::uppercaseToDash(substr($method->name, 6));

                    if($methodName !== "index"){
                        echo "[" .$index++. "] " . $methodName . PHP_EOL;
                    }
                }
            }

            exit;
        }

        public function actionFlushCache()
        {
            Yii::$app->cache->flush();

            echo "Cache was successfully flushed" . PHP_EOL;

            exit;
        }

        public function actionSendNewsletter()
        {
            /**
             * @var CronQueue $newsletters
             * @var Mail $mailModel
             * @var MailMessage $message
             * @var MailSubscriber $subscriber
             * @var Mailer $mailer
             */
            $newsletters = CronQueue::find()
                ->where(['type' => 'newsletter-request'])
                ->andWhere(['status' => 0])
                ->all();

            if(!empty($newsletters)){
                $mailer = Yii::$app->mailer;

                foreach($newsletters AS $newsletter){
                    $data = Json::decode($newsletter->data, false);

                    if(isset($data->id)){
                        $mailModel = Mail::findOne($data->id);
                        $subscribersId = MailSubscriberData::find()->where(['mail_id' => $mailModel->id])->all();
                        $message = MailMessage::findOne($mailModel->mail_message_id);

                        if($message){
                            $message->prepareForSent();

                            foreach($subscribersId AS $subscriberId){
                                $subscriber = MailSubscriber::findOne($subscriberId);

                                $mailer->compose()
                                    ->setFrom($mailer->getHost())
                                    ->setTo($subscriber->email)
                                    ->setSubject($message->title)
                                    ->setHtmlBody($message->content)
                                    ->send();
                            }
                        }
                    }

                    $newsletter->status = 1;
                    $newsletter->save();
                }

                echo "All newsletters was send successfully.";
            }
            else{
                echo "No new newsletters to send";
            }
        }


        public function actionCreateProductsIndexes()
        {
            try{
                $es = new ElasticSearch();

                $es->indexSomething();

                print_r("Indexes created");
            }
            catch(\Exception $ex){
                print_r($ex);
            }

            exit;
        }
    }