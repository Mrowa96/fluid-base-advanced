<?php
    define("DS", DIRECTORY_SEPARATOR);

    Yii::setAlias('@common', dirname(__DIR__));
    Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
    Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
    Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

    Yii::setAlias('@data', Yii::getAlias("@frontend")."/web/data");
    Yii::setAlias('@backendData', Yii::getAlias("@backend")."/var/data");

    Yii::setAlias('@dataAvatars', Yii::getAlias("@data")."/avatars");
    Yii::setAlias('@dataTemp', Yii::getAlias("@backendData")."/temp");

    Yii::setAlias('domainLink', ".{{domainLink}}");
    Yii::setAlias('frontendLink', "http://{{domainLink}}");
    Yii::setAlias('backendLink', "http://{{manageDomainLink}}");
    Yii::setAlias('dataLink', Yii::getAlias("@frontendLink")."/data");