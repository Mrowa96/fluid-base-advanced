<?php
    return [
        'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
        'components' => [
            'db' => require_once 'db.php',
            'cache' => [
                'class' => 'yii\caching\ApcCache',
            ],
            'session' => [
                'class' => 'yii\web\Session',
                'cookieParams' => [
                    'path' => '/',
                    'domain' => Yii::getAlias("@domainLink")
                ],
            ],
            'user' => [
                'identityClass' => 'common\models\User',
                'enableAutoLogin' => true,
                'identityCookie' => [
                    'name' => '{{cookieName}}',
                    'path' => '/',
                    'domain' => Yii::getAlias("@domainLink"),
                ],
            ],
            'authManager' => [
                'class' => 'yii\rbac\DbManager',
            ],
            'request' => [
                'cookieValidationKey' => '{{cookieValidationKey}}',
            ],
            'mailer' => [
                'class' => 'common\utilities\Mailer',
            ],
            'smsGate' => [
                'class' => 'common\utilities\SmsGate',
            ],
        ],
    ];
