<?php
    class Initializer
    {
        protected $files;
        protected $mode;
        protected $extensions;
        
        public function __construct($argv)
        {
            $this->mode = (isset($argv[1])) ? trim(strtolower($argv[1])) : 'prod';
            $this->extensions = ['gd', 'imagick', 'pdo', 'pdo_mysql', 'mbstring', 'curl', 'zip', 'apc','soap'];
            
            $this->clear();
            $this->checkRequirements();
        }

        public function start()
        {
            $this->setMode();
            $this->loadMenu();
        }

        protected function loadMenu()
        {
            $this->clear();

            echo "[1] Skonfiguruj bazę danych\n";
            echo "[2] Skonfiguruj domenę\n";
            echo "[3] Ustaw środowisko\n";
            echo "[0] Wyjdź\n";
            echo "\nOpcja: ";

            $option = fgets(STDIN);

            switch($option){
                case 1:
                    $result = $this->setUpDatabase();
                    break;
                case 2:
                    $result = $this->setUpDomain();
                    break;
                case 3:
                    $result = $this->setUpGeneral();
                    break;
                    break;
                case 0:
                    $this->clear();
                    echo "\nBye bye!\n";
                    exit;
                    break;
                default:
                    $this->loadMenu();
                    break;
            }

            $this->clear();
            if(isset($result) && $result){
                echo "\nOperacja wykonana została pomyślnie.\n";

                sleep(3);

                $this->loadMenu();
            }
            else{
                echo "\nOperacja nie została wykonana. Spróbuj jeszcze raz (wywyłując polecenie php init) lub skontaktuj się z administratorem.\n";
            }
        }
        protected function setMode()
        {
            switch($this->mode){
                case "dev":
                    ini_set('display_errors', 1);
                    ini_set('display_startup_errors', 1);
                    error_reporting(E_ALL);
                    break;
                case "prod":
                    ini_set('display_errors', 0);
                    ini_set('display_startup_errors', 0);
                    break;
            }
        }
        protected function clear()
        {
            system('clear');

            echo "Fluid Shop Initialization Service" . PHP_EOL;
            echo "_________________________________" . PHP_EOL;
        }

        private function checkRequirements()
        {
            foreach ($this->extensions AS $ext){
                if(!extension_loaded($ext)){
                    echo "Środowisko nie jest przygotowane. Brakuje rozszerzenia: " . $ext ."\n";
                    exit;
                }
            }

            return true;
        }
        private function setUpDomain()
        {
            $this->clear();
            echo "Konfiguracja domeny\n";
            $examplePath = __DIR__ . '/config/bootstrap.php';
            $path = ROOT_DIR.'/common/config/bootstrap.php';

            if(file_exists($examplePath)){
                $content = file_get_contents($examplePath);

                echo "-> Wprowadź domene w formacie xxxx.xx (frontend): ";
                $frontend = fgets(STDIN);

                echo "-> Wprowadź domene w formacie xxx.xxxx.xx (backend): ";
                $backend = fgets(STDIN);

                $content = str_replace("{{domainLink}}", trim($frontend), $content);
                $content = str_replace("{{manageDomainLink}}", trim($backend), $content);

                return file_put_contents($path, $content);
            }
            else{
                return false;
            }
        }
        private function setUpGeneral()
        {
            $this->clear();
            $examplePath = __DIR__ . '/config/main.php';
            $path = ROOT_DIR.'/common/config/main.php';

            if(file_exists($examplePath)){
                $content = file_get_contents($examplePath);

                $bytes = openssl_random_pseudo_bytes(32);
                $key = strtr(substr(base64_encode($bytes), 0, 32), '+/=', '_-.');

                $content = str_replace("{{cookieValidationKey}}", $key, $content);
                $content = str_replace("{{cookieName}}", bin2hex(openssl_random_pseudo_bytes(8)), $content);

                return file_put_contents($path, $content);
            }
            else{
                return false;
            }
        }
        private function setUpDatabase()
        {
            $this->clear();
            echo "Konfiguracja bazy danych\n";
            $examplePath = __DIR__ . '/config/db.php';
            $path = ROOT_DIR.'/common/config/db.php';
            $dumpPath = ROOT_DIR.'/extras/dump.sql';

            if(file_exists($examplePath)){
                $content = file_get_contents($examplePath);

                echo "-> Wprowadź nazwę/adres ip hosta: ";
                $host = trim(fgets(STDIN));

                echo "-> Wprowadź nazwę bazy danych: ";
                $dbName = trim(fgets(STDIN));

                echo "-> Wprowadź nazwę użytkownika: ";
                $username = trim(fgets(STDIN));

                echo "-> Wprowadź hasło: ";
                $password = trim(fgets(STDIN));

                $content = str_replace("{{host}}", $host, $content);
                $content = str_replace("{{dbname}}", $dbName, $content);
                $content = str_replace("{{username}}", $username, $content);
                $content = str_replace("{{password}}", $password, $content);

                try {
                    $connection = new PDO("mysql:host=" . $host . ";dbname=" . $dbName, $username, $password);
                    $connection->query(file_get_contents($dumpPath));

                    return file_put_contents($path, $content);
                }
                catch (\Exception $e) {
                    echo $e->getMessage();

                    return false;
                }
            }
            else{
                return false;
            }
        }
    }