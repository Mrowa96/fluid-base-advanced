<?php
    namespace common\models\Settings;

    use common\models\Settings;
    use Yii;

    class Smtp extends Settings
    {
        public $host;
        public $login;
        public $password;
        public $port;
        public $encryption;

        const MODULE = 'SMTP';

        public function rules()
        {
            return [
                [['host', 'login', 'password', 'port', 'encryption'], 'string']
            ];
        }

        public function attributeLabels()
        {
            return [
                'host' => Yii::t("settings", "Smtp host"),
                'port' => Yii::t("settings", "Smtp port"),
                'login' => Yii::t("settings", "Smtp login"),
                'password' => Yii::t("settings", "Smtp password"),
                'encryption' => Yii::t("settings", "Smtp encryption"),
            ];
        }
    }