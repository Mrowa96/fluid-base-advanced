<?php
    namespace common\models\Settings;

    use common\models\Settings;
    use Yii;

    class Information extends Settings
    {
        public $name;
        public $email;
        public $phone;
        public $website;
        public $street;
        public $city;
        public $zip;

        const MODULE = 'INFORMATION';

        public function rules()
        {
            return [
                [['name','email','phone','street','city','zip','website'], 'string'],
                ['email', 'email'],
            ];
        }

        public function attributeLabels()
        {
            return [
                'name' => Yii::t("settings", "Name"),
                'email' => Yii::t("settings", "Email"),
                'phone' => Yii::t("settings", "Phone"),
                'website' => Yii::t("settings", "Website"),
                'street' => Yii::t("settings", "Street and number"),
                'city' => Yii::t("settings", "City"),
                'zip' => Yii::t("settings", "Zipcode"),
            ];
        }
    }