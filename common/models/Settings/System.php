<?php
    namespace common\models\Settings;

    use common\models\Settings;
    use Yii;

    class System extends Settings
    {
        public $theme;
        public $site_title;
        public $file_manager;
        public $track_level;
        public $currency;
        public $footer_text;
        public $language;
        public $language_level;

        const MODULE = 'SYSTEM';

        public function rules()
        {
            return [
                [['site_title', 'theme', 'currency', 'footer_text', 'language', 'language_level'], 'string'],
                ['file_manager', 'boolean']
            ];
        }

        public function attributeLabels()
        {
            return [
                'site_title' => Yii::t("settings", "Default title"),
                'footer_text' => Yii::t("settings", "Footer text"),
                'file_manager' => Yii::t("settings", "File manager"),
                'language' => Yii::t("settings", "Language"),
                'language_level' => Yii::t("settings", "Language level"),
                'theme' => Yii::t("settings", "Theme"),
                'currency' => Yii::t("settings", "Currency"),
            ];
        }

        public static function getLanguageLevel()
        {
            return [
                'cookie' => Yii::t("settings", "Cookie"),
                'user' => Yii::t("settings", "User"),
                'global' => Yii::t("settings", "Global"),
            ];
        }
    }