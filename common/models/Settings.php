<?php
    namespace common\models;

    use fluid\fileManager\File;
    use ReflectionObject;
    use ReflectionProperty;
    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\Json;

    /**
     * @property int $id
     * @property string $key
     * @property string $value
     * @property string $default
     * @property string $description
     * @property string $module
     */
    class Settings extends ActiveRecord
    {
        private $modules = [];

        public function loadData($data)
        {
            foreach($data AS $key => $value){
                $className = "common\\models\\Settings\\" . $key;

                if(class_exists($className)){
                    $model = new $className();

                    if(method_exists($model, "load")){
                        $model->load($data);

                        $this->modules[] = $model;
                    }
                }
            }

            return true;
        }
        public function saveData()
        {
            foreach($this->modules AS $model){
                $model->saveKeys();
            }

            return true;
        }
        public function saveKeys()
        {
            $module = new ReflectionObject($this);
            $properties = $module->getProperties(ReflectionProperty::IS_PUBLIC);
            $moduleName = mb_strtoupper($module->getShortName());

            if (!empty($properties)) {
                foreach($properties AS $prop){
                    $option = Settings::find()->where(['module' => $moduleName])->andWhere(['key' => $prop->name])->one();

                    if($option){
                        if(empty($this->{$prop->name})){
                            $this->{$prop->name} = $option->default;
                        }

                        $option->value = $this->{$prop->name};

                        $option->save();
                    }
                }
            }

            return true;
        }
        public function prepareForForm()
        {
            $module = new ReflectionObject($this);
            $moduleName = mb_strtoupper($module->getShortName());
            /** @var Settings $settings */
            $settings = Settings::find()->where(['module' => $moduleName])->all();

            if(!empty($settings)){
                foreach($settings AS $setting){
                    if(property_exists($this, $setting->key)){
                        $this->{$setting->key} = $setting->value;
                    }
                }
            }

            return $this;
        }

        public static function tableName()
        {
            return '{{%settings}}';
        }
        public static function getOne($key, $module, $onlyValue = true){
            $option = Settings::findOne(['key' => $key, 'module' => $module]);

            if($option){
                if($onlyValue){
                    return trim($option->value);
                }
                else{
                    return $option;
                }
            }

            return false;
        }
        public static function getThemes(){
            $themes = new File(Yii::getAlias("@backend") . '/web/styles/themes.json');
            $result = [];

            if($themes->exists){
                $themesParsed = Json::decode($themes->read(), false);

                if($themesParsed){
                    foreach($themesParsed->themes AS $theme){
                        $result[$theme->class] = Yii::t("theme", $theme->name);
                    }
                }
            }

            return $result;
        }
    }
