<?php
    namespace common\models;

    use Yii;
    use yii\base\NotSupportedException;
    use yii\behaviors\TimestampBehavior;
    use yii\db\ActiveRecord;
    use yii\web\IdentityInterface;
    use fluid\fileManager\Directory;

    /**
     * This is the model class for table "{{%user}}".
     *
     * @property string username
     * @property string name
     * @property string email
     * @property string avatar
     * @property bool|string created_at
     * @property string role
     * @property string roleName
     * @property mixed id
     * @property mixed auth_key
     * @property string password_reset_token
     * @property string language
     * @property string password_hash
     * @property bool isAdmin
     * @property bool deletable
     */

    class User extends ActiveRecord implements IdentityInterface{
        const STATUS_DELETED = 0;
        const STATUS_ACTIVE = 10;

        public $password;
        public $password_repeat;
        public $role;
        public $roleName;
        public $isAdmin;

        public static function tableName(){
            return '{{%user}}';
        }

        public function behaviors(){
            return [
                TimestampBehavior::className(),
            ];
        }
        public function rules(){
            return [
                ['status', 'default', 'value' => self::STATUS_ACTIVE],
                ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
                ['deletable', 'default', 'value' => 1]
            ];
        }
        public function attributes(){
            return array_merge(
                parent::attributes(),
                ['role', 'roleName']
            );
        }
        public function attributeLabels(){
            return [
                'username' => Yii::t("user", "Username"),
                'name' => Yii::t("user", "Name"),
                'email' => Yii::t("user", "Adres email"),
                'password' => Yii::t("user", "Password"),
                'password_repeat' => Yii::t("user", "Password repeat"),
                'language' => Yii::t("user", "Language"),
                'role' => Yii::t("user", "Role"),
                'roleName' => Yii::t("user", "Rolename"),
                'created_at' => Yii::t("user", "Created")
            ];
        }

        public function beforeSave($insert)
        {
            if(!$this->avatar){
                $this->avatar = User::getRandomAvatar();
            }
            else{
                if(strpos($this->avatar, Yii::getAlias("@dataLink") . '/') !== false){
                    $this->avatar = str_replace(Yii::getAlias("@dataLink") . '/', '', $this->avatar);
                }
            }

            if(!$this->name){
                $this->name = ucfirst($this->username);
            }

            return parent::beforeSave($insert);
        }
        public function afterSave($insert, $changedAttributes){
            unset($this->role);
            unset($this->roleName);

            parent::afterSave($insert, $changedAttributes);
        }
        public function afterFind(){
            $auth = Yii::$app->authManager;
            $roles = $auth->getRolesByUser($this->id);
            reset($roles);

            $this->created_at = date("M, Y", $this->created_at);
            $this->role = $roles[key($roles)]->name;
            $this->roleName = $roles[key($roles)]->description;
            $this->avatar = Yii::getAlias("@dataLink") . '/' . $this->avatar;

            if(isset($roles['admin'])){
                $this->isAdmin = true;
            }

            parent::afterFind();
        }
        public function getGridViewExample(){
            return [
                [
                    'property' => 'avatar',
                    'type' => 'image',
                    'value' => 1,
                ],
                [
                    'property' => 'username',
                ],
                [
                    'property' => 'name',
                    'value' => 1,
                ],
                [
                    'property' => 'email',
                ],
                [
                    'property' => 'roleName',
                ],
                [
                    'property' => 'created_at',
                ]
            ];
        }

        public static function findIdentity($id){
            return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
        }
        public static function findIdentityByAccessToken($token, $type = null){
            throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
        }
        public static function findByUsername($username){
            return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
        }
        public static function findByPasswordResetToken($token){
            if (!static::isPasswordResetTokenValid($token)) {
                return null;
            }

            return static::findOne([
                'password_reset_token' => $token,
                'status' => self::STATUS_ACTIVE,
            ]);
        }
        public static function isPasswordResetTokenValid($token){
            if (empty($token)) {
                return false;
            }

            $timestamp = (int) substr($token, strrpos($token, '_') + 1);
            $expire = Yii::$app->params['user.passwordResetTokenExpire'];

            return $timestamp + $expire >= time();
        }

        public function getId(){
            return $this->getPrimaryKey();
        }
        public function getAuthKey(){
            return $this->auth_key;
        }
        public function generateAuthKey(){
            $this->auth_key = Yii::$app->security->generateRandomString();
        }
        public function generatePasswordResetToken(){
            $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
        }
        public function hashPassword($password){
            return Yii::$app->security->generatePasswordHash($password);
        }

        public function validateAuthKey($authKey){
            return $this->getAuthKey() === $authKey;
        }
        public function validatePassword($password){
            return Yii::$app->security->validatePassword($password, $this->password_hash);
        }
        public function setPassword($password){
            $this->password_hash = $this->hashPassword($password);
        }
        public function removePasswordResetToken(){
            $this->password_reset_token = null;
        }

        public static function getAvatars(){
            $dir = new Directory(Yii::getAlias("@dataAvatars"));
            $result = [];

            if($dir->exists){
                $avatars = $dir->objects;

                foreach ($avatars AS $avatar){
                    if(strpos($avatar, ".thumb") === false){
                        $avatar = 'avatars/' . $avatar;

                        $result[] = $avatar;
                    }
                }
            }

            return $result;
        }
        public static function getRandomAvatar()
        {
            $avatars = User::getAvatars();
            $index = mt_rand(0, (count($avatars) - 1));

            if(isset($avatars[$index])){
                return $avatars[$index];
            }
            else{
                return User::getRandomAvatar();
            }
        }
    }
