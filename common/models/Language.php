<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\ArrayHelper;
    use backend\models\LanguageSource as LanguageSourceBackend;
    use frontend\models\LanguageSource as LanguageSourceFrontend;

    /**
     * This is the model class for table "{{%i18n}}".
     *
     * @property integer $id
     * @property string $name
     * @property string $symbol
     * @property string $module
     */
    class Language extends ActiveRecord
    {
        protected $newLanguage;

        public static function tableName()
        {
            return '{{%i18n}}';
        }

        public function rules()
        {
            return [
                [['name', 'symbol'], 'string'],
                [['name'], 'string', 'max' => 64],
                [['symbol'], 'string', 'max' => 10],
                [['module'], 'string', 'max' => 64],
            ];
        }
        public function attributeLabels()
        {
            return [
                'id' => Yii::t('language', 'ID'),
                'name' => Yii::t('language', 'Name'),
                'symbol' => Yii::t('language', 'Symbol'),
                'module' => Yii::t('language', 'Module'),
            ];
        }

        public function save($runValidation = true, $attributeNames = null)
        {
            $this->newLanguage = $this->getIsNewRecord();

            return parent::save($runValidation, $attributeNames);
        }
        public function afterSave($insert, $changedAttributes)
        {
            if($this->newLanguage){
                switch($this->module){
                    case "frontend":
                        $sources = LanguageSourceFrontend::find()->all();
                        break;

                    case "backend":
                    default:
                        $sources = LanguageSourceBackend::find()->all();
                        break;
                }

                /** @var LanguageSourceFrontend | LanguageSourceBackend $source */
                foreach($sources AS $source){
                    $message = new LanguageMessage();

                    $message->i18n_id = $this->id;
                    $message->source_id = $source->id;
                    $message->translation = $source->message;

                    $message->save();
                }
            }

            parent::afterSave($insert, $changedAttributes);
        }

        public function delete()
        {
            LanguageMessage::deleteAll(['i18n_id' => $this->id]);

            return parent::delete();
        }

        public static function groupByCategories($module = "backend")
        {
            switch($module){
                case "frontend":
                    $sources = LanguageSourceFrontend::find()->all();
                    break;

                case "backend":
                default:
                    $sources = LanguageSourceBackend::find()->all();
                    break;
            }

            $result = [];

            /** @var LanguageSourceFrontend | LanguageSourceBackend $source */
            foreach($sources AS $source){
                $translations = LanguageMessage::find()->where(['source_id' => $source->id])->all();

                /** @var LanguageMessage $translation */
                foreach($translations AS $translation){
                    $result[$source->category][$source->message][$translation->i18n_id] = $translation->translation;
                }
            }

            return $result;
        }
        public static function prepareForManage($id)
        {
            $language = Language::findOne($id);
            $data = [];

            switch($language->module){
                case "frontend":
                    $sources = LanguageSourceFrontend::find()->orderBy('category')->all();
                    break;
                case "backend":
                default:
                    $sources = LanguageSourceBackend::find()->orderBy('category')->all();
                    break;
            }


            /** @var LanguageSourceFrontend | LanguageSourceBackend $source */
            foreach($sources AS $source){
                /** @var LanguageMessage $message */
                $message = LanguageMessage::find()->where(['i18n_id' => $language->id, 'source_id' => $source->id])->one();

                if($message){
                    $data[$source->category][] = [
                        'id' => $source->id,
                        'source' => $source->message,
                        'message' => $message->translation,
                    ];
                }
                else{
                    $data[$source->category][] = [
                        'id' => $source->id,
                        'source' => $source->message,
                        'message' => $source->message,
                    ];
                }
            }

            return $data;
        }
        public static function prepareForForm($module = "backend")
        {
            return ArrayHelper::map(self::find()->where(['module' => $module])->all(), 'symbol', 'name');
        }
        public static function saveMessages($id)
        {
            $data = Yii::$app->request->post();
            $transaction = LanguageMessage::getDb()->beginTransaction();

            try{
                foreach($data AS $key => $value){
                    if((int) $key){
                        /** @var LanguageMessage $message */
                        $message = LanguageMessage::find()->where(['source_id' => $key, 'i18n_id' => $id])->one();

                        if($message){
                            $message->translation = $value;
                            $message->save();
                        }
                    }
                }

                $transaction->commit();

                return true;
            }
            catch(\Exception $ex){
                $transaction->rollBack();
            }

            return false;
        }
    }