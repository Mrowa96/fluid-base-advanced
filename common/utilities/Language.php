<?php
    namespace common\utilities;

    use Yii;
    use yii\web\Cookie;
    use common\models\Settings;
    use common\models\Settings\System;
    use common\models\User;

    class Language
    {
        protected $level;

        public function __construct()
        {
            $this->level = Settings::getOne('language_level', System::MODULE);
        }

        public function manage()
        {
            switch($this->level){
                case "global":
                    $this->manageGlobalLevel();
                    break;
                case "cookie":
                    $this->manageCookieLevel();
                    break;
                case "user":
                    $this->manageUserLevel();
                    break;
            }

            /* This is needed for javascript translation, which cannot read secure Yii2 cookies*/
            if(!isset($_COOKIE['language']) || $_COOKIE['language'] !== Yii::$app->language){
                setcookie("language", Yii::$app->language);
            }
        }
        public function setCookieLanguage($symbol)
        {
            Yii::$app->response->cookies->add(new Cookie([
                'name' => 'language_secure',
                'value' => $symbol,
            ]));
        }
        public function setUserLanguage(User $user, $symbol)
        {
            $user->language = $symbol;
            $user->save();
        }

        protected function manageGlobalLevel()
        {
            Yii::$app->language = Settings::getOne('language', System::MODULE);
        }
        protected function manageUserLevel()
        {
            /** @var User $user */
            if(!Yii::$app->user->isGuest){
                $user = Yii::$app->user->getIdentity();

                if(!$user->language){
                    $user->language = Yii::$app->language;
                    $user->save();
                }

                Yii::$app->language = $user->language;
            }
        }
        protected function manageCookieLevel()
        {
            $cookies = Yii::$app->request->cookies;

            if(!$cookies->has('language_secure')){
                Yii::$app->response->cookies->add(new Cookie([
                    'name' => 'language_secure',
                    'value' => Yii::$app->language,
                ]));
            }

            Yii::$app->language = $cookies->getValue('language_secure');
        }
    }