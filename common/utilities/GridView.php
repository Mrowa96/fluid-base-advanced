<?php
    namespace common\utilities;

    use yii\helpers\Json;

    class GridView extends \yii\grid\GridView
    {
        public $buttons;
        public static $configuration = [];

        public function run()
        {
            $modelName = $this->dataProvider->query->modelClass;

            $id = $this->options['id'];
            $this->options['data-model'] = $modelName;
            $options = Json::htmlEncode($this->getClientOptions());
            $view = $this->getView();
            GridViewAsset::register($view);

            $view->registerJs("jQuery('#$id').yiiGridView($options);");

            $gridView = get_parent_class();
            $baseListView = get_parent_class($gridView);

            $baseListView::run();
        }

        public static function widget($config = [])
        {
            if(isset($config['configuration'])){
                self::$configuration = $config['configuration'];
                unset($config['configuration']);
            }

            if(isset($config['buttons'])){
                $config['columns'] = self::joinColumns($config['columns'], $config['buttons']);
                unset($config['buttons']);
            }

            return parent::widget($config);
        }

        private static function joinColumns($columns, $buttons)
        {
            if(!in_array('noSerialColumn', self::$configuration)){
                array_unshift($columns, ['class' => 'yii\grid\SerialColumn']);
            }

            $keys = array_keys($buttons);
            $template = "";

            foreach($keys AS $key){
                $template .= "{" . $key. "} ";
            }
            $template = substr($template, 0, strlen($template) - 1);

            $columns[] = [
                'class' => 'yii\grid\ActionColumn',
                'template' => $template,
                'buttons' => $buttons
            ];

            return $columns;
        }
    }