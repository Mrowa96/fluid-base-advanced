<?php
    namespace common\utilities;

    class Utility
    {
        public static function createUrl($string)
        {
            if(is_string($string)){
                $string = trim(mb_strtolower($string));
                $newString = self::handleWithPolishChars($string);

                return $newString;
            }
            else{
                throw new \Exception("Parameter is not a string.");
            }
        }

        public static function uppercaseToDash($string)
        {
            $string = lcfirst($string);
            $newString = '';

            for($i = 0; $i < strlen($string); $i++){
                if(strtolower($string[$i]) === $string[$i]){
                    $newString .= $string[$i];
                }
                else{
                    $newString .= "-" . strtolower($string[$i]);
                }
            }

            return $newString;
        }

        private static function handleWithPolishChars($string)
        {
            $before = array('ą', 'ę', 'ś', 'ć', 'ń', 'ź', 'ż', 'ó', 'ł', 'Ą', 'Ę', 'Ś', 'Ć', 'Ń', 'Ź', 'Ż', 'Ó', 'Ł', ' ');
            $after = array('a', 'e', 's', 'c', 'n', 'z', 'z', 'o', 'l', 'A', 'E', 'S', 'C', 'N', 'Z', 'Z', 'O', 'L', '-');
            $newString = '';

            for ($i = 0; $i < mb_strlen($string); $i++){
                $inp = mb_substr($string, $i, 1, $encoding="UTF-8");

                if (in_array($inp, $before)){
                    $newString .= $after[self::findInArray($before, $inp)];
                }
                else if (ctype_alnum($inp) || $inp == ' ' || $inp == '.' || $inp == '-'){
                    $newString .= $inp;
                }
            }

            return $newString;
        }

        private static function findInArray($arr, $needle)
        {
            for ($i = 0; $i < count($arr); $i++) {
                if ($arr[$i] == $needle) {
                    return $i;
                }
            }

            return -1;
        }
    }