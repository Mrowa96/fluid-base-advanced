<?php
    namespace common\utilities;

    use yii\web\AssetBundle;

    class GridViewAsset extends AssetBundle
    {
        public $basePath = '@webroot';
        public $baseUrl = '@web';
        public $js = [
            '/scripts/_yii/gridView.js'
        ];
        public $depends = [
            'yii\web\YiiAsset',
        ];
    }
