<?php
    namespace common\utilities;

    use yii\web\AssetBundle;

    class ActiveFormAsset extends AssetBundle
    {
        public $basePath = '@webroot';
        public $baseUrl = '@web';
        public $js = [
            '/scripts/_yii/activeForm.js'
        ];
        public $depends = [
            'yii\web\YiiAsset',
        ];
    }
