<?php
    namespace common\utilities;

    use common\models\Product;
    use Elasticsearch\ClientBuilder;

    class ElasticSearch
    {
        protected $client;
        private $index = 'fluid-base-advanced';
        private $hosts = ['127.0.0.1:9200'];
        private $indexPerPackage = 1000;

        public function __construct()
        {
            $this->client = ClientBuilder::create()->setHosts($this->hosts)->build();
        }

        public function indexSomething()
        {
            /** Get all data from Model to build indexes */
            $allData = [];
            $key = 1;
            $params = [
                'body' => []
            ];

            if($this->client->indices()->exists(['index' => $this->index])) {
                $this->client->indices()->delete([
                    'index' => $this->index
                ]);
            }

            foreach ($allData AS $data){
                /**
                 *  $params['body'][] = [
                            'index' => [
                            '_index' => $this->index,
                            '_type' => 'NAME',
                            '_id' => $key++
                        ]
                    ];
                 */

                /**
                 *  $params['body'][] = [
                        'id' => $data->id,
                        'name' => mb_strtolower($data->name),
                        'description' => strip_tags($data->description),
                    ];
                 */

                if ($key % $this->indexPerPackage == 0){
                    $this->client->bulk($params);
                    $params = ['body' => []];
                }
            }

            if (!empty($params['body'])) {
                $this->client->bulk($params);
            }
        }

        public function querySomething($phrase)
        {
            $params = [];
            /**
             * Prepare good query, for example:
             *  $params = [
                    'index' => $this->index,
                    'type' => 'NAME',
                    'body' => [
                        'query' => [
                            'bool' => [
                                'should' => [
                                    [
                                        'multi_match' => [
                                            'query' => $phrase,
                                            'fuzziness' => 'AUTO',
                                            'operator' => 'and',
                                            'fields' => ['name', 'description']
                                        ]
                                    ],
                                [
                                    'match_phrase_prefix' => [
                                        'name' => $phrase
                                        ]
                                    ],
                                ]
                            ]
                        ]
                    ]
                ];
             */

            $results = $this->client->search($params);

            return $this->prepareResults($results);
        }

        private function prepareResults(array $data)
        {
            $indexes = $data['hits']['hits'];
            $results = [];

            foreach($indexes AS $index){
                /**
                 * $results[] = Model::findOne($index['_source']['id']);
                 */
            }

            return $results;
        }
    }