<?php
    namespace common\utilities;
    
    use Yii;

    class ClassicMenu
    {
        private $menuDocument = null;
        private $ulElement = null;
        private $options = [];
        private $controller = null;

        public function __construct(array $options = []){
            $this->menuDocument = new \DOMDocument('1.0');
            $this->ulElement = $this->menuDocument->createElement("ul");
            $this->options = (!empty($options)) ? $options : ['class' => 'sidebar-menu',  'childClass' => 'treeview'];
            $this->controller = Yii::$app->controller->id;

            $this->setOptions();

            $this->menuDocument->appendChild($this->ulElement);
        }

        public function addData(array $groups)
        {
            if(!empty($groups)){
                foreach($groups AS $group){

                    if($group->admin_access === 1 && (Yii::$app->user->isGuest || !Yii::$app->user->identity->isAdmin)){
                        continue;
                    }

                    $liElement = $this->addLinkElement($group, $this->ulElement);

                    if(!empty($group->children)){
                        $ulChildElement = $this->menuDocument->createElement("ul");
                        $ulChildElement->setAttribute("class", "treeview-menu");

                        foreach ($group->children AS $child){
                            $this->addLinkElement($child, $ulChildElement);
                        }

                        $liElement->appendChild($ulChildElement);
                    }
                }
            }

            return $this;
        }

        public function addHeader($text)
        {
            $liElement = $this->menuDocument->createElement("li", $text);

            $liElement->setAttribute("class", "header");

            if($this->ulElement->childNodes->length){
                $this->ulElement->insertBefore($liElement, $this->ulElement->childNodes->item(0));
            }
            else{
                $this->ulElement->appendChild($liElement);
            }

            return $this;
        }

        public function render()
        {
            return $this->menuDocument->saveHTML();
        }

        private function addLinkElement($group, $ulElement){
            $liElement = $this->menuDocument->createElement("li");
            $linkElement = $this->menuDocument->createElement("a");
            $textElement = $this->menuDocument->createElement("span", Yii::t("navigation", $group->name));
            $liClass = "";

            if($this->controller === $group->route){
                $liClass .= "active ";
            }

            if($group->icon){
                $iconElement = $this->menuDocument->createElement("i");
                $iconElement->setAttribute("class", $group->icon);
                $linkElement->appendChild($iconElement);
            }

            if(isset($this->options['childClass'])){
                $liClass .= $this->options['childClass'];
            }

            $linkElement->setAttribute("href", $group->url);
            $liElement->setAttribute("class", $liClass);

            $linkElement->appendChild($textElement);
            $liElement->appendChild($linkElement);
            $ulElement->appendChild($liElement);

            return $liElement;
        }

        private function setOptions()
        {
            $options = $this->options;

            if(isset($options['class'])){
                $this->ulElement->setAttribute("class", $options['class']);
            }
        }
    }