<?php
    namespace backend\searches;

    use Yii;
    use yii\base\Model;
    use yii\data\ActiveDataProvider;
    use common\models\User as UserModel;

    class User extends UserModel
    {
        public function rules()
        {
            return [
                [['id','username', 'name', 'email', 'role', 'roleName'], 'safe'],
            ];
        }

        public function scenarios()
        {
            return Model::scenarios();
        }

        public function search($params)
        {
            $query = UserModel::find();

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
            ]);

            $this->load($params);

            if (!$this->validate()) {
                return $dataProvider;
            }

            $query->andFilterWhere([
                'id' => $this->id,
            ]);

            $query->andFilterWhere(['like', 'username', $this->username])
                ->andFilterWhere(['like', 'email', $this->email])
                ->andFilterWhere(['like', 'role', $this->role])
                ->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'created_at', $this->created_at])
                ->andFilterWhere(['like', 'roleName', $this->roleName]);

            return $dataProvider;
        }
    }
