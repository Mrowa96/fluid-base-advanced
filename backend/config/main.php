<?php
    $config = [
        'id' => 'fba-panel',
        'language' => 'pl_PL',
        'basePath' => dirname(__DIR__),
        'controllerNamespace' => 'backend\controllers',
        'bootstrap' => ['log'],
        'modules' => [],
        'components' => [
            'log' => [
                'traceLevel' => YII_DEBUG ? 3 : 0,
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
            'errorHandler' => [
                'errorAction' => 'site/error',
            ],
            'urlManager' => require_once 'urlManager.php',
            'urlManagerFrontend' => array_merge(
                require_once dirname(__DIR__) . '/../frontend/config/urlManager.php',
                ['class' => 'yii\web\urlManager']
            ),
            'i18n' => [
                'translations' => [
                    '*' => [
                        'class' => 'backend\utilities\i18n',
                        'forceTranslation' => true,
                    ],
                ],
            ],
        ],
    ];

    if (!YII_ENV_TEST) {
        $config['bootstrap'][] = 'gii';
        $config['modules']['gii'] = [
            'class' => 'yii\gii\Module',
            'generators' => [
                'crud' => [
                    'class' => 'yii\gii\generators\crud\Generator',
                    'templates' => [
                        'customCrud' => '@common/utilities/Gii/crud/default',
                    ]
                ],
                'model' => [
                    'class' => 'yii\gii\generators\model\Generator',
                    'templates' => [
                        'customModel' => '@common/utilities/Gii/model/default',
                    ]
                ]
            ],
        ];
    }

    return $config;