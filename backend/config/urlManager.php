<?php
    return [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'rules' => [
            'logout' => 'site/logout',
            'login' => 'site/login',
        ],
        'suffix' => '.html'
    ];