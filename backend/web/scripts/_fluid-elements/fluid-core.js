(function () {
    var Fluid = {
        info: {
            name: "Fluid Elements",
            id: 0,
            version: "1.1.0",
            author: "Paweł Mrowiec, work@pawel-mrowiec.pl"
        },
        wrapper: null,
        loadScreen: null,
        translationsService: '/api/get-translations.html',
        url: (location.origin) ? location.origin : location.protocol + "//" + location.host,
        started: false,
        elements: {},
        domElements: [],
        dataName: 'fluid',
        protectedModulesIndex: 10,
        randomId: [],
        modules: {
            0: 'Fluid',
            1: 'Library',
            2: 'Http',
            3: 'Loader',
            4: 'Browser',
            5: 'Dim',
            6: 'Notify',
            7: 'LoadScreen',
            8: 'History',
            9: 'Translation',
            10: 'Modal',
            11: 'Panel',
            12: 'Editor',
            13: 'DynamicView',
            14: 'MailBox',
            15: 'Form',
            16: 'List',
            17: 'FileManager',
            18: 'TableView',
            19: 'Navigation',
            20: 'GridView',
            21: 'TabbedView'
        },

        init: function () {
            window.addEventListener("load", function () {
                if (Fluid.started === false) {
                    Fluid.Lib = new Fluid.Library();
                    Fluid.Http = new Fluid.Http();
                    Fluid.Loader = new Fluid.Loader();
                    Fluid.Browser = new Fluid.Browser();
                    Fluid.History = new Fluid.History();
                    Fluid.Translation = new Fluid.Translation();

                    Fluid.wrapper = document.getElementsByClassName("wrapper")[0];
                    Fluid.domElements = Fluid.Lib.toArray(document.getElementsByTagName("*"));

                    Fluid.handleLoadScreen();
                    Fluid.addRequiredListeners();
                    Fluid.handleReloadAndRedirect();
                    Fluid.Loader.load(Fluid);
                    Fluid.Translation.init();

                    window.Fluid = Fluid;
                    Fluid.started = true;
                    Fluid.loadScreen.hide();

                    console.info("Fluid Elements started.");
                }
                else {
                    Fluid.alreadyStarted();
                }
            }, false);
        },
        alreadyStarted: function () {
            Fluid.Message(0, 0);
            return Fluid;
        },
        checkCaller: function (mod) {
            if (mod != Fluid) {
                return true;
            }
            else {
                throw Fluid.Message(0, null, mod, false);
            }
        },
        addToElements: function (element) {
            if (Fluid.Lib.is(element)) {
                if (Fluid.Lib.is(this.elements[element.info.name]) === false) {
                    this.elements[element.info.name] = [];
                }

                this.elements[element.info.name].push(element);
            }
        },
        addRequiredListeners: function(){
            implementTouchPanelOpen();
            handleContextMenu();

            function implementTouchPanelOpen(){
                document.addEventListener("touchstart", function(e){
                    var touches, touch;

                    e = e || window.event;
                    touches = e.changedTouches;

                    if(touches.length && Fluid.elements.Panel && Fluid.elements.Panel.length){
                        touch = touches[0];

                        Fluid.elements.Panel.forEach(function(panel){
                            // if hidden
                            if(panel.conf.touchShow === true && panel.hidden === true){
                                switch(panel.conf.position){
                                    case "left":
                                        if(0 <= touch.clientX && panel.touchArea >= touch.clientX){
                                            Fluid.Browser.panelsToOpen[panel.conf.position].push(panel);
                                        }
                                        break;
                                    case "bottom":
                                        if(Fluid.Lib.getHeight() >= touch.clientY && (Fluid.Lib.getHeight() - panel.touchArea) <= touch.clientY){
                                            Fluid.Browser.panelsToOpen[panel.conf.position].push(panel);
                                        }
                                        break;
                                }
                            }
                        });
                    }
                }, false);
                document.addEventListener("touchmove", function(e){
                    var touches, touch, differenceX, differenceY;

                    e = e || window.event;
                    touches = e.changedTouches;

                    if(touches.length && Fluid.elements.Panel && Fluid.elements.Panel.length) {
                        touch = touches[0];

                        if(Fluid.Browser.moveAttemps > 0){
                            if(Fluid.Browser.moveAttemps === 5){
                                Fluid.Browser.moveStartPos = {
                                    x: touch.clientX,
                                    y: touch.clientY
                                }
                            }

                            Fluid.Browser.moveAttemps--;
                        }
                        else{
                            if(Fluid.Browser.moveAttemps === 0){
                                differenceX = Math.abs(touch.clientX - Fluid.Browser.moveStartPos.x);
                                differenceY = Math.abs(touch.clientY - Fluid.Browser.moveStartPos.y);

                                if(Fluid.Browser.moveStartPos.x < Fluid.Lib.getWidth()/2){
                                    if(Fluid.Browser.moveStartPos.y < Fluid.Lib.getHeight()/2){
                                        if(differenceX > differenceY){
                                            Fluid.Browser.selectedPanelPosition = "left";
                                        }
                                        else{
                                            Fluid.Browser.selectedPanelPosition = "top";
                                        }
                                    }
                                    else{
                                        if(differenceX > differenceY){
                                            Fluid.Browser.selectedPanelPosition = "left";
                                        }
                                        else{
                                            Fluid.Browser.selectedPanelPosition = "bottom";
                                        }
                                    }
                                }
                                else{
                                    if(Fluid.Browser.moveStartPos.y < Fluid.Lib.getHeight()/2){
                                        if(differenceX > differenceY){
                                            Fluid.Browser.selectedPanelPosition = "right";
                                        }
                                        else{
                                            Fluid.Browser.selectedPanelPosition = "top";
                                        }
                                    }
                                    else{
                                        if(differenceX > differenceY){
                                            Fluid.Browser.selectedPanelPosition = "right";
                                        }
                                        else{
                                            Fluid.Browser.selectedPanelPosition = "bottom";
                                        }
                                    }
                                }

                                Fluid.Browser.moveAttemps = null;
                            }
                            else if(Fluid.Browser.moveAttemps === null){
                                if(Fluid.Browser.panelsToOpen[Fluid.Browser.selectedPanelPosition].length){
                                    Fluid.Browser.panelsToOpen[Fluid.Browser.selectedPanelPosition][0].show();
                                }
                            }
                        }

                    }
                }, false);
                document.addEventListener("touchend",onCancel, false);
                document.addEventListener("touchcancel",onCancel, false);

                function onCancel(){
                    Fluid.Browser.refreshMoveAttemps();
                    Fluid.Browser.selectedPanelPosition = null;
                }
            }
            function handleContextMenu(){
                document.addEventListener("contextmenu", function(e){
                    var ev, hasContextMenu, rows, link = null, dList, aTag;

                    ev = e || window.event;

                    if(ev.target.tagName === "A"){
                        link = ev.target;
                    }

                    if(!ev.target.hasContextMenu || ev.target.hasContextMenu === false){
                        hasContextMenu = Fluid.Lib.iterateOverParents(ev.target, function(parent, element){
                            if(parent.tagName === "A"){
                                link = parent;
                            }

                            if(parent.hasContextMenu){
                                return parent.hasContextMenu;
                            }
                            else{
                                return false;
                            }
                        });

                        if(hasContextMenu !== true){
                            /*rows = [{value: 999, element: 'O aplikacji'}];

                             if(link !== null && link.getAttribute("href") !== "#"){
                             aTag = document.createElement("a");
                             aTag.setAttribute("target", "_blank");
                             aTag.setAttribute("href", link.getAttribute("href"));
                             aTag.innerHTML = "Otwórz w nowej karcie";

                             aTag.addEventListener("click", function(){
                             window.open(this.getAttribute("href"), "_blank");
                             });

                             rows.push({
                             value: 1,
                             element: aTag,
                             withoutWrap: true
                             });
                             }

                             Fluid.Browser.hideLists();

                             dList = new Fluid.List({
                             name: 'globalContextMenu',
                             append: ".wrapper",
                             canHide: true,
                             hidden: true,
                             position: {x: e.clientX, y: e.clientY},
                             rows: rows,
                             rowsPrefix: '<a href="#">',
                             rowsSuffix: '</a>',
                             emitter: ev.target,
                             skipDefaultValue: false,
                             onSelect: function (list) {
                             switch (list.value) {
                             case "999":
                             var content = "Aplikacja oparta została o: <br> Fluid Elements&copy; " + Fluid.info.version,
                             fluidCmsVer = document.querySelector("#fluidCmsVer");

                             if(fluidCmsVer){
                             content += "<br>Fluid CMS&copy; " + fluidCmsVer.value
                             }

                             new Fluid.Modal({
                             header: "O aplikacji",
                             dim: true,
                             content: content
                             });
                             break;
                             }

                             list.remove();
                             }
                             });

                             dList.show();*/
                        }
                    }
                    else{
                        ev.preventDefault();
                        return false;
                    }


                });
            }
        },
        handleReloadAndRedirect: function(){
            var reloadWithMessage = localStorage.getItem("reloadWithMessage"),
                redirectWithMessage = localStorage.getItem("reloadWithMessage"),
                message, type;

            if(reloadWithMessage){
                message = localStorage.getItem("reloadMessage");
                type = localStorage.getItem("reloadType");

                localStorage.removeItem("reloadMessage");
            }
            else if(redirectWithMessage){
                message = localStorage.getItem("redirectMessage");
                type = localStorage.getItem("redirectType");
            }

            new Fluid.Notify(message, type);
        },
        handleLoadScreen: function(){
            var loadScreen = document.querySelectorAll(".loadScreen");

            if(loadScreen.length){
                this.loadScreen = new Fluid.LoadScreen(loadScreen[0]);
            }
            else{
                this.loadScreen = new Fluid.LoadScreen();
            }

            this.loadScreen.show();
        }
    };

    Fluid.Loader = function () {
        this.name = null;
        this.module = null;
        this.temporary = {};
    };
    Fluid.Loader.prototype.load = function (module) {
        var _this = this;

        if (Fluid.Lib.is([module, module.dataName])) {
            this.name = module.dataName;
            this.module = module;
            this.wrapper = module.wrapper;

            Fluid.domElements.forEach(function (elm) {
                if (elm.dataset[_this.name]) {
                    _this.temporary.element = elm;
                    Fluid.Loader.configureElement();
                    Fluid.Loader.initializeElement();
                }
            });
        }
    };
    Fluid.Loader.prototype.configureElement = function () {
        var confObj = {},
            conf,
            property,
            indexColon = 0,
            lastIndexColon = 0,
            indexComma = 0,
            indexApostophe = 0,
            nextApostophe = 0;

        if (Fluid.started === false) {
            if (Fluid.Lib.is(this.temporary.element)) {
                conf = this.temporary.element.dataset[this.name];

                while (conf.indexOf(":", indexColon) !== -1 && indexColon !== -1) {
                    indexColon = conf.indexOf(":", indexColon);
                    property = conf.substr(indexComma, indexColon - indexComma).trim();
                    indexComma = conf.indexOf(",", indexColon);
                    indexApostophe = conf.indexOf("'", indexColon);
                    indexColon++;

                    if (indexApostophe < indexComma && indexApostophe !== -1) {
                        nextApostophe = conf.indexOf("'", indexApostophe + 1);
                        indexComma = conf.indexOf(",", nextApostophe);
                        indexColon = conf.indexOf(":", nextApostophe);
                        value = conf.substr(indexApostophe + 1, Math.abs(nextApostophe - indexApostophe - 1)).trim();
                    }
                    else {
                        value = conf.substr(indexColon, Math.abs(indexComma - indexColon)).trim();
                        if (value[0] === "'") {
                            value = value.substr(1);
                        }

                        if (value[value.length - 1] === "'") {
                            value = value.substr(0, value.length - 1);
                        }
                    }

                    confObj[property] = Fluid.Lib.strBoolToBool(value);

                    lastIndexColon = indexColon;
                    indexComma++;
                }

                this.temporary.configuration = confObj;
                this.temporary.element.removeAttribute("data-" + this.name);
            }
        }
        else {
            Fluid.alreadyStarted();
        }
    };
    Fluid.Loader.prototype.initializeElement = function () {
        var temp = this.temporary,
            modules, mod, key, _this = this;

        if (Fluid.Lib.is(temp.element, temp.configuration)) {
            temp.configuration.createdBy = "loader";

            if (Fluid.Lib.is(temp.configuration.element)) {
                mod = temp.configuration.element;
            }
            else {
                Fluid.Message(0, 1);
                return;
            }

            if (Fluid.Lib.is(mod)) {
                mod = mod[0].toUpperCase() + mod.substr(1);
                modules = Fluid.Lib.toArray(this.module['modules']);

                if (modules.indexOf(mod) !== -1) {
                    key = Object.keys(this.module['modules']).filter(function(key) {
                        return _this.module['modules'][key] === mod
                    })[0];

                    if(key > Fluid.protectedModulesIndex){
                        new this.module[mod](temp.configuration, temp.element);
                    }
                }
            }
        }
    };
    Fluid.Loader.prototype.mergeConfiguration = function (element) {
        var conf = {},
            dConf = {},
            newConf = {};

        if (Fluid.Lib.is(element, element.defaultConf, element.conf)) {
            conf = element.conf;
            dConf = element.defaultConf;

            for (prop in dConf) {
                if (dConf.hasOwnProperty(prop)) {
                    if (conf[prop] !== undefined) {
                        newConf[prop] = conf[prop];
                    }
                    else {
                        newConf[prop] = dConf[prop];
                    }
                }
            }

            for (prop in conf) {
                if (conf.hasOwnProperty(prop)) {
                    if (newConf[prop] === undefined) {
                        newConf[prop] = conf[prop];
                    }
                }
            }

            element.conf = newConf;
        }
    };

    Fluid.Browser = function () {
        this.info = {
            name: "Browser",
            id: 4
        };
        this.dim = null;
        this.selectedPanel = null;
        this.selectedPanelPosition = null;
        this.moveAttemps = 5;
        this.moveStartPos = {};
        this.panelsToOpen = {
            left: [],
            right: [],
            top: [],
            bottom: []
        };
        this.scripts = [];
        this.styles = [];

        Fluid.checkCaller(this);

        this.updateScripts();
        this.updateStyles();
    };
    Fluid.Browser.prototype.updateScripts = function(){
        this.scripts = Fluid.Lib.toArray(document.getElementsByTagName("script"));
    };
    Fluid.Browser.prototype.updateStyles = function(){
        var _this = this, styles;

        styles = Fluid.Lib.toArray(document.getElementsByTagName("link"));
        styles.forEach(function(style){
            if(style.getAttribute("rel") === "stylesheet"){
                _this.styles.push(style);
            }
        })
    };
    Fluid.Browser.prototype.refreshMoveAttemps = function(){
        this.moveAttemps = 5;
    }
    Fluid.Browser.prototype.handleOffEvents = function (offConf) {
        /*
         *    offConf = {
         *        element: Node,
         *        notCLicked: function(element){},
         *        excludedElements: [Node, Node, ...]
         *    }
         */

        if (Fluid.Lib.is(offConf)) {
            document.addEventListener("click", function (e) {
                var evt = e || window.event,
                    element = offConf.element,
                    excludedElements = offConf.excludedElements || [],
                    callBack = offConf.notClicked,
                    clicked = evt.target,
                    parent = clicked;

                while (parent !== element) {
                    if (excludedElements.length && excludedElements.indexOf(parent) !== -1) {
                        break;
                    }
                    else if (parent !== document && parent) {
                        parent = parent.parentNode;
                    }
                    else {
                        callBack(element);
                        break;
                    }
                }
            }, false);
        }
    };
    Fluid.Browser.prototype.hidePanels = function(){
        var iteration = 0;

        if(Fluid.elements.Panel){
            Fluid.elements.Panel.forEach(function(panel){
                if(panel.hidden !== true){
                    panel.hide(true);
                    iteration++;
                }
            });
        }

        return iteration > 0;
    };
    Fluid.Browser.prototype.hideLists = function(){
        var iteration = 0;

        if(Fluid.elements.List){
            Fluid.elements.List.forEach(function(list){
                if(list.hidden !== true){
                    list.hide();
                    iteration++;
                }
            });
        }

        return iteration > 0;
    };
    Fluid.Browser.prototype.hideModals = function(){
        var iteration = 0;

        if(Fluid.elements.Modal){
            Fluid.elements.Modal.forEach(function(modal){
                if(modal.hidden !== true){
                    modal.hide(true);
                    iteration++;
                }
            });
        }

        return iteration > 0;
    };
    Fluid.Browser.prototype.reloadWithMessage = function(message, type){
        localStorage.setItem("reloadWithMessage", true);
        localStorage.setItem("reloadMessage", message);
        localStorage.setItem("reloadType", type);

        location.reload();
    };
    Fluid.Browser.prototype.redirectWithMessage = function(url, message, type){
        localStorage.setItem("redirectWithMessage", true);
        localStorage.setItem("redirectMessage", message);
        localStorage.setItem("redirectType", type);

        location.href = url;
    };

    Fluid.LoadScreen = function(elm){
        this.info = {
            name: "LoadScreen",
            id: 7
        };
        this.element = (elm) ? elm : null;

        Fluid.checkCaller(this);
    };
    Fluid.LoadScreen.prototype.show = function(){
        if(this.element === null){
            this.createElement();
        }

        this.element.classList.remove("loadScreenClose");
        this.element.classList.add("loadScreenOpen");
    };
    Fluid.LoadScreen.prototype.hide = function(){
        if(this.element === null){
            this.createElement();
        }

        this.element.classList.remove("loadScreenOpen");
        this.element.classList.add("loadScreenClose");
        this.addListeners("hide", this.element);

    };
    Fluid.LoadScreen.prototype.addListeners = function(type, elm){
        var _this = this;

        if(type){
            switch(type){
                case "hide":
                    if(elm){
                        elm.addEventListener("animationend", function(e){
                            var ev = e || window.event;

                            if(ev.target === _this.element){
                                _this.element.parentNode.removeChild(_this.element);
                            }
                        })
                    }
                    break;
            }
        }
    };
    Fluid.LoadScreen.prototype.createElement = function(){
        var loadScreen = document.createElement("div");

        loadScreen.classList.add("loadScreen");
        Fluid.wrapper.appendChild(loadScreen);

        this.element = loadScreen;
    };

    Fluid.Dim = function (conf) {
        this.info = {
            name: "Dim",
            id: 5
        };
        this.defaultConf = {
            background: "#000",
            opacityWhenShowed: 0.6,
            duration: ".3s",
            delay: "0s",
            effect: "ease",
            wrapper : (Fluid.wrapper !== null) ? Fluid.wrapper : document.getElementsByClassName("wrapper")[0]
        };
        this.conf = {};
        this.element = null;
        this.createTime = new Date().getTime();

        Fluid.checkCaller(this);

        if(Fluid.Lib.is(conf)){
            this.conf = conf;
        }

        if(Fluid.Lib.is(Fluid.elements.Dim) === false || Fluid.elements.Dim.length === 0){
            Fluid.addToElements(this);
            Fluid.Loader.mergeConfiguration(this);

            this.createElement();

            return this;
        }
        else if(Fluid.Lib.is(Fluid.elements.Dim[0])){
            var dimObject = Fluid.elements.Dim[0];

            dimObject.conf = this.conf;
            Fluid.Loader.mergeConfiguration(dimObject);

            if(dimObject.element === null){
                dimObject.createElement();
            }

            return dimObject;
        }
        else{
            //throw error
        }
    };
    Fluid.Dim.prototype.hide = function(){
        this.element.style.opacity = 0;

        this.element.addEventListener("transitionend", this.onHide, false);
    };
    Fluid.Dim.prototype.show = function(){
        var currentTime = new Date().getTime(),
            _this = this;

        if(currentTime - this.createTime < 10){
            setTimeout(function(){
                showDim();
            }, 10);
        }
        else{
            showDim();
        }

        function showDim(){
            _this.element.style.zIndex = 1150;
            _this.element.style.opacity = _this.conf.opacityWhenShowed;

            _this.element.removeEventListener("transitionend", _this.onHide);
        }

    };
    Fluid.Dim.prototype.onHide = function(){
        this.style.zIndex = -1;
    };
    Fluid.Dim.prototype.createElement = function(){
        this.element = document.createElement("div");
        this.element.classList.add("dim");
        this.conf.wrapper.appendChild(this.element);
        this.element.style.background = this.conf.background;
        this.element.style.transition = "opacity " + this.conf.duration + " " + this.conf.effect + " " + this.conf.delay;;

        Fluid.Browser.dim = this;
    };

    Fluid.Translation = function(){
        this.info = {
            name: "Translation",
            id: '9'
        };

        this.serviceUrl = null;
        this.initialized = false;
        this.data = null;
        this.lang = null;
        this.notFoundService = false;

        Fluid.checkCaller(this);
        Fluid.addToElements(this);

        this.init();

        return this;
    };
    Fluid.Translation.prototype.init = function(){
        if (this.initialized === false) {
            var translations = localStorage.getItem("translations");

            if(Fluid.translationsService === null){
                console.error("Translation service is not defined");

                this.notFoundService = true;
            }
            else{
                this.serviceUrl = Fluid.translationsService;

                if(window.Cookies && Cookies.get("language")){
                    this.lang = Cookies.get("language");
                }
                else{
                    console.error("Cannot find language cookie");
                }

                if(translations){
                    translations = JSON.parse(translations);

                    var d1 = new Date(),
                        d2 = new Date(translations.expire);

                    if(d1.getTime() > d2.getTime()){
                        this.loadNewTranslations();
                    }
                    else{
                        this.data = translations.translations;
                        this.lang = translations.language;
                    }
                }
                else{
                    this.loadNewTranslations();
                }
            }

            this.initialized = true;
        }
        else{
            //error
        }
    };
    Fluid.Translation.prototype.loadNewTranslations = function(){
        var _this = this;

        if(this.notFoundService === false){
            Fluid.Http.get({
                url: this.serviceUrl,
                onResponse: function(response){
                    var res = JSON.parse(response),
                        date = new Date();

                    if(res.status){
                        _this.data = res.translations.data;

                        res.expire = date.setDate(date.getDate() + 1);
                        localStorage.setItem("translations", JSON.stringify(res));
                    }
                }
            });
        }
        else{
            console.error("Translation service is not defined");

            return false;
        }
    };
    Fluid.Translation.prototype.t = function(category, source){
        var translation;

        if(this.notFoundService === false){
            try{
                translation = this.data[category][source][this.lang];
            }
            catch (error){
                translation = source;
            }

            return translation;
        }
        else{
            console.error("Translation service is not defined");

            return false;
        }
    };

    Fluid.Editor = function(conf, elm){
        this.info = {
            name: "Editor",
            id: 12
        };
        this.defaultConf = {
            id: null
        };
        this.conf = {};
        this.element = null;
        this.initialized = false;
        this.formInfoContainer = null;
        this.formInfo2Container = null;
        this.urls = {};
        this.forms = {
            info1: null,
            info2: null
        };
        this.template = null;
        this.module = null;
        this.templateWindow = null;
        this.editorPanel = null;
        this.exsistingData = {};
        this.nextBtnText = "Utwórz";

        Fluid.checkCaller(this);
        Fluid.addToElements(this);

        if (Fluid.Lib.is(conf)) {
            this.conf = conf;
            Fluid.Loader.mergeConfiguration(this);

            if(Fluid.Lib.is(elm)){
                this.element = elm;
            }

            this.init();

            return this;
        }
        else {
            Fluid.Message(12, 0);
            return Fluid;
        }
    };
    Fluid.Editor.prototype.init = function(){
        var _this = this, dim;

        if (this.initialized === false) {
            if(this.element !== null){
                this.element.classList.add("editor");

                initUrls();

                if(this.conf.id !== null){
                    getExistingData();
                }
                else{
                    this.addListener();
                    initSteps();
                }
            }
            else{
                //error
            }

            this.initialized = true;
        }
        else{
            //error
        }

        function initSteps(){
            var elements;

            elements = Fluid.Lib.toArray(_this.element.getElementsByTagName("*"));
            elements.forEach(function(element, index){
                if(element.dataset.step){
                    switch(element.dataset.step){
                        case "start":
                            element.classList.add("startPage");
                            break;

                        case "information":
                            _this.formInfoContainer = document.getElementById("formInfoContainer");
                            _this.formInfo2Container = document.getElementById("formInfo2Container");
                            _this.handleInformationStep(element);
                            element.classList.add("informationPage");
                            break;

                        case "create":
                            _this.handleCreateStep();
                            element.classList.add("creatorPage");
                            break;
                    }

                    element.removeAttribute("data-step");
                }
            });
        }
        function initUrls(){
            _this.urls.formData =  Fluid.url + '/page/get-data.html';
            _this.urls.savePage = Fluid.url + '/page/save-page.html';
            _this.urls.getExistingData = Fluid.url + '/page/get-existing-data.html';
        }
        function getExistingData(){
            Fluid.Http.get({
                url: _this.urls.getExistingData + "?id=" + _this.conf.id,
                onResponse: function(response){

                    _this.exsistingData = JSON.parse(response);
                    _this.nextBtnText = "Zaktualizuj";
                    _this.addListener();

                    initSteps();
                }
            })
        }
    };
    Fluid.Editor.prototype.addListener = function(type, elm, callback){
        var _this = this;

        if(Fluid.Lib.is(type)){
            switch (type){
                case "text":
                    if(Fluid.Lib.is(elm)){
                        elm.addEventListener("click", function(){
                            var textEditor, okButton, wrapper;

                            wrapper = document.createElement("div");
                            wrapper.classList.add("content", "container");

                            textEditor = document.createElement("textarea");
                            textEditor.id = "editorArea";
                            textEditor.value = elm.innerHTML;

                            okButton = document.createElement("span");
                            okButton.classList.add("buttonOk");
                            okButton.innerHTML = "Zatwierdź";

                            wrapper.appendChild(textEditor);
                            wrapper.appendChild(okButton);

                            okButton.addEventListener("click", function(){
                                elm.innerHTML = tinymce.activeEditor.getContent();
                                _this.editorPanel.hide();
                            });

                            _this.editorPanel.replaceContent(wrapper);

                            if(tinymce){
                                tinymce.init({
                                    selector: '#editorArea',
                                    language: 'pl',
                                    plugins: "link"
                                });
                            }

                            _this.editorPanel.show();
                        });
                    }
                    break;
                case "image":
                    if(Fluid.Lib.is(elm)){
                        elm.addEventListener("click", function(){
                            var wrapper, imageChoser;

                            wrapper = new Fluid.Panel({
                                dim: false,
                                moveContentOnSlide: _this.element,
                                hidden: true,
                                removeOnHide: true,
                                create: true,
                                title: "Menedżer plików"
                            });
                            imageChoser = new Fluid.FileManager({
                                path: "/data",
                                onLoad: function(){
                                    Fluid.Lib.toArray(document.querySelectorAll(".elementThumbWrap")).forEach(function(item){
                                        item.removeAttribute("class");

                                        item.classList.add("elementThumbWrap", "col-lg-2", "col-md-2", "col-sm-2", "col-xs-3");
                                    });
                                    Fluid.Lib.toArray(document.querySelectorAll(".elementNameWrap")).forEach(function(item){
                                        item.removeAttribute("class");

                                        item.classList.add("elementNameWrap", "col-lg-7", "col-md-7", "col-sm-7", "col-xs-7");
                                    });
                                    Fluid.Lib.toArray(document.querySelectorAll(".elementSizeWrap")).forEach(function(item){
                                        item.removeAttribute("class");

                                        item.classList.add("elementSizeWrap", "col-lg-3", "col-md-3", "col-sm-3", "col-xs-hidden");
                                    });

                                },
                                onSelectItem: function(item, fileManager){
                                    elm.setAttribute("src", fileManager.srcUrl + item.dataset.path);
                                }
                            }, wrapper.element);
                            wrapper.element.classList.add("col-lg-3", "col-md-3", "col-sm-6", "col-xs-12");
                            imageChoser.element.classList.add("imageChoser");

                            wrapper.show();
                        });
                        elm.addEventListener("contextmenu", function(e){
                            var ev = e || window.event,
                                list;

                            Fluid.Browser.hideLists();

                            list = new Fluid.List({
                                name: 'imageContextList',
                                append: ".wrapper",
                                canHide: true,
                                hidden: false,
                                position: {x: ev.clientX, y: ev.clientY},
                                rows: [
                                    {value: 0, element: 'Usuń'},
                                ],
                                skipDefaultValue: false,
                                rowsPrefix: '<a href="#">',
                                rowsSuffix: '</a>',
                                emitter: elm,
                                onSelect: function (list) {
                                    switch (list.value) {
                                        case "0":
                                            list.emitter.parentNode.removeChild(list.emitter);
                                            Fluid.Browser.hidePanels();
                                            break;
                                    }

                                    list.hide();
                                }
                            });

                            elm.hasContextMenu = true;
                            ev.preventDefault();

                        })
                    }
                    break;
                case "templateWindow":
                    if(Fluid.Lib.is(elm)){
                        elm.addEventListener("contextmenu", function(e){
                            var ev = e || window.event;

                            elm.hasContextMenu = true;

                            ev.preventDefault();
                        })
                    }
                    break;
            }
        }
        else{
            document.addEventListener('nextView', function (e) {
                var dynamicView = e.detail;

                Fluid.Browser.hidePanels();

                if(dynamicView.currentIndex === 0){
                    nextStandard(dynamicView);
                    dynamicView.sections[dynamicView.currentIndex].show();
                    onInformationStep();
                }
                else if(dynamicView.currentIndex === 1){
                    if(_this.forms.info1.clientValidation() && _this.forms.info2.clientValidation()){
                        nextStandard(dynamicView);
                        dynamicView.pagination.nextButton.innerHTML = _this.nextBtnText;
                        dynamicView.sections[dynamicView.currentIndex].show();
                    }
                }
                else if(dynamicView.currentIndex === 2){
                    _this.completeData();

                    if(history.back() === undefined){
                        location.href = Fluid.url;
                    }
                }
            });
            document.addEventListener('prevView', function (e) {
                var dynamicView = e.detail;

                Fluid.Browser.hidePanels();

                prevStandard(dynamicView);
                dynamicView.sections[dynamicView.currentIndex].show();

                if(dynamicView.currentIndex === 1 || dynamicView.currentIndex === 2 ){
                    onInformationStep();
                }

                if(dynamicView.currentIndex === dynamicView.prevCurrentIndex){
                    if(history.back() === undefined){
                        location.href = Fluid.url;
                    }
                }
            });
            document.addEventListener('paginationStep', function (e) {
                var dynamicView = e.detail;

                Fluid.Browser.hidePanels();

                if(dynamicView.currentIndex === 2){
                    if(!_this.forms.info1.clientValidation() || !_this.forms.info2.clientValidation()){
                        dynamicView.currentIndex = dynamicView.prevCurrentIndex;
                        return;
                    }
                    else{
                        dynamicView.pagination.nextButton.innerHTML = _this.nextBtnText;
                    }
                }

                dynamicView.sections[dynamicView.currentIndex].show();
                dynamicView.updateProgressBar(dynamicView.currentIndex);
                dynamicView.updatePagination(dynamicView.currentIndex);

                if(dynamicView.currentIndex === 1){
                    onInformationStep();
                    dynamicView.pagination.nextButton.innerHTML = "Dalej";
                }
                if(dynamicView.currentIndex === 0){
                    dynamicView.pagination.nextButton.innerHTML = "Dalej";
                }
            });

            function onInformationStep(){
                if(!_this.forms.info2){
                    _this.handleInformationStep();
                }
                else{
                    _this.forms.info2.onComplete()
                }
            }
            function nextStandard(dynamicView){
                if(dynamicView.currentIndex < dynamicView.sections.length) {
                    dynamicView.currentIndex++;
                    dynamicView.updateProgressBar(dynamicView.currentIndex);
                    dynamicView.updatePagination(dynamicView.currentIndex);
                }
            }
            function prevStandard(dynamicView){
                if(dynamicView.currentIndex > 0){
                    dynamicView.currentIndex--;
                    dynamicView.updateProgressBar(dynamicView.currentIndex);
                    dynamicView.updatePagination(dynamicView.currentIndex);
                    dynamicView.pagination.nextButton.innerHTML = "Dalej";
                }
            }
        }
    };
    Fluid.Editor.prototype.handleInformationStep = function(element){
        var form, form2, _this = this;

        initForm1();
        initForm2();

        function initForm1(){
            if(!_this.forms.info1){
                form = new Fluid.Form({ create: true, attributes: {
                    action: '/page/create', id: 'information'
                } });

                form.field({placeholder: "Tytuł", name: "title", validate: "required, text", value: (_this.exsistingData.title) ? _this.exsistingData.title : ""});
                form.field({placeholder: "Słowa kluczowe", name: "keywords", validate: "required, text", value: (_this.exsistingData.keywords) ? _this.exsistingData.keywords : ""});
                form.field({placeholder: "Adres (adres modułu zostanie dodany automatycznie)", name: "url", validate: "required, text", value: (_this.exsistingData.url) ? _this.exsistingData.url : ""});
                form.field({placeholder: "Opis", name: "description", type: 'textarea', rows: 5, validate: "required, text", value: (_this.exsistingData.description) ? _this.exsistingData.description : ""});
                form.field({label:"Wyświelt na nawigacji", name: "show_in_navigation", type: "checkbox", value: "1"});

                form.init();
                form.append(this.formInfoContainer);

                _this.forms.info1 = form;
            }
        }

        function initForm2(){
            if(!_this.forms.info2){
                form2 = new Fluid.Form({ create: true, attributes: {
                    action: '/page/create', id: 'information2'
                }});

                _this.getData(function(response){
                    var data = JSON.parse(response), rows = [];

                    if(data.templates.length){
                        rows.push({
                            value: 0,
                            element: 'Wybierz szablon'
                        });

                        if(_this.exsistingData.id){
                            rows.push({
                                value: _this.exsistingData.template_id,
                                element: 'Aktualny szablon',
                                extra: "current"
                            });
                        }

                        data.templates.forEach(function(val){
                            rows.push({
                                value:  val.id,
                                element: val.title
                            })
                        });

                        form2.field({
                            type: "select",
                            rows: rows,
                            name: 'template_id',
                            hidden: true,
                            canHide: true,
                            orderRows: false,
                            skipDefaultValue: (_this.exsistingData.id) ? false : true,
                            onSelect: function(list, elm){
                                if(_this.exsistingData.id && elm.dataset.extra && elm.dataset.extra === "current"){
                                    data.templates.forEach(function(template){
                                        if(template.id == _this.exsistingData.template_id){
                                            _this.template = JSON.parse(JSON.stringify(template));
                                            _this.template.html_data = _this.exsistingData.data;

                                            return true;
                                        }
                                    })
                                }
                                else{
                                    if(_this.exsistingData.id) {
                                        new Fluid.Notify("Pamiętaj, gdy zaktualizujesz stronę, aktualna zostanie bezpowrotnie usunięta.", "warning");
                                    }

                                    _this.template = data.templates[parseInt(list.value, 10) - 1];
                                }

                                if(_this.template){
                                    _this.refreshCreateStep();
                                }
                            },
                            validate: "required"
                        });
                    }

                    if(data.modules.length){
                        rows = [];

                        rows.push({
                            value: 0,
                            element: 'Wybierz moduł'
                        });

                        data.modules.forEach(function(val){
                            rows.push({
                                value: val.id,
                                element: val.name
                            })
                        });
                        form2.field({
                            type: "select",
                            rows: rows,
                            name: 'module_id',
                            hidden: true,
                            onSelect: function(list){
                                _this.module = data.modules[list.value - 1];
                            },
                            value: (_this.exsistingData.module_id) ? _this.exsistingData.module_id : 0,
                            validate: "required"
                        });
                    }

                    form2.init();
                    form2.append(_this.formInfo2Container);

                    _this.forms.info2 = form2;
                })
            }
        }
    };
    Fluid.Editor.prototype.handleCreateStep = function(){
        var _this = this;

        this.templateWindow = document.getElementById("templateWindow");
        this.addListener("templateWindow", this.templateWindow);

        this.editorPanel = new Fluid.Panel({
            dim: false,
            position: "top",
            create: true,
            title: null,
            hidden: true,
            id: "editorPanel",
            moveContentOnSlide: this.element
        });
    };
    Fluid.Editor.prototype.getData = function(callback){
        Fluid.Http.get({
            url: this.urls.formData,
            onResponse: callback
        });
    };
    Fluid.Editor.prototype.refreshCreateStep = function(){
        var head = document.getElementsByTagName("head")[0],
            _this = this;

        if(this.template !== null){
            this.templateWindow.innerHTML = this.template.html_data;

            addCss();
            addJs();
            addTemplateListeners();
        }

        function addCss(){
            var style = document.createElement("style");

            style.innerHTML = _this.template.css_data;

            head.appendChild(style);
        }
        function addJs(){
            var script = document.createElement("script");

            script.innerHTML = _this.template.js_data;

            head.appendChild(script);
        }
        function addTemplateListeners(){
            var elements = Fluid.Lib.toArray(_this.templateWindow.getElementsByTagName("*"));

            elements.forEach(function(element){
                if(element.dataset.pattern){
                    switch(element.dataset.pattern){
                        case "text":
                            _this.addListener("text", element);
                            break;
                        case "image":
                            _this.addListener("image", element);
                            break;
                    }
                }
            })
        }
    };
    Fluid.Editor.prototype.completeData = function(){
        var _this = this, requestData = [], toSend;

        getFormsData();
        getTemplateData();
        console.log(toSend);
        if(this.exsistingData.id){
            toSend = "data=" + JSON.stringify(requestData) + "&id=" + this.exsistingData.id;
        }
        else{
            toSend = "data=" + JSON.stringify(requestData);
        }

        Fluid.Http.post({
            url: this.urls.savePage,
            toSend: toSend,
            onResponse: function(response){
                var res = JSON.parse(response);

                if(res.failed === 0){
                    history.back();
                }
                else{
                    new Fluid.Notify(res.message);
                }
            }
        });

        function getTemplateData(){
            requestData.push({
                name: 'data',
                value: _this.templateWindow.innerHTML
            })
        }
        function getFormsData(){
            if(_this.forms.info1){
                for(var name in _this.forms.info1.elements){
                    requestData.push({
                        name: name,
                        value: _this.forms.info1.elements[name].value,
                    })
                }
            }

            if(_this.forms.info2){
                for(var name in _this.forms.info2.elements){
                    requestData.push({
                        name: name,
                        value: _this.forms.info2.elements[name].value || _this.forms.info2.elements[name].dataset.value,
                    })
                }
            }
        }
    };

    Fluid.DynamicView = function(conf, elm){
        this.info = {
            name: "DynamicView",
            id: 13
        };
        this.defaultConf = {
            pagination: true,
            progressBar: true,
            startPage: 0,
            handleByYourself: true
        };
        this.conf = {};
        this.element = null;
        this.initialized = false;
        this.sections = [];
        this.progressBar = null;
        this.pagination = null;
        this.currentIndex = null;
        this.prevCurrentIndex = null;

        Fluid.checkCaller(this);
        Fluid.addToElements(this);

        if (Fluid.Lib.is(conf)) {
            this.conf = conf;
            Fluid.Loader.mergeConfiguration(this);

            this.conf.startPage = parseInt(this.conf.startPage, 10);
            this.currentIndex = this.conf.startPage;
            this.prevCurrentIndex = this.currentIndex;

            if(Fluid.Lib.is(elm)){
                this.element = elm;
            }

            this.init();

            return this;
        }
        else {
            Fluid.Message(13, 0);
            return Fluid;
        }
    };
    Fluid.DynamicView.prototype.init = function(){
        var _this = this;

        if (this.initialized === false) {
            if(this.element !== null){
                this.element.classList.add("dynamicView");

                initSections();
                this.conf.pagination === true ? initPagination() : null;
                this.conf.progressBar === true ? initProgressBar() : null;

                this.updatePagination(this.currentIndex);
                this.updateProgressBar(this.currentIndex);

            }
            else{
                //error
            }

            this.initialized = true;
        }
        else{
            //error
        }

        function initSections(){
            var elements, section, index = 0;

            elements = Fluid.Lib.toArray(_this.element.getElementsByTagName("*"));
            elements.forEach(function(element){
                if(element.dataset.type === "section"){
                    element.classList.add("section");
                    element.removeAttribute("data-type");
                    section = new Fluid.DynamicView.Section(element, _this);

                    _this.conf.startPage !== index ? section.hide(true) : null;

                    _this.sections.push(section);

                    index++;
                }
            });
        }
        function initPagination(){
            var paginationBlock, childBlock, nextButton, prevButton;

            paginationBlock = document.createElement("ul");
            paginationBlock.classList.add("pagination");

            if(_this.sections.length){
                _this.sections.forEach(function(section, index){
                    childBlock = document.createElement("li");
                    childBlock.innerHTML = index + 1;
                    childBlock.index = index;
                    childBlock.classList.add("child", "hidden-xs");

                    _this.addListener("paginationChild", childBlock);

                    paginationBlock.appendChild(childBlock);
                })
            }

            prevButton = document.createElement("li");
            nextButton = document.createElement("li");

            prevButton.classList.add("prevButton");
            nextButton.classList.add("nextButton");

            prevButton.innerHTML = "Wstecz";
            nextButton.innerHTML = "Dalej";

            _this.addListener("prevButton", prevButton);
            _this.addListener("nextButton", nextButton);

            paginationBlock.insertBefore(prevButton, paginationBlock.childNodes[0]);
            paginationBlock.appendChild(nextButton);

            _this.element.appendChild(paginationBlock);

            _this.pagination = paginationBlock;
            _this.pagination.prevButton = prevButton;
            _this.pagination.nextButton = nextButton;
        }
        function initProgressBar(){
            var progressBar, percentageBlock;

            progressBar = document.createElement("div");
            progressBar.classList.add("progressBar");

            percentageBlock = document.createElement("div");
            percentageBlock.classList.add("percentageBlock");

            progressBar.appendChild(percentageBlock);
            _this.element.insertBefore(progressBar, _this.element.childNodes[0]);

            _this.progressBar = percentageBlock;
        }
    };
    Fluid.DynamicView.prototype.addListener = function(type, elm, callback){
        var _this = this;

        if(Fluid.Lib.is(type)){
            switch (type){
                case "paginationChild":
                    if(Fluid.Lib.is(elm)){
                        elm.addEventListener("click", function(){
                            var event;

                            _this.prevCurrentIndex = _this.currentIndex;
                            _this.currentIndex = elm.index;

                            if(_this.conf.handleByYourself === true){
                                _this.updateProgressBar(_this.currentIndex);
                                _this.updatePagination(_this.currentIndex);
                                _this.sections[_this.currentIndex].show();
                            }
                            else{
                                event = new CustomEvent('paginationStep', {'detail': _this});
                                document.dispatchEvent(event);
                            }
                        });
                    }
                    break;
                case "nextButton":
                    if(Fluid.Lib.is(elm)){
                        elm.addEventListener("click", function(){
                            var event;

                            if(_this.conf.handleByYourself === true){
                                if(_this.currentIndex < _this.sections.length) {
                                    _this.prevCurrentIndex = _this.currentIndex;
                                    _this.currentIndex++;
                                    _this.updateProgressBar(_this.currentIndex);
                                    _this.updatePagination(_this.currentIndex);
                                }
                                else{
                                    history.back();
                                }
                                _this.sections[_this.currentIndex].show();
                            }
                            else{
                                _this.prevCurrentIndex = _this.currentIndex;
                                event = new CustomEvent('nextView', {'detail': _this});
                                document.dispatchEvent(event);
                            }
                        });
                    }
                    break;
                case "prevButton":
                    if(Fluid.Lib.is(elm)){
                        elm.addEventListener("click", function(){
                            var event;

                            if(_this.conf.handleByYourself === true){
                                if(_this.currentIndex > 0){
                                    _this.prevCurrentIndex = _this.currentIndex;
                                    _this.currentIndex--;
                                    _this.updateProgressBar(_this.currentIndex);
                                    _this.updatePagination(_this.currentIndex);
                                }
                                else{
                                    history.back();
                                }
                                _this.sections[_this.currentIndex].show();
                            }
                            else{
                                _this.prevCurrentIndex = _this.currentIndex;
                                event = new CustomEvent('prevView', {'detail': _this});
                                document.dispatchEvent(event);
                            }
                        });
                    }
                    break;
            }
        }
        else{

        }
    };
    Fluid.DynamicView.prototype.updateProgressBar = function(index) {
        if(this.progressBar !== null){
            if(this.sections.length){
                this.progressBar.style.width = (index/this.sections.length) * 100 + "%";
            }
            else{
                this.progressBar.style.width = "100%";
            }
        }
    };
    Fluid.DynamicView.prototype.updatePagination = function(index) {
        var children;

        if(this.pagination !== null){
            children = Fluid.Lib.toArray(this.pagination.getElementsByClassName("child"));
            children.forEach(function(child, key){

                if(key === index){
                    child.classList.add("active");
                }
                else{
                    child.classList.remove("active");
                }
            });
        }
    };
    Fluid.DynamicView.prototype.hideAllSections = function() {
        if(this.sections.length){
            this.sections.forEach(function(section){
                section.element.style.display = "none";
            })
        }
    };
    Fluid.DynamicView.Section = function(element, parent){
        this.parent = parent;
        this.element = element;
    };
    Fluid.DynamicView.Section.prototype.show = function(){
        this.parent.hideAllSections();
        this.element.style.display = "block";
        this.element.classList.add("showSection")
    };
    Fluid.DynamicView.Section.prototype.hide = function(hideFast){
        if(hideFast === true){
            this.element.style.display = "none";
        }
        else{

        }
    };

    Fluid.History = function(){
        this.info = {
            name: "History",
            id: 8
        };
        this.history = null;
        this.historyData = [];
        this.initialized = false;

        this.init();
    };
    Fluid.History.prototype.init = function(){
        if(this.initialized === false){
            this.refreshData();
            this.initialized = true;
        }
    };
    Fluid.History.prototype.add = function(data){
        var current;

        this.refreshData();

        current = this.current();

        if(current !== undefined){
            if(typeof(data) === "object"){
                if(current.url !== data.url){
                    this.historyData.push(data);
                    sessionStorage.setItem("customHistory", JSON.stringify(this.historyData));
                }
            }
            else if(typeof(data) === "string"){
                if(current !== data){
                    this.historyData.push(data);
                    sessionStorage.setItem("customHistory", JSON.stringify(this.historyData));
                }
            }
        }
        else{
            this.historyData.push(data);
            sessionStorage.setItem("customHistory", JSON.stringify(this.historyData));
        }
    };
    Fluid.History.prototype.prev = function(){
        var prev;

        this.refreshData();

        if(this.historyData.length - 2 >= 0){
            this.historyData.pop();
            prev = this.historyData[this.historyData.length - 1]
        }
        else if(this.historyData.length === 1){
            prev = this.historyData.pop();
        }
        else{
            prev = null;
        }

        sessionStorage.setItem("customHistory", JSON.stringify(this.historyData));

        return prev;
    };
    Fluid.History.prototype.current = function(){
        this.refreshData();

        return this.historyData[this.historyData.length - 1];
    };
    Fluid.History.prototype.refreshData = function(){
        this.history = sessionStorage.getItem("customHistory");

        if(!this.history){
            sessionStorage.setItem("customHistory", JSON.stringify([]));
            this.history = sessionStorage.getItem("customHistory");
        }

        this.historyData = JSON.parse(this.history);
    };

    Fluid.Navigation = function(conf, elm, slaveNav){
        this.info = {
            name: "Navigation",
            id: 19
        };
        this.defaultConf = {
            onContentChange: function(response){}
        };
        this.conf = {};
        this.element = null;
        this.activeUrl = null;
        this.responseData = null;
        this.callers = [];
        this.initialized = false;
        this.animEventSetted = false;
        this.lockBack = false;
        this.twinNav = null;
        this.slaveNav = slaveNav || false;
        this.pageDuringLoad = false;

        Fluid.checkCaller(this);
        Fluid.addToElements(this);

        if (Fluid.Lib.is(conf)) {
            this.conf = conf;
            Fluid.Loader.mergeConfiguration(this);

            if(Fluid.Lib.is(elm)){
                this.element = elm;
            }

            this.init();
            this.addListener();

            return this;
        }
        else {
            Fluid.Message(19, 0);
            return Fluid;
        }
    };
    Fluid.Navigation.prototype.init = function(){
        var _this = this;

        if (this.initialized === false) {
            if(this.element !== null){
                this.element.classList.add("navigation");

                initHistory();
                initCallableElements();
            }
            else{
                //error
            }

            this.initialized = true;
        }
        else{
            //error
        }

        function initHistory(){
            _this.activeUrl = window.location.pathname;

            if(_this.slaveNav === false){
                Fluid.History.add({url: _this.activeUrl, firstSite: true});
                history.pushState({url: _this.activeUrl, firstSite: true}, "Fluid site", _this.activeUrl);
            }
        }
        function initCallableElements(){
            var elements = Fluid.Lib.toArray(_this.element.getElementsByTagName("a"));

            elements.forEach(function(elm){
                var caller = elm.dataset.navUrl,
                    staticLink = elm.dataset.navStatic;

                if(caller){
                    elm.toUrl = caller;
                    elm.removeAttribute("data-nav-url");
                    elm.classList.add("navCaller");
                    _this.callers.push(elm);

                    if(staticLink == 1){
                        elm.setAttribute("href", caller);
                    }
                    else{
                        elm.lock = false;
                        _this.addListener("caller", elm);

                    }
                }
            });

            _this.setActiveCaller(true);
        }
    };
    Fluid.Navigation.prototype.addListener = function(type, elm, callback){
        var _this = this;

        if(Fluid.Lib.is(type)){
            switch (type){
                case "caller":
                    if(Fluid.Lib.is(elm)){
                        elm.addEventListener("click", function(e){
                            e = e || window.event;
                            e.preventDefault();

                            if(Fluid.Lib.is(elm.toUrl) && elm.lock === false){
                                _this.load(elm.toUrl);
                            }
                            else{
                                //error
                            }

                            return false;
                        }, false);
                    }
                    break;
                case "animationEnd":
                    if(Fluid.Lib.is(elm) ) {
                        elm.addEventListener("animationend", function (e) {
                            if(Fluid.Lib.isFunction(callback)){
                                //all action happend in callback
                                callback(e);
                            }
                        }, false);
                    }
                    break;
            }
        }
        else{
            if(this.slaveNav === false){
                window.addEventListener("popstate", function(e){
                    var ev = e || window.event,
                        data;

                    if(_this.pageDuringLoad === false){
                        data = Fluid.History.prev();

                        if(data.firstSite === false || data.firstSite === true){
                            _this.load(data.url);
                        }
                        else{
                            location.href = (data !== null) ? data.url : "/";
                        }
                    }
                    else{
                        history.replaceState({}, "Fluid site", Fluid.History.current().url)
                    }

                    ev.preventDefault();
                    return false;
                }, false);
            }
            else{
                Fluid.Message(19,1);
            }
        }
    };
    Fluid.Navigation.prototype.load = function(url, back){
        var _this = this,
            httpConf = {};

        back = back || false;

        if(Fluid.Lib.is(url)){
            if(this.activeUrl !== url){
                httpConf = {
                    url: url,
                    toSend: "data=" + JSON.stringify({
                        command: "getContent"
                    }),
                    onResponse: function(response){
                        if(back !== true){
                            history.pushState({url: url, firstSite: false}, "Fluid site", url);
                            Fluid.History.add({url: url, firstSite: false});
                        }

                        _this.responseData = JSON.parse(response);
                        _this.activeUrl = url;
                        (_this.twinNav !== null) ? _this.twinNav.activeUrl = url : null;
                        _this.setActiveCaller();
                        _this.changeContent();

                        if(Fluid.Lib.isFunction(_this.conf.onContentChange)){
                            _this.conf.onContentChange(_this.responseData);
                        }
                    }
                };

                this.pageDuringLoad = true;
                Fluid.Http.post(httpConf)
            }
        }
        else{
            //error
        }
    };
    Fluid.Navigation.prototype.setActiveCaller = function(firstUse, twin){
        var _this = this;

        if(!firstUse || firstUse !== true){
            this.callers.forEach(function(caller){
                caller.classList.remove("active");
            });
        }

        this.callers.forEach(function(caller){
            if(caller.toUrl == _this.activeUrl){
                caller.classList.add("active");
                return true;
            }
        });

        if(!twin){
            (_this.twinNav !== null) ? _this.twinNav.setActiveCaller(false, true) : null;
        }
    };
    Fluid.Navigation.prototype.changeContent = function(){
        var _this = this, backgroundView, content, newContent, head, body, style, script, contentParent, fileExists;

        if(this.responseData !== null){
            head = document.getElementsByTagName("head")[0];
            body = document.getElementsByTagName("body")[0];

            lockCallers();
            setContentPosition();
            createBackgroundView();
            handleMeta();

            content.classList.add("shrink")

            _this.animationType = "shrink";
            _this.addListener("animationEnd", content, function(e){
                var ev = e || window.event;

                if(ev.target === content){
                    content.classList.remove("shrink");

                    newContent = document.createElement("div");
                    newContent.id = "content";
                    newContent.setAttribute("class", content.getAttribute("class"));
                    newContent.innerHTML = _this.responseData.data;
                    newContent.style.cssText = content.style.cssText;

                    contentParent = content.parentNode;

                    if(contentParent !== null){
                        contentParent.removeChild(content);
                        contentParent.appendChild(newContent);
                    }

                    if(_this.responseData.scriptFiles.length){
                        _this.responseData.scriptFiles.forEach(function(file, index){
                            fileExists = false;

                            Fluid.Browser.scripts.forEach(function(script){
                                if(script.getAttribute("src") === file){
                                    fileExists = true;
                                    runScriptAfterLoad(index);
                                }
                            });

                            if(fileExists === false){
                                script = document.createElement("script");
                                script.setAttribute("src", file);

                                body.appendChild(script);

                                script.onload = function(){runScriptAfterLoad(index);}
                            }
                        });

                        Fluid.Browser.updateScripts();
                    }
                    if(_this.responseData.styleFiles.length){
                        _this.responseData.styleFiles.forEach(function(file){
                            fileExists = false;

                            Fluid.Browser.styles.forEach(function(style){
                                if(style.getAttribute("href") === file){
                                    fileExists = true;
                                }
                            });

                            if(fileExists === false){
                                style = document.createElement("link");
                                style.setAttribute("href", file);

                                head.appendChild(style);
                            }
                        });

                        Fluid.Browser.updateStyles();
                    }
                    if(_this.responseData.style.length){
                        _this.responseData.style.forEach(function(data){
                            if(document.getElementsByClassName("style_" + _this.responseData.id).length === 0){
                                style = document.createElement("style");
                                style.innerHTML = data;
                                style.classList.add("style_" + _this.responseData.id);

                                head.appendChild(style);
                            }
                        });
                    }

                    _this.animationType = "grow";
                    newContent.classList.add("grow");
                    newContent.addEventListener("animationend", function (e) {
                        var ev = e || window.event;

                        if(ev.target === newContent){
                            switch(_this.animationType){
                                case "grow":
                                    newContent.classList.remove("grow");

                                    if(backgroundView && backgroundView.parentNode){
                                        backgroundView.parentNode.removeChild(backgroundView);
                                    }

                                    unlockCallers();
                                    _this.pageDuringLoad = false;

                                    break;
                            }
                        }
                    }, false);
                }
            });
        }

        function runScriptAfterLoad(index){
            var existingElement;

            if(_this.responseData.script.length){
                if(index === _this.responseData.scriptFiles.length - 1){
                    _this.responseData.script.forEach(function(data, key) {
                        if (data !== null) {
                            existingElement = document.getElementsByClassName("script_" + _this.responseData.id + "_" + key);

                            if(existingElement.length === 0){
                                script = document.createElement("script");
                                script.innerHTML = data;
                                script.classList.add("script_" + _this.responseData.id + "_" + key);

                                body.appendChild(script);
                            }
                            else{
                                if(_this.responseData.reloadScript === true){
                                    existingElement[0].parentNode.removeChild(existingElement[0]);

                                    script = document.createElement("script");
                                    script.innerHTML = data;
                                    script.classList.add("script_" + _this.responseData.id + "_" + key);

                                    body.appendChild(script);
                                }
                            }
                        }
                    });
                }
            }
        }
        function handleMeta(){
            var title, keywords, description;

            title = document.getElementsByTagName("title")[0];
            keywords = document.getElementsByName("keywords");
            description = document.getElementsByName("description");

            if(_this.responseData.meta){
                title.innerHTML = _this.responseData.meta.title;

                if(keywords.length){
                    keywords[0].setAttribute("content", _this.responseData.meta.keywords);
                }

                if(description.length){
                    description[0].setAttribute("content", _this.responseData.meta.description);
                }
            }
        }
        function setContentPosition(){
            content = document.getElementById("content");
            content.prop = content.getBoundingClientRect();

            content.style.position = "absolute";
            content.style.top = content.prop.top + "px";
            content.style.left = content.prop.left + "px";
        }
        function createBackgroundView(){
            var element = document.getElementsByClassName("backgroundView");

            if(element && element.length){
                backgroundView = element[0];
            }
            else{
                backgroundView = document.createElement("div");
                backgroundView.classList.add("backgroundView");
            }

            backgroundView.style.position = "absolute";
            backgroundView.style.top = content.prop.top + "px";
            backgroundView.style.left = content.prop.left + "px";

            Fluid.wrapper.appendChild(backgroundView);
        }
        function lockCallers(){
            _this.callers.forEach(function(caller){
                caller.lock = true;
            });

            _this.lockBack = true;
        }
        function unlockCallers(){
            _this.callers.forEach(function(caller){
                caller.lock = false;
            });

            _this.lockBack = false;
        }
    };

    Fluid.Modal = function (conf, elm) {
        this.info = {
            name: "Modal",
            id: 10
        };
        this.defaultConf = {
            show: true,
            hide: false,
            close: false,
            header: "Example title",
            content: "Example content.",
            contentAjax: false,
            footer: false,
            create: false,
            actions: {
                events: "hide",
                parent: "header"
            },
            animation: true,
            type: "standard",
            promptSendButtonText: "Wyślij",
            promptInputValue: "",
            dim: false,
            onHide: function(){},
            onClose: function(){},
            onPromptSubmit: function (value) {
            }
        };
        this.conf = conf || {};
        this.element = null;
        this.hidden = false;
        this.initialized = false;
        this.blocks = [];
        this.actions = [];
        this.lastAction = null;

        Fluid.checkCaller(this);
        Fluid.addToElements(this);

        Fluid.Loader.mergeConfiguration(this);

        if (Fluid.Lib.is(elm)) {
            this.element = elm;
        }
        else {
            this.create();
        }

        this.init();
        this.addListeners();

        return this;
    };
    Fluid.Modal.prototype.init = function () {
        var _this = this,
            content,
            tempActions;

        if (this.initialized === false) {
            this.hide(true);
            this.element.classList.contains("fluid-modal") === false ? this.element.classList.add("fluid-modal") : null;
            this.element.classList.add("modal" + this.conf.type.substr(0, 1).toUpperCase() + this.conf.type.substr(1));

            if (this.conf.createdBy === "loader") {
                content = this.element.innerHTML;

                if (content.length) {
                    this.conf.content = content;
                    this.element.innerHTML = "";
                }

                if (typeof(this.conf.actions) === "string") {
                    tempActions = this.conf.actions;
                    this.conf.actions = {
                        events: tempActions
                    };
                }

                if (this.conf.actionsPosition) {
                    this.conf.actions.position = this.conf.actionsPosition;
                }

                if (this.conf.actionsParent) {
                    this.conf.actions.parent = this.conf.actionsParent;
                }

                initHeader();
                initContent();
            }

            initActions();
            initBlocks();
            initVisibility();

            this.addListeners("shortcuts");
            this.initialized = true;
        }
        else {
            Fluid.Message(10, 5);
        }

        function initHeader() {
            if (_this.conf.header) {
                _this.create("header");
            }
        }

        function initActions() {
            var events, positions, parent;

            if (_this.conf.actions) {
                if (_this.conf.actions.events) {
                    if (typeof(_this.conf.actions.events) === "string") {
                        events = _this.conf.actions.events.split(" ");
                        _this.conf.actions.events = [];
                        events.forEach(function (event) {
                            _this.conf.actions.events.push(event.trim());
                        });
                    }
                    else {
                        //error
                    }
                }
                else {
                    _this.conf.actions.events = _this.defaultConf.actions.events;
                    return initActions();
                }

                /*if(_this.conf.actions.position){
                 if(typeof(_this.conf.actions.position) === "string"){
                 positions = _this.conf.actions.position.trim().split(" ");
                 _this.conf.actions.position = {};

                 if(positions.length === 2){
                 _this.conf.actions.position.x = positions[0];
                 _this.conf.actions.position.y = positions[1];
                 }
                 else if(positions.length === 1 && positions[0] === "center"){
                 _this.conf.actions.position.x = positions[0];
                 _this.conf.actions.position.y = positions[0];
                 }
                 else{
                 //return error?
                 }
                 }
                 else{
                 //error!
                 }

                 }
                 else{
                 _this.conf.actions.position = _this.defaultConf.actions.position;
                 return initActions();
                 }*/

                if (_this.conf.actions.parent && typeof(_this.conf.actions.parent) === "string") {
                    parent = _this.conf.actions.parent.trim();

                    if (parent.length && _this.blocks.length) {
                        _this.conf.actions.parent = null;

                        _this.blocks.forEach(function (block) {
                            switch (parent[0]) {
                                case ".":
                                    parent = parent.substr(1);
                                    _this.conf.actions.parent = block.getElementsByClassName(parent);
                                    return;

                                case "#":
                                    _this.conf.actions.parent = block.getElementById(parent);
                                    return;
                            }
                        });

                        if (_this.conf.actions.parent === null) {
                            _this.blocks.forEach(function (block) {
                                if (block.blockType === parent) {
                                    _this.conf.actions.parent = block;
                                    return;
                                }
                            });
                        }

                        if (_this.conf.actions.parent === null) {
                            _this.blocks.forEach(function (block) {
                                if (block.blockType === "header") {
                                    _this.conf.actions.parent = block;
                                    return;
                                }
                            });
                        }

                        if (_this.conf.actions.parent === null) {
                            _this.conf.actions.parent = _this.element;
                        }
                    }
                    else {
                        _this.conf.actions.parent = _this.element;
                    }
                }
                else {
                    _this.conf.actions.parent = _this.defaultConf.actions.parent;
                    return initActions();
                }

                _this.create("actions");
            }
        }

        function initContent() {
            if (_this.conf.contentAjax) {
                if (_this.conf.content === false) {
                    Fluid.Http.post({
                        url: _this.conf.contantAjax,
                        onResponse: function(response){
                            _this.conf.content = response;
                            _this.create("content");
                        }
                    });
                }
                else {
                    Lib.Message(10, 4);
                }
            }
            else if (_this.conf.content) {
                _this.create("content")
            }
        }

        function initBlocks() {
            if (_this.blocks.length) {
                _this.blocks.forEach(function (block) {
                    _this.element.appendChild(block);
                });
            }
        }

        function initVisibility() {
            if (_this.conf.show) {
                if (_this.conf.hide === false) {
                    _this.show();
                }
            }
            else {
                if (_this.conf.hide) {
                    Fluid.Message(10, 2);
                }
            }
        }
    };
    Fluid.Modal.prototype.create = function (toCreate) {
        var header, title, content, actions, action, wrap,
            prompt, promptInput, promptSendButton,
            _this = this;

        if (Fluid.Lib.is(toCreate) === false) {
            toCreate = ['all'];
        }
        else if (typeof(toCreate) === "string") {
            toCreate = [toCreate];
        }

        toCreate.forEach(function (val) {
            switch (val) {
                case "header":
                    if (_this.conf.header instanceof HTMLElement) {
                        header = _this.conf.header;

                        header.classList.add("modal-header");
                        header.blockType = "header";

                        _this.element.appendChild(header);
                    }
                    else if (typeof(_this.conf.header) === "string") {
                        header = document.createElement("header");
                        title = document.createElement("span");

                        header.classList.add("modal-header");
                        header.blockType = "header";
                        title.classList.add("modal-title");
                        title.innerHTML = _this.conf.header;

                        header.appendChild(title);
                        _this.blocks.push(header);
                    }
                    break;
                case "content":
                    if (_this.conf.content instanceof HTMLElement) {
                        _this.blocks.push(_this.conf.content);
                    }
                    else if (typeof(_this.conf.content) === "string") {
                        content = document.createElement("section");

                        content.classList.add("modal-content");
                        content.blockType = "content";
                        content.innerHTML = _this.conf.content;

                        _this.blocks.push(content);

                        switch (_this.conf.type) {
                            case "prompt":
                                promptInput = document.createElement("input");
                                promptSendButton = document.createElement("button");

                                promptInput.setAttribute("name", "promptValue");
                                promptInput.classList.add("promptValue");
                                promptSendButton.classList.add("promptSendButton");

                                _this.promptInput = promptInput;
                                _this.promptInput.value = _this.conf.promptInputValue;
                                _this.promptSendButton = promptSendButton;
                                _this.promptSendButton.innerHTML = _this.conf.promptSendButtonText;

                                _this.promptSendButton.addEventListener("click", function () {
                                    _this.conf.onPromptSubmit(_this.promptInput.value);
                                });

                                content.appendChild(promptInput);
                                content.appendChild(promptSendButton);
                                break;

                            case "alert":
                                promptSendButton = document.createElement("button");

                                promptSendButton.classList.add("promptSendButton");

                                _this.promptSendButton = promptSendButton;
                                _this.promptSendButton.innerHTML = _this.conf.promptSendButtonText;

                                _this.promptSendButton.addEventListener("click", function () {
                                    _this.conf.onPromptSubmit();
                                });

                                content.appendChild(promptSendButton);
                                break;

                            case "modal": //rename to default?

                                break;
                        }
                    }
                    else if (_this.conf.content === false) {
                        _this.element.innerHTML = "";
                    }
                    break;
                case "actions":
                    if (_this.conf.actions) {
                        actions = document.createElement("div");
                        actions.classList.add("modal-actions");

                        if (_this.conf.actions.events) {
                            _this.conf.actions.events.forEach(function (event) {
                                action = document.createElement("span");
                                action.classList.add("action", event);
                                action.actionType = event;
                                _this.actions.push(action);

                                if(event === "hide" || event === "close"){
                                    _this.lastAction = event;
                                }

                                actions.appendChild(action);
                            })
                        }

                        if (_this.conf.actions.parent) {
                            _this.conf.actions.parent.appendChild(actions);
                        }
                    }

                    break;
                case "all":
                    wrap = document.createElement("div");

                    _this.element = wrap;
                    _this.create(["header", "content"]);
                    Fluid.wrapper.appendChild(_this.element);
                    break;
            }
        });
    };
    Fluid.Modal.prototype.addListeners = function (type) {
        var _this = this;

        switch(type){
            case "shortcuts":
                document.addEventListener("keyup", function(e){
                    if(e.keyCode == 27){
                        if(_this.lastAction !== null){
                            _this[_this.lastAction]();
                        }
                    }
                });
                break;
            default:
                if (this.actions.length) {
                    this.actions.forEach(function (action) {
                        switch (action.actionType) {
                            case "close":
                                action.addEventListener("click", function () {
                                    _this.close();
                                });
                                action.addEventListener("click", _this.conf.onClose);
                                break;
                            case "hide":
                                action.addEventListener("click", function () {
                                    _this.hide();
                                });
                                action.addEventListener("click", _this.conf.onHide);
                                break;
                        }
                    });
                }
                break;
        }
    };
    Fluid.Modal.prototype.show = function () {
        Fluid.Browser.hideModals();

        if(this.conf.dim === true){
            var dim;

            dim = new Fluid.Dim({
                opacityWhenShowed: 0.6
            });
            dim.show();
        }

        this.element.classList.remove("modalCloseNoAnim");
        this.element.classList.remove("modalClose");

        if (this.conf.animation) {
            this.element.classList.add("modalOpen");
            this.hidden = false;
        }
        else {
            this.element.classList.remove("modalCloseNoAnim");

            if (this.hidden) {
                this.element.classList.add("modalOpen");
                this.element.style.zIndex = "100000";
                this.element.style.visibility = "visible";
                this.hidden = false;
            }
            else {
                Fluid.Message(10, 3);
            }
        }
    };
    Fluid.Modal.prototype.hide = function (hideFast) {
        var hideFast = hideFast || false;

        if(this.conf.dim === true && Fluid.Browser.dim !== null){
            Fluid.Browser.dim.hide();
        }

        if (hideFast) {
            this.element.classList.add("modalCloseNoAnim");
        }
        else {
            this.element.classList.remove("modalCloseNoAnim");
            this.element.classList.add("modalClose");
        }

        this.hidden = true;
    };
    Fluid.Modal.prototype.close = function (hideFast) {
        var _this = this;

        this.hide(hideFast);

        this.element.addEventListener("animationend", function(){
            this.parentNode.removeChild(_this.element);
        });
    };

    Fluid.TabbedView = function(conf, elm){
        //temporary only from HTML loader
        this.info = {
            name: "TabbedView",
            id: '21'
        };
        this.defaultConf = {
            default: 1 //first tab
        };
        this.conf = conf || {};
        this.currentTab = null;
        this.previousTab = null;
        this.triggers = [];
        this.sections = [];
        this.element = null;
        this.initialized = false;

        Fluid.checkCaller(this);
        Fluid.addToElements(this);

        Fluid.Loader.mergeConfiguration(this);

        if(Fluid.Lib.is(elm)){
            this.element = elm;
        }

        this.init();

        return this;
    };
    Fluid.TabbedView.prototype.init = function(){
        var _this = this;

        if (this.initialized === false) {
            if(this.element !== null){
                this.element.classList.add("tabbedView");
                handleControls();
                handleSections();
                setUpDefault();
            }
            else{
                //error
            }

            this.initialized = true;
        }
        else{
            //error
        }

        function handleControls(){
            var controls = Fluid.Lib.toArray(_this.element.getElementsByClassName("controls"));

            if(controls.length){
                controls.forEach(function(control){
                    var elements = Fluid.Lib.toArray(control.getElementsByTagName("*"));

                    elements.forEach(function(element){
                        if(element.dataset.trigger){
                            element.section = element.dataset.trigger;
                            element.removeAttribute("data-trigger");

                            _this.addListener("trigger", element);
                            _this.triggers.push(element);
                        }
                    })
                });
            }
        }
        function handleSections(){
            var sections = Fluid.Lib.toArray(_this.element.getElementsByClassName("sections"));

            sections = (sections.length) ? sections[0] : null;

            if(sections !== null){
                var elements = Fluid.Lib.toArray(sections.getElementsByTagName("*"));

                elements.forEach(function(element){
                    if(element.dataset.section){
                        element.trigger = element.dataset.section;
                        element.id = element.dataset.section + "Section";
                        element.removeAttribute("data-section");

                        _this.sections.push(element);
                    }
                })
            }
        }
        function setUpDefault(){
            if(_this.conf.default && _this.triggers.length){
                _this.triggers.forEach(function(trigger){
                    if(trigger.section === _this.conf.default){
                        _this.currentTab = trigger.section;
                    }
                });

                if(_this.currentTab === null){
                    _this.currentTab = _this.triggers[0].section
                }

                _this.showTab();
            }
        }
    };
    Fluid.TabbedView.prototype.showTab = function(){
        var _this = this, found = false;

        this.hideTabs();

        if(this.sections.length){
            this.sections.forEach(function(section){
                if(section.trigger === _this.currentTab){
                    section.style.display = "block";
                    found = true;
                }
            });

            if(found === true){
                if(this.triggers.length){
                    this.triggers.forEach(function(trigger){
                        if(trigger.section === _this.currentTab){
                            trigger.classList.add("active");
                        }
                    })
                }
            }
            else{
                new Fluid.Notify("Nie można odnaleźć treści o indeksie: " + this.currentTab + ".", "warning");
                this.showPreviousTab();
            }
        }
    };
    Fluid.TabbedView.prototype.hideTabs = function(){
        if(this.sections.length){
            this.sections.forEach(function(section){
                section.style.display = "none";
            })
        }

        if(this.triggers.length){
            this.triggers.forEach(function(trigger){
                trigger.classList.remove("active");
            })
        }
    };
    Fluid.TabbedView.prototype.showPreviousTab = function(){
        if(this.previousTab !== null){
            this.currentTab = this.previousTab;
            this.showTab();
        }
    };
    Fluid.TabbedView.prototype.addListener = function(type, elm){
        var _this = this;

        if(Fluid.Lib.is(type)){
            switch (type){
                case "trigger":
                    if(Fluid.Lib.is(elm)){
                        elm.addEventListener("click", function(){
                            if(_this.currentTab !== elm.section){
                                _this.previousTab = _this.currentTab;
                                _this.currentTab = elm.section;
                                _this.showTab();
                            }
                        });
                    }
                    break;
            }
        }
    };

    Fluid.Form = function (conf, elm)  {
        this.info = {
            name: "Form",
            id: 15
        };
        this.defaultConf = {
            ajax: true,
            submitButton: null,
            create: false,
            attributes: {},
            clientValidation: false
        };
        this.conf = {};
        this.element = null;
        this.elements = {};
        this.radios = [];
        this.filesPreview = {};
        this.dataToSend = null;
        this.submitButton = null;
        this.extraSendData = {};
        this.initialized = false;
        this.validateType = 1;
        this.formName = null;
        this.selects = [];
        this.hasError = false;

        Fluid.checkCaller(this);
        Fluid.addToElements(this);

        if (Fluid.Lib.is(conf)) {
            this.conf = conf;

            Fluid.Loader.mergeConfiguration(this);

            if (Fluid.Lib.is(elm)) {
                this.element = elm;

                this.init();
                this.addListeners("submitButton");
                this.onComplete();
            }
            else if(this.conf.create === true){
                this.create("form");
            }

            this.element.classList.add("form");
            this.element.setAttribute("role", "form");

            return this;
        }
        else {
            Fluid.Message(15, 0);
            return Fluid;
        }
    };
    Fluid.Form.prototype.init = function () {
        var _this = this;

        if (!this.initialized) {
            loadElements();
            this.loadParameters();

            if (Fluid.Lib.is(this.action) && Fluid.Lib.is(this.formName)) {
                if (!Fluid.Lib.is(this.conf.submitButton)) {
                    getSubmitButton();
                }
                else {
                    this.submitButton = document.querySelector(this.conf.submitButton);
                }

                this.initialized = true;
            }
            else {
                throw "error action formName - to do";
                //to do!
            }
        }

        function loadElements() {
            var inputs,
                textareas,
                selects,
                type,
                name;

            inputs = _this.element.querySelectorAll("input");
            textareas = _this.element.querySelectorAll("textarea");
            //selects = _this.element.getElementsByTagName("select");

            if (inputs.length > 0) {
                inputs = Fluid.Lib.toArray(inputs);

                inputs.forEach(function (input) {
                    type = input.getAttribute("type");
                    name = input.getAttribute("name");

                    if (type !== "hidden" && type !== "disabled") {
                        if (type === "submit") {
                            _this.submitButton = input;
                        }
                        else if (type === "checkbox") {
                            handleCheckbox(input);
                            _this.elements[name] = input;
                        }
                        else if (type === "radio") {
                            handleRadio(input);
                            _this.elements[name] = {type: "radio", value: null};
                        }
                        else if (type === "file") {
                            _this.addListeners("file", input)
                            _this.elements[name] = input;
                        }
                        else {
                            _this.elements[name] = input;
                        }
                    }
                });
            }

            if (textareas.length > 0) {
                textareas = Fluid.Lib.toArray(textareas);

                textareas.forEach(function (textarea) {
                    _this.elements[textarea.getAttribute("name")] = textarea;
                })
            }
        }

        function handleRadio(input) {
            var name = input.getAttribute("name"),
                control = input.parentNode.getElementsByClassName("radio-control"),
                radio = null;

            control = Fluid.Lib.toArray(control);
            control.forEach(function (element) {
                if (element.dataset.name === name) {
                    radio = element;
                    radio.dataset.checked = "false";
                    radio.input = input;
                    _this.radios.push(radio);
                }
            });

            if (Fluid.Lib.is(radio)) {
                _this.addListeners("radio", radio);
            }
        }

        function handleCheckbox(input) {
            var name = input.getAttribute("name"),
                control = input.parentNode.getElementsByClassName("checkbox-control"),
                checkbox = null;

            control = Fluid.Lib.toArray(control);
            control.forEach(function (element) {
                if (element.dataset.name === name) {
                    checkbox = element;
                    checkbox.dataset.checked = "false";
                    checkbox.input = input;
                    checkbox.input.value = "0";
                    delete(checkbox.dataset.name);

                    input.checkbox = checkbox;
                }
            });

            if (Fluid.Lib.is(checkbox)) {
                _this.addListeners("checkbox", checkbox);
            }
            else {
                checkbox = document.createElement("div");
                checkbox.classList.add("checkbox-control");
                checkbox.dataset.checked = "false";
                checkbox.input = input;
                checkbox.input.value = "0";

                input.checkbox = checkbox;
                input.parentNode.appendChild(checkbox);

                _this.addListeners("checkbox", checkbox);
            }
        }

        function getSubmitButton() {
            var button;

            if (!Fluid.Lib.is(_this.submitButton)) {
                button = _this.element.getElementsByTagName("button");

                if (button.length) {
                    button = button[0];
                    _this.submitButton = button;
                }
                else {
                    Fluid.Message(15, 1);
                }
            }
        }
    };
    Fluid.Form.prototype.field = function(data){
        var inputElm, field, labelElm, label, labelPosition, select;

        if(Fluid.Lib.is(data)){
            data.type = (Fluid.Lib.is(data.type)) ? data.type : "text";
            label = (Fluid.Lib.is(data.label)) ? data.label : false;
            labelPosition = (Fluid.Lib.is(data.labelPosition)) ? data.labelPosition : "before";

            field = document.createElement("div");
            field.classList.add("form-group");

            switch(data.type){
                case "textarea":
                    inputElm = document.createElement("textarea");
                    inputElm.classList.add("form-control");
                    field.appendChild(inputElm);

                    if(data.value){
                        inputElm.innerHTML = data.value;
                        delete(data.value);
                    }

                    delete(data.type);
                    break;

                case "select":
                    select = new Fluid.List({
                        append: field,
                        mode: 'select',
                        rows: data.rows,
                        onSelect: data.onSelect ? data.onSelect : function(){},
                        injectedValue: data.value,
                        hidden: data.hidden ? data.hidden : false,
                        canHide: data.canHide ? data.canHide : true,
                        orderRows: (data.orderRows !== undefined)? data.orderRows : true,
                        skipDefaultValue: (data.skipDefaultValue !== undefined) ? data.skipDefaultValue : true
                    });
                    select.selectElement.classList.add("form-control");
                    field.classList.add("select-group");
                    this.selects.push({
                        wrapper: field,
                        list: select,
                        selected: select.selectElement
                    });

                    inputElm = select.element;

                    delete(data.onSelect);
                    delete(data.rows);
                    delete(data.type);
                    break;

                default:
                    inputElm = document.createElement("input");
                    inputElm.classList.add("form-control");
                    field.appendChild(inputElm);
                    break;
            }

            if(data.validate){
                inputElm.dataset.validate = data.validate;
                delete(data.validate);
            }

            Fluid.Lib.setAttributes(inputElm, data);

            if(label !== false){
                labelElm = document.createElement("label");
                labelElm.innerHTML = label;

                switch(labelPosition){
                    case "before":
                        field.insertBefore(labelElm, field.childNodes[0]);
                        break;
                    case "after":
                        field.appendChild(labelElm);
                        break;
                }
            }
            this.element.appendChild(field);
            this.elements[data.name] = inputElm;
        }
    };
    Fluid.Form.prototype.loadParameters = function() {
        var action, method, formName;

        action = this.element.getAttribute("action");
        method = this.element.getAttribute("method");
        formName = this.element.getAttribute("id");

        this.action = (Fluid.Lib.is(action)) ? action : null;
        this.method = (Fluid.Lib.is(method)) ? method : "POST";
        this.formName = (Fluid.Lib.is(formName)) ? formName : null;
    };
    Fluid.Form.prototype.create = function(whatToCreate, attributes){
        /*
         This is deprecated and will be removed in the future.
         */
        var createdElement = null,
            extraElement = null,
            _this = this;

        attributes = attributes || {};

        createElement(whatToCreate);
        setAttributes();

        if(this.element !== createdElement){
            this.element.appendChild(createdElement);
        }

        return createdElement;

        function createElement(whatToCreate){
            switch(whatToCreate) {
                case "form":
                    createdElement = document.createElement(whatToCreate);
                    attributes = _this.conf.attributes;

                    _this.element = createdElement;
                    break;

                case "textInput":
                    createdElement = document.createElement("input");
                    addToAttributes("type", "text");
                    break;

                case "fileInput":
                    createdElement = document.createElement("input");
                    extraElement = document.createElement("i");

                    extraElement.classList.add("fa", "fa-upload", "fileControl");

                    _this.element.appendChild(extraElement);

                    addToAttributes("type", "file");
                    addToAttributes("multiple", "multiple");
                    break;
                case "selectInput":

                    break;
            }

            _this.loadParameters();
        }

        function addToAttributes(property, value){
            if(typeof(attributes) === "object"){
                attributes[property] = value;
            }
        }

        function setAttributes(){
            for(var attribute in attributes){
                if(attributes.hasOwnProperty(attribute)){
                    createdElement.setAttribute(attribute, attributes[attribute]);
                }
            }
        }
    };
    Fluid.Form.prototype.append = function(toAppend){
        toAppend.appendChild(this.element);

        this.onComplete();
    };
    Fluid.Form.prototype.addListeners = function (type, element) {
        var _this = this;

        switch (type) {
            case "submitButton":
                if (Fluid.Lib.is(this.submitButton)) {
                    this.submitButton.addEventListener("click", function (e) {
                        if (_this.conf.ajax) {
                            e = e || window.event;
                            _this.prepareData();
                            _this.sendData();
                            e.preventDefault();
                        }
                    }, false);
                }

                break;

            case "checkbox":
                element.addEventListener("click", function () {
                    if (this.dataset.checked === "false") {
                        this.input.value = "1";
                        this.dataset.checked = "true";
                    }
                    else if (this.dataset.checked === "true") {
                        this.input.value = "0";
                        this.dataset.checked = "false";
                    }
                }, false);

                break;

            case "radio":
                element.addEventListener("click", function () {
                    var name = this.dataset.name;

                    _this.elements[name].value = this.input.value;

                    _this.radios.forEach(function (radio) {
                        radio.dataset.checked = "false";
                    });

                    this.dataset.checked = "true";

                }, false);

                break;

            case "file":
                if (Fluid.Lib.is(element)) {
                    element.addEventListener("change", function () {
                        var files, fileData, inputName, filePreviewObj;

                        files = Fluid.Lib.toArray(this.files);
                        files.forEach(function (file) {
                            fileData = new FormData();
                            inputName = element.getAttribute("name");

                            fileData.append("file", file);
                            fileData.append("fileInput", inputName);
                            fileData.append("formName", _this.formName);
                            fileData.append("extraData", _this.extraSendData);

                            filePreviewObj = new Fluid.Form.FilePreview(_this, element, file);

                            _this.fileToSend = fileData;
                            _this.sendFile(filePreviewObj);
                        });
                    }, false);
                }
                break;
        }
    };
    Fluid.Form.prototype.prepareData = function () {
        var normalData = {}, type, formData;

        formData = new FormData();

        for (var index in this.elements) {
            if (this.elements.hasOwnProperty(index)) {
                if (!this.elements[index].hasOwnProperty("type")) {
                    type = this.elements[index].getAttribute("type");
                }

                if (type && type === "file") {
                    normalData[index] = null;
                }
                else {
                    normalData[index] = this.elements[index].value;
                }
            }
        }

        formData.append("formName", this.formName);
        formData.append("extraData", this.extraSendData);
        formData.append("data", JSON.stringify(normalData));

        this.dataToSend = formData;

        if(this.conf.clientValidation === true){
            this.clientValidation();
        }
    };
    Fluid.Form.prototype.sendData = function () {
        var _this = this;

        if((this.conf.clientValidation === true && this.hasError === true) || this.conf.clientValidation === false){
            Fluid.Http.post({
                url: this.action, toSend: this.dataToSend, onResponse: function (response) {
                    _this.processData(response);
                }
            });
        }
    };
    Fluid.Form.prototype.sendFile = function (filePreviewObj) {
        var completed = 0;

        Fluid.Http.post({
            url: this.action, toSend: this.fileToSend, onProgress: function (e) {
                if (e.lengthComputable) {
                    completed = (e.loaded / e.total) * 100;
                    filePreviewObj.progress.style.width = completed + "%";
                }
            }
        });
    };
    Fluid.Form.prototype.processData = function (response) {
        var response = JSON.parse(response),
            processedElement,
            errorBlock,
            _this = this;

        if (response.status === 0) {
            clearErrors();

            response = Fluid.Lib.toArray(response);

            switch (this.validateType) {
                case 1:
                    if (response.length) {
                        response.forEach(function (element) {
                            if (Fluid.Lib.is(_this.elements[element.elementName])) {
                                processedElement = _this.elements[element.elementName];

                                if (processedElement.type === "radio") {
                                    _this.radios.forEach(function (radio) {
                                        radio.classList.add("error");
                                    });
                                }
                                else if (processedElement.getAttribute("type") === "checkbox") {
                                    processedElement.checkbox.classList.add("error");
                                }
                                else {
                                    processedElement.classList.add("error");
                                    processedElement.setAttribute("placeholder", element.message);
                                }
                            }

                            if (element.elementName === "form") {
                                errorBlock = document.createElement("div");
                                errorBlock.classList.add("errorBlock");
                                errorBlock.innerHTML = element.message;

                                _this.element.appendChild(errorBlock);
                            }
                        });
                    }

                    break;
            }
        }
        else if (response.status === 1) {
            if (response.backUrl) {
                window.location.href = response.backUrl;
            }
        }

        function clearErrors() {
            var processedElement, errorBlock;

            errorBlock = _this.element.getElementsByClassName("errorBlock");

            switch (_this.validateType) {
                case 1:
                    for (index in _this.elements) {
                        processedElement = _this.elements[index];

                        if (processedElement.type === "radio") {
                            _this.radios.forEach(function (radio) {
                                radio.classList.remove("error");
                            });
                        }
                        else if (processedElement.getAttribute("type") === "checkbox") {
                            processedElement.checkbox.classList.remove("error");
                        }
                        else {
                            if (processedElement.classList.contains("error")) {
                                processedElement.classList.remove("error");
                            }

                            processedElement.removeAttribute("placeholder");
                        }
                    }

                    if (errorBlock.length) {
                        errorBlock = Fluid.Lib.toArray(errorBlock);

                        errorBlock.forEach(function (element) {
                            element.parentNode.removeChild(element);
                        })
                    }

                    break;
            }
        }
    };
    Fluid.Form.prototype.onComplete = function(){
        var selectedPos = null;

        if(this.selects.length){
            this.selects.forEach(function(obj){
                selectedPos = obj.selected.getBoundingClientRect();

                obj.list.element.style.width = obj.wrapper.getBoundingClientRect().width + "px";
                obj.list.element.style.maxWidth = "none";
                obj.list.element.style.marginTop = selectedPos.height + "px";
                obj.list.element.style.top = "0px";
            });
        }
    };
    Fluid.Form.prototype.clientValidation = function(){
        var input, validateParams, _this = this;

        if(this.dataToSend !== null){
            this.prepareData();
        }
        else{
            this.hasError = false;

            for(var name in this.elements){
                if(this.elements.hasOwnProperty(name)){
                    input = this.elements[name];

                    if(input.dataset.validate){
                        input.parentNode.classList.remove("has-error");
                        validateParams = input.dataset.validate.split(",");

                        validateParams.forEach(function(param){
                            param = param.trim();

                            _this.validate(input, param);
                        })
                    }
                }
            }
        }

        return !this.hasError;
    };
    Fluid.Form.prototype.validate = function(element, validator){
        var error = null, _this = this;

        switch(validator){
            case "text":
                if(typeof(element.value) !== "string" ){
                    addError("Podana wartość nie jest tekstem.");
                }

                break;

            case "required":
                if(!element.value.length && !element.dataset.value){
                    addError("Pole musi być uzupełnione");
                }

                break;
        }

        function addError(error){
            //element.setAttribute("placeholder", error);
            element.parentNode.classList.add("has-error")

            _this.hasError = true;
        }
    };
    Fluid.Form.FilePreview = function (form, input, file) {
        var name = input.getAttribute("name"),
            previewBlock,
            progressBar,
            inputFilePreview;

        this.element = null;
        this.progress = null;

        if (!form.filesPreview[name]) {
            form.filesPreview[name] = [];
        }

        inputFilePreview = input.parentNode.parentNode.parentNode.getElementsByClassName("filesPreview");

        if (inputFilePreview.length) {
            inputFilePreview = inputFilePreview[0];

            previewBlock = document.createElement("div");
            progressBar = document.createElement("div");

            previewBlock.classList.add("previewBlock");
            progressBar.classList.add("progressBar");

            previewBlock.appendChild(progressBar);
            inputFilePreview.appendChild(previewBlock);

            this.element = previewBlock;
            this.progress = progressBar;

            return this;
        }
    };

    Fluid.GridView = function(conf, elm){
        this.info = {
            name: "GridView",
            id: '20'
        };
        this.defaultConf = {};
        this.conf = conf || {};
        this.element = null;
        this.initialized = false;
        this.actions = [];
        this.filterData = {};
        this.filterChecboxes = [];
        this.modelName = null;
        this.modelData = null;

        Fluid.checkCaller(this);
        Fluid.addToElements(this);

        Fluid.Loader.mergeConfiguration(this);

        if(Fluid.Lib.is(elm)){
            this.element = elm;
        }

        this.init();

        return this;
    };
    Fluid.GridView.prototype.init = function(){
        if (this.initialized === false) {
            if(this.element !== null){
                this.modelName = this.element.dataset.model;
                this.element.classList.add("gridView");

                this.handleActions();
                this.handleFilters();
                this.handleCustomization();
            }
            else{
                //error
            }

            this.initialized = true;
        }
        else{
            //error
        }
    };
    Fluid.GridView.prototype.handleFilters = function(){
        var filters  = this.element.querySelector(".filters");

        if(filters){
            filters.classList.add("hidden-xs")
        }
    };
    Fluid.GridView.prototype.handleCustomization = function(){
        var _this = this, anotherGridViewExists = false;

        Fluid.elements.GridView.forEach(function(gridView){
            if(gridView.modelName == _this.modelName && gridView !== _this){
                anotherGridViewExists = true;

                return true;
            }
        });

        if(!anotherGridViewExists){
            Fluid.Http.get({
                url: '/api/get-model-data.html?name=' + _this.modelName,
                onResponse: function(response){
                    var res = JSON.parse(response),
                        customizeBtn, ths, lastTh;

                    if(res && response.length > 2){
                        Fluid.elements.GridView.forEach(function(gridView){
                            if(gridView.modelName == _this.modelName){
                                ths = gridView.element.querySelectorAll("thead th");
                                customizeBtn = document.createElement("i");

                                if(ths.length){
                                    lastTh = ths[ths.length - 1];
                                    lastTh.innerHTML = "";

                                    customizeBtn.classList.add("fa", "fa-cogs", "customizeBtn", "hidden-xs");

                                    lastTh.appendChild(customizeBtn);
                                    gridView.modelData = res;
                                    gridView.addListener("customize", customizeBtn);
                                }
                            }
                        });
                    }
                }
            });
        }
    };
    Fluid.GridView.prototype.handleActions = function(){
        var actions, statusActions, priorityActions, _this = this;

        actions = Fluid.Lib.toArray(this.element.getElementsByClassName("action"));

        refreshTable();

        if(actions.length){
            this.actions = [];

            actions.forEach(function(action){
                var key, url, text;

                if(action.parentNode && action.parentNode.parentNode){
                    key = action.parentNode.parentNode.dataset.key;
                    url = action.getAttribute("href");
                    text = action.getAttribute("title");

                    if(key){
                        if(!_this.actions[key]){
                            _this.actions[key] = [];
                        }

                        _this.actions[key].push({
                            url: url ? url : "#",
                            text: text,
                            default: action.classList.contains("default"),
                            noRedirect: action.classList.contains("noRedirect"),
                            confirm: action.classList.contains("confirm"),
                            confirmContent: action.dataset.content,
                            emitter: action.parentNode.parentNode.querySelector(".ordinaryActionsBtn").parentNode,
                            contextMenuEmitter: action.parentNode.parentNode,
                            originalNode: action.cloneNode(true)
                        });
                    }
                }
            });

            if(this.actions.length) {
                this.actions.forEach(function(actionData){
                    if(Array.isArray(actionData) && actionData.length){
                        actionData.forEach(function(action){
                            if(action.default === true){
                                _this.addListener("doubleClick", action);
                            }
                        });
                        _this.addListener("actionsButton", actionData);
                        _this.addListener("contextmenu", actionData);
                    }
                });
            }
        }

        function refreshTable(){
            var thead, tbody, trs, actionsBtn, actionsBtnTd, tr, td;

            tbody = _this.element.querySelector("tbody");
            thead = _this.element.querySelector("thead");

            if(tbody !== null && (actions.length || statusActions.length || priorityActions.length)){
                trs = Fluid.Lib.toArray(tbody.getElementsByTagName("tr"));

                switch(trs.length){
                    case 1:
                        tr = trs[0];
                        td = tr.querySelector("td");

                        if(td){
                            if(td.querySelector("div") === null){
                                applyActions(trs);
                            }
                            else if(!td.querySelector("div").classList.contains("empty")){
                                applyActions(trs);
                            }
                        }
                        break;
                    default:
                        applyActions(trs);
                        break;
                }
            }

            function applyActions(trs){
                trs.forEach(function(tr){
                    var lastTd = tr.getElementsByTagName("td");

                    lastTd = (lastTd.length) ? lastTd[lastTd.length - 1] : null;

                    if(lastTd !== null){
                        lastTd.style.display = "none";
                    }

                    actionsBtnTd = document.createElement("td");
                    actionsBtnTd.classList.add("actionsBtnWrap");

                    if(actions.length){
                        var actionBtnWrap = document.createElement("span");

                        actionsBtn = document.createElement("i");
                        actionsBtn.classList.add("actionsBtn","ordinaryActionsBtn", "fa", "fa-bars");
                        actionBtnWrap.appendChild(actionsBtn);
                        actionsBtnTd.appendChild(actionBtnWrap);
                    }

                    tr.appendChild(actionsBtnTd)
                });
            }
        }
    };
    Fluid.GridView.prototype.addListener = function(type, elm, additional){
        var _this = this;

        if(Fluid.Lib.is(type)){
            switch (type){
                case "actionsButton":
                    if(Fluid.Lib.is(elm) && elm[0].emitter){
                        var list, rows = [],
                            emitter = elm[0].emitter,
                            emitterPos = emitter.getBoundingClientRect();

                        elm.forEach(function(row, index){
                            if(row.noRedirect){
                                rows.push({
                                    value: index,
                                    element: row.originalNode.outerHTML,
                                    withoutWrap: true
                                })
                            }
                            else{
                                rows.push({
                                    value: index,
                                    element: "<a data-confirm-box='" + row.confirm + "' href='" + row.url+ "'>" + row.text + "</a>",
                                    withoutWrap: true
                                })
                            }
                        });

                        list = new Fluid.List({
                            name: 'folderContextList',
                            append: ".wrapper",
                            canHide: true,
                            deleteOnHide: false,
                            hidden: true,
                            positionType: 'afterEvent',
                            rows: rows,
                            emitter: emitter,
                            emitterEvent: "click",
                            skipDefaultValue: false,
                            skipOnSelect: true,
                            onSelect: function(list, element){
                                element.addEventListener("click", function(e){
                                    var ev = e || window.event,
                                        aTag = this.querySelector("a"),
                                        modal, url;

                                    Fluid.Browser.hideLists();

                                    if(aTag && aTag.dataset.confirmBox === "true"){
                                        ev.preventDefault();

                                        list.hide();

                                        url = aTag.getAttribute("href");
                                        modal = new Fluid.Modal({
                                            header: "Potwierdź usuniecie",
                                            content: "<span class='text-center'>Czy na pewno chcesz usunąć ten element?</span>",
                                            actions: {
                                                events: "close",
                                                parent: "header"
                                            },
                                            type: "alert",
                                            dim: true,
                                            promptSendButtonText: "Zatwierdź",
                                            onPromptSubmit: function () {
                                                modal.hide();
                                                window.location.href = url;
                                            }
                                        });
                                    }
                                })
                            }
                        });
                    }
                    break;
                case "contextmenu":
                    if(Fluid.Lib.is(elm) && elm[0].contextMenuEmitter){
                        elm[0].contextMenuEmitter.addEventListener("contextmenu", function(e){
                            var list, rows = [];

                            elm.forEach(function(row, index){
                                if(row.noRedirect){
                                    rows.push({
                                        value: index,
                                        element: row.originalNode.outerHTML,
                                        withoutWrap: true
                                    })
                                }
                                else{

                                    rows.push({
                                        value: index,
                                        element: "<a data-confirm-box='" + row.confirm + "' href='" + row.url+ "'>" + row.text + "</a>",
                                        withoutWrap: true
                                    })
                                }
                            });

                            e = e || window.event;

                            Fluid.Browser.hideLists();

                            list = new Fluid.List({
                                name: 'folderContextList',
                                append: ".wrapper",
                                canHide: true,
                                deleteOnHide: true,
                                hidden: false,
                                position: {x: e.clientX, y: e.clientY},
                                rows: rows,
                                emitter: this,
                                skipDefaultValue: false,
                                skipOnSelect: true,
                                onSelect: function(list, element){
                                    element.addEventListener("click", function(e){
                                        var ev = e || window.event,
                                            aTag = this.querySelector("a"),
                                            modal, url, content;

                                        Fluid.Browser.hideLists();

                                        if(aTag &&aTag.dataset.confirmBox === "true"){
                                            ev.preventDefault();

                                            list.hide();

                                            if(elm[0].confirmContent){
                                                content = elm[0].confirmContent;
                                            }
                                            else{
                                                content = "Czy na pewno chcesz usunąć ten element?";
                                            }

                                            url = aTag.getAttribute("href");
                                            modal = new Fluid.Modal({
                                                header: "Potwierdź usuniecie",
                                                content: "<span class='text-center'>" + content + "</span>",
                                                actions: {
                                                    events: "close",
                                                    parent: "header"
                                                },
                                                type: "alert",
                                                dim: true,
                                                promptSendButtonText: "Zatwierdź",
                                                onPromptSubmit: function () {
                                                    modal.hide();
                                                    window.location.href = url;
                                                }
                                            });

                                        }
                                    })
                                }
                            });

                            this.hasContextMenu = true;

                            e.preventDefault();
                        });
                    }
                    break;
                case "doubleClick":
                    if(Fluid.Lib.is(elm) && elm.contextMenuEmitter){
                        elm.contextMenuEmitter.addEventListener("dblclick", function(e){
                            var ev = e || window.event;

                            Fluid.Browser.hideLists();
                            window.location.href = elm.url;

                            ev.preventDefault();
                        })
                    }
                    break;
                case "customize":
                    if(Fluid.Lib.is(elm)){
                        elm.addEventListener("click", function(e){
                            var ev = e || window.event;

                            if(!elm.list){
                                Fluid.Browser.hideLists();

                                if(_this.modelData){
                                    var emitterPos = elm.getBoundingClientRect(),
                                        rows = [], list;

                                    _this.filterData = _this.modelData;

                                    if(_this.modelData){
                                        for(var prop in _this.modelData){
                                            if(_this.modelData.hasOwnProperty(prop)){
                                                rows.push({
                                                    value: prop.property,
                                                    element: withCheckbox(_this.modelData[prop]),
                                                    withoutWrap: true
                                                });
                                            }
                                        }

                                        list = new Fluid.List({
                                            name: 'gridViewCustomize',
                                            append: ".wrapper",
                                            canHide: true,
                                            position: {x: emitterPos.left, y: emitterPos.top + emitterPos.height},
                                            rows: rows,
                                            disableHover: true,
                                            emitter: elm,
                                            emitterEvent: "click"
                                        });

                                        list.addSpecialRow(createAcceptRow());

                                        elm.list = list;
                                    }
                                }
                            }
                        })
                    }
                    break;
                case "checkboxFilter":
                    if(Fluid.Lib.is(elm)){
                        additional.addEventListener("click", function(){
                            if(elm.value == "1"){
                                elm.value = "0";
                                elm.removeAttribute("checked")
                            }
                            else{
                                elm.value = "1";
                                elm.setAttribute("checked", "checked")
                            }
                        });
                    }
                    break;
                case "customizeAccept":
                    if(Fluid.Lib.is(elm)){
                        elm.addEventListener("click", function(e){
                            var ev = e || window.event,
                                toSend = [];

                            Fluid.Browser.hideLists();

                            _this.filterChecboxes.forEach(function(checkbox){
                                toSend.push({
                                    key: checkbox.dataset.key,
                                    value: checkbox.value
                                });
                            });

                            Fluid.Http.post({
                                url: '/api/save-grid-view-settings.html',
                                toSend: "data=" + JSON.stringify({
                                    keys: toSend,
                                    model: _this.element.dataset.model
                                }),
                                onResponse: function(response){
                                    var res;

                                    if(response.length){
                                        res = JSON.parse(response);

                                        if(res.status == 1){
                                            Fluid.Browser.reloadWithMessage(res.message, "success");
                                        }
                                        else{
                                            new Fluid.Notify(res.message, "error");
                                        }
                                    }
                                }
                            });

                            ev.preventDefault();
                        })
                    }
                    break;
            }
        }

        function withCheckbox(prop){
            var checkboxInput = document.createElement("input"),
                checkboxBox = document.createElement("span"),
                checkboxLabel = document.createElement("label"),
                checkboxWrap = document.createElement("a"),
                id = "GridView[" + prop.key + "]";

            checkboxInput.setAttribute("type", "checkbox");
            checkboxInput.setAttribute("name", id);
            checkboxInput.setAttribute("id", id);
            checkboxInput.dataset.key = prop.key;
            checkboxInput.value = "0";

            checkboxLabel.innerHTML = prop.label;
            checkboxLabel.setAttribute("for", id);
            checkboxLabel.classList.add("checkbox-label");

            checkboxBox.classList.add("checkbox-box");

            checkboxWrap.classList.add("checkbox-wrap");
            checkboxWrap.setAttribute("href", "#");

            checkboxWrap.appendChild(checkboxInput);
            checkboxWrap.appendChild(checkboxBox);
            checkboxWrap.appendChild(checkboxLabel);

            if(prop.set === 1){
                checkboxInput.value = "1";
                checkboxInput.setAttribute("checked", "checked")
            }

            _this.filterChecboxes.push(checkboxInput);
            _this.addListener("checkboxFilter", checkboxInput, checkboxBox);

            return checkboxWrap;
        }
        function createAcceptRow(){
            var acceptButton = document.createElement("span");

            acceptButton.classList.add("acceptButton", "fa", "fa-check");

            _this.addListener("customizeAccept", acceptButton);

            return {
                element: acceptButton,
                position: "bottom"
            }
        }
    };

    Fluid.FileManager = function (conf, elm) {
        var _this = this;

        this.info = {
            name: "FileManager",
            id: 17
        };
        this.defaultConf = {
            path: '/upload',
            requestUrl: "/file-manager/get-url.html",
            contentHeight: null,
            mode: "standard", //standard, input
            modeOptions: {},
            create: true,
            filter: false, //false, image...
            onLoad: function(){},
            onSelectItem: function(item, instance){}
        };
        this.conf = {};
        this.element = null;
        this.interface = false;
        this.content = null;
        this.breadcrums = null;
        this.lists = {
            folderContext: null,
            fileContext: null
        };
        this.elements = [];
        this.paths = [];
        this.dataToSend = null;
        this.uploadForm = null;
        this.url = "/file-manager/";
        this.srcUrl = null;
        this.tempUrl = this.url;
        this.chosenData = [];
        this.chosenObject = null;
        this.pasteButton = null;
        this.operationsButton = false;
        this.selected = [];
        this.currentData = [];

        this.changeNameModal = null;
        this.newFolderModal = null;

        this.initialized = false;

        Fluid.checkCaller(this);
        Fluid.addToElements(this);

        if (Fluid.Lib.is(conf)) {
            this.conf = conf;

            Fluid.Loader.mergeConfiguration(this);

            if (Fluid.Lib.is(elm)) {
                this.element = elm;
            }

            if(this.srcUrl === null){
                Fluid.Http.get({
                    url: _this.conf.requestUrl,
                    onResponse: function (response) {
                        var data = JSON.parse(response);

                        if(Fluid.Lib.is(data.url)){
                            _this.srcUrl = data.url;
                            _this.init();
                        }
                        else{
                            //error
                        }
                    }
                });
            }
            else{
                this.init();
            }

            return this;
        }
        else {
            Fluid.Message(17, 0);
            return Fluid;
        }
    };
    Fluid.FileManager.prototype.init = function () {
        var _this = this;

        if (!this.initialized) {
            this.setPath();

            if (this.conf.createdBy === "loader") {
                this.createInterface();
                this.addListeners("shortcuts");
                this.addListeners("managerWindow");
                this.updateContent();
            }
            else {
                if (this.element) {
                    this.createInterface();
                    this.addListeners("shortcuts");
                    this.addListeners("managerWindow");
                    this.updateContent();
                }
                else {
                    if(this.conf.create === true){
                        this.handleMode();

                        if(this.element){
                            this.createInterface();
                            this.addListeners("shortcuts");
                            this.addListeners("managerWindow");
                            this.updateContent();
                        }
                    }
                    else{
                        Fluid.Message(17, 1);
                    }
                }
            }

            if(this.element){
                this.element.classList.add("fileManager");
                this.initialized = true;
            }
            else{
                Fluid.Message(17,6);
            }
        }
    };
    Fluid.FileManager.prototype.handleMode = function(){
        var openBlock, input, wrapper, previewBlock, previewBlockWrap, imagePreview;

        switch(this.conf.mode){
            case "standard":
                this.element = document.createElement("div");
                Fluid.wrapper.appendChild(this.element);

                break;
            case "input":
                if(this.conf.modeOptions){
                    if(this.conf.modeOptions.input && this.conf.modeOptions.input instanceof HTMLElement){
                        input = this.conf.modeOptions.input;
                        openBlock = document.createElement("div");
                        previewBlock = document.createElement("div");
                        previewBlockWrap = document.createElement("div");
                        wrapper = new Fluid.Panel({
                            dim: true,
                            hidden: true,
                            create: true,
                            title: "Menedżer plików",
                            filter: "image",
                            class: ["col-lg-3", "col-md-3", "col-sm-6", "col-xs-12"],
                            showEmitter: openBlock
                        });

                        input.setAttribute("type", "hidden");
                        openBlock.classList.add("fileManagerInputOpen", "btn", "btn-warning");
                        openBlock.innerHTML = "Wybierz plik";
                        previewBlockWrap.classList.add("row", "previewBlockWrap");
                        previewBlock.classList.add("previewBlock", "col-lg-3", "col-md-3", "col-sm-6", "col-xs-12");
                        wrapper.element.classList.add("fileManagerInput");

                        imagePreview = document.createElement("img");
                        imagePreview.classList.add("img-responsive")

                        previewBlockWrap.appendChild(previewBlock);
                        previewBlock.appendChild(imagePreview);
                        input.parentNode.appendChild(previewBlockWrap);
                        input.parentNode.appendChild(openBlock);

                        if(input.value.length){
                            if(this.conf.modeOptions.displayAs){
                                switch(this.conf.modeOptions.displayAs){
                                    case "image":
                                        imagePreview.setAttribute("src", input.value);
                                        openBlock.style.marginTop = "15px";
                                        break;
                                }
                            }
                        }

                        this.element = wrapper.element;
                        this.conf.path = "/data";
                        this.conf.onLoad = function(){
                            Fluid.Lib.toArray(document.querySelectorAll(".elementThumbWrap")).forEach(function(item){
                                item.removeAttribute("class");

                                item.classList.add("elementThumbWrap", "col-lg-2", "col-md-2", "col-sm-2", "col-xs-3");
                            });
                            Fluid.Lib.toArray(document.querySelectorAll(".elementNameWrap")).forEach(function(item){
                                item.removeAttribute("class");

                                item.classList.add("elementNameWrap", "col-lg-7", "col-md-7", "col-sm-7", "col-xs-7");
                            });
                            Fluid.Lib.toArray(document.querySelectorAll(".elementSizeWrap")).forEach(function(item){
                                item.removeAttribute("class");

                                item.classList.add("elementSizeWrap", "col-lg-3", "col-md-3", "col-sm-3", "col-xs-hidden");
                            });

                        };
                        this.conf.onSelectItem = function(item, fileManager){
                            var url;

                            url = fileManager.srcUrl + item.dataset.path;

                            switch(item.dataset.type){
                                case "image":
                                    imagePreview.setAttribute("src", url);
                                    openBlock.style.marginTop = "15px";
                                    break;
                            }

                            input.value = url;
                        };
                        this.setPath();
                    }
                }
                break;
        }
    };
    Fluid.FileManager.prototype.createInterface = function (elementToCreate) {
        var _this = this,
            headerBlock,
            columnsBlock,
            contentBlock,
            wrapBlock;

        if (this.interface === false) {
            wrapBlock = document.createElement("section");
            headerBlock = createHeader();
            columnsBlock = createColumns();
            contentBlock = createContent();

            wrapBlock.classList.add("wrapBlock");

            this.element.appendChild(wrapBlock);
            wrapBlock.appendChild(headerBlock);
            //wrapBlock.appendChild(columnsBlock);
            wrapBlock.appendChild(contentBlock);

            this.addListeners("contentResize", contentBlock);

            this.interface = true;
        }
        else if (Fluid.Lib.is(elementToCreate)) {
            switch (elementToCreate) {
                case "copyButton":
                    createPasteButton("copy");
                    break;
                case "moveButton":
                    createPasteButton("move");
                    break;
                case "emptyView":
                    createEmptyView();
                    break;
            }
        }

        function createHeader() {
            var prevButton,
                nextButton,
                homeButton,
                uploadButton,
                uploadForm,
                addFolderButton,
                settingsButton,
                breadcrumsElm,
                breadcrumsBlock,
                buttonsBlock,
                navigationBlock,
                clearBlock;

            headerBlock = document.createElement("header");
            prevButton = document.createElement("a");
            nextButton = document.createElement("a");
            homeButton = document.createElement("a");
            addFolderButton = document.createElement("a");
            settingsButton = document.createElement("a");
            breadcrumsElm = document.createElement("span");

            uploadForm = new Fluid.Form({
                create: true,
                attributes: {
                    action: _this.url + "add.html",
                    method: "POST",
                    class: "uploadForm"
                }
            });
            uploadButton = uploadForm.create("fileInput");

            navigationBlock = document.createElement("div");
            buttonsBlock = document.createElement("div");
            breadcrumsBlock = document.createElement("div");
            clearBlock = document.createElement("div");

            breadcrumsBlock.classList.add("breadcrumsBlock");
            navigationBlock.classList.add("navigationBlock");
            buttonsBlock.classList.add("buttonsBlock");
            clearBlock.classList.add("clearFloat");

            prevButton.classList.add("prevButton");
            nextButton.classList.add("nextButton", "blocked");
            homeButton.classList.add("homeButton");
            uploadButton.classList.add("uploadButton");
            addFolderButton.classList.add("addFolderButton");
            settingsButton.classList.add("settingsButton");
            breadcrumsElm.classList.add("breadcrumsElm");

            nextButton.blocked = true;

            prevButton.setAttribute("href", "#");
            nextButton.setAttribute("href", "#");
            homeButton.setAttribute("href", "#");
            //uploadButton.setAttribute("href", "#");
            addFolderButton.setAttribute("href", "#");
            settingsButton.setAttribute("href", "#");

            prevButton.innerHTML = '<i class="icon fa fa-angle-left"></i>';
            nextButton.innerHTML = '<i class="icon fa fa-angle-right"></i>';
            homeButton.innerHTML = '<i class="icon fa fa-home"></i>';
            //uploadButton.innerHTML = '<i class="icon fa fa-upload"></i>';
            addFolderButton.innerHTML = '<i class="icon fa fa-folder"></i>';
            settingsButton.innerHTML = '<i class="icon fa fa-cog"></i>';

            _this.addListeners("prevButton", prevButton);
            _this.addListeners("nextButton", nextButton);
            _this.addListeners("homeButton", homeButton);
            //_this.addListeners("uploadButton", uploadButton);
            _this.addListeners("addFolderButton", addFolderButton);
            _this.addListeners("settingsButton", settingsButton);

            navigationBlock.appendChild(prevButton);
            //navigationBlock.appendChild(nextButton);
            navigationBlock.appendChild(homeButton);

            breadcrumsBlock.appendChild(breadcrumsElm);

            //buttonsBlock.appendChild(settingsButton);
            buttonsBlock.appendChild(addFolderButton);
            buttonsBlock.appendChild(uploadForm.element);

            headerBlock.appendChild(navigationBlock);
            headerBlock.appendChild(breadcrumsBlock);
            headerBlock.appendChild(buttonsBlock);
            headerBlock.appendChild(clearBlock);

            headerBlock.classList.add("header");
            _this.breadcrums = breadcrumsElm;
            _this.buttonsBlock = buttonsBlock;
            _this.uploadForm = uploadForm;
            _this.addListeners("fileUpload", uploadButton);

            return headerBlock;
        }

        function createColumns() {
            columnsBlock = document.createElement("div");

            columnsBlock.classList.add("columns");

            return columnsBlock;
        }

        function createContent() {
            _this.content = document.createElement("div");
            _this.content.classList.add("content");

            if (!Fluid.Lib.is(_this.conf.contentHeight)) {
                _this.setContentHeight();
            }
            else {
                _this.content.style.height = _this.conf.contentHeight;
            }

            return _this.content;
        }

        function createPasteButton(type) {
            var pasteButton;

            if (Fluid.Lib.is(_this.pasteButton)) {
                _this.pasteButton.parentNode.removeChild(_this.pasteButton);
                _this.pasteButton = null;
            }

            pasteButton = document.createElement("a");
            pasteButton.setAttribute("href", "#");
            pasteButton.classList.add("pasteButton");
            pasteButton.innerHTML = "<i class='icon fa fa-clipboard'></i>";
            pasteButton.type = type;

            _this.addListeners("pasteButton", pasteButton)

            _this.buttonsBlock.appendChild(pasteButton);
            _this.pasteButton = pasteButton;
        }

        function createEmptyView() {
            var emptyWrap, emptyIcon, emptyText;

            emptyWrap = document.createElement("div");
            emptyIcon = document.createElement("i");
            emptyText = document.createElement("span");

            emptyWrap.classList.add("emptyContent");
            emptyIcon.classList.add("fa", "fa-folder-o", "emptyIcon");
            emptyText.classList.add("emptyText");

            emptyText.innerHTML = "Katalog jest pusty";

            emptyWrap.appendChild(emptyIcon);
            emptyWrap.appendChild(emptyText);

            _this.content.appendChild(emptyWrap);
        }
    };
    Fluid.FileManager.prototype.updateContent = function () {
        var _this = this,
            url = null,
            toSend = {};

        toSend.path = this.getLastPath();

        if (Fluid.Lib.is(this.dataToSend)) {
            toSend.data = this.dataToSend;
        }

        if (Fluid.Lib.is(this.tempUrl)) {
            url = this.tempUrl + "content.html";
            this.tempUrl = this.url;
        }

        Fluid.Http.post({
            url: url,
            toSend: "data="+JSON.stringify(toSend),
            onResponse: function (response) {
                var data = JSON.parse(response);

                _this.clearContent();
                _this.uploadForm.extraSendData = JSON.stringify({
                    dir: _this.getLastPath()
                });

                if(data.falied === true && data.message){
                    new Fluid.Notify(data.message);
                }
                else{
                    _this.breadcrums.innerHTML = _this.getLastPath();
                    if (data.length) {
                        _this.currentData = data;
                        data.forEach(function (elm) {
                            if(_this.conf.filter){
                                if(elm.category === _this.conf.filter || elm.type === "folder"){
                                    _this.createElement(elm);
                                }
                            }
                            else{
                                _this.createElement(elm);
                            }

                            _this.addListeners("element", elm)
                        });

                        if(Fluid.Lib.isFunction(_this.conf.onLoad)){
                            _this.conf.onLoad();
                        }
                    }
                    else {
                        _this.createInterface("emptyView");
                    }
                }
            },
            onProgress: function (e) {
            }
        });
    };
    Fluid.FileManager.prototype.setPath = function () {
        var browserPath;

        browserPath = Fluid.Lib.getUrlParam("location");

        if (Fluid.Lib.is(browserPath) && !Fluid.Lib.isArray(browserPath)) {
            this.paths.push(browserPath);
        }
        else {
            this.paths.push(this.conf.path);
        }
    };
    Fluid.FileManager.prototype.clearContent = function () {
        this.elements = [];
        this.content.innerHTML = "";
    };
    Fluid.FileManager.prototype.clearPaste = function (type) {
        this.chosenData = [];

        if (this.elements.length > 0) {
            this.elements.forEach(function (element) {
                if (element.classList.contains("move")) {
                    element.classList.remove("move");
                }
                else if (element.classList.contains("copy")) {
                    element.classList.remove("copy");
                }
            });
        }
    };
    Fluid.FileManager.prototype.getLastPath = function () {
        if (this.paths.length > 0) {
            return this.paths[this.paths.length - 1];
        }
        else if (Fluid.Lib.is(this.conf.path)) {
            return this.conf.path;
        }
        else {
            Fluid.Message(17, 3);
        }
    };
    Fluid.FileManager.prototype.createElement = function (element) {
        var elementBlock = document.createElement("div"),
            elementThumbWrap = document.createElement("div"),
            elementThumb = document.createElement("img"),
            elementNameWrap = document.createElement("div"),
            elementName = document.createElement("span"),
            elementSizeWrap = document.createElement("div"),
            elementSize = document.createElement("span"),
            type, thumb, size, _this = this;

        if (Fluid.Lib.is(element)) {
            elementBlock.classList.add("element", "row");
            elementBlock.dataset.path = element.path;

            if (element.type === "file") {
                elementThumb = generateThumb();
                elementThumbWrap.classList.add("elementThumbWrap", "col-lg-1", "col-md-1", "col-sm-2", "col-xs-3");

                elementThumbWrap.appendChild(elementThumb);
                elementBlock.appendChild(elementThumbWrap);

                if (Fluid.Lib.is(element.basename)) {
                    elementName.innerHTML = element.basename;
                    elementNameWrap.classList.add("elementNameWrap", "col-lg-8", "col-md-8", "col-sm-9", "col-xs-9");

                    elementNameWrap.appendChild(elementName);
                    elementBlock.appendChild(elementNameWrap);
                }

                if (Fluid.Lib.is(element.size)) {

                    elementSize.innerHTML = element.size.human;
                    elementSizeWrap.classList.add("elementSizeWrap", "col-lg-2", "col-md-2", "col-sm-1", "hidden-xs");

                    elementSizeWrap.appendChild(elementSize);
                    elementBlock.appendChild(elementSizeWrap);
                }

                elementBlock.dataset.name = element.name + "." + element.ext;
                elementBlock.dataset.type = element.category;
                elementBlock.classList.add("file");
                this.addListeners("file", elementBlock);
            }
            else if (element.type === "folder") {
                elementThumb = generateThumb(true);
                elementThumbWrap.classList.add("elementThumbWrap", "col-lg-1", "col-md-1", "col-sm-2", "col-xs-3");

                elementThumbWrap.appendChild(elementThumb);
                elementBlock.appendChild(elementThumbWrap);

                if (Fluid.Lib.is([element.name])) {
                    elementName.innerHTML = element.name;
                    elementNameWrap.classList.add("elementNameWrap", "col-lg-8", "col-md-8", "col-sm-9", "col-xs-9");

                    elementNameWrap.appendChild(elementName);
                    elementBlock.appendChild(elementNameWrap);
                }

                if (Fluid.Lib.is(element.size)) {
                    size = (parseInt(element.size.human, 10) === 0) ? "Pusty" : element.size.human;
                    elementSize.innerHTML = size;
                    elementSizeWrap.classList.add("elementSizeWrap", "col-lg-1", "col-md-1", "col-sm-1", "col-xs-hidden");

                    elementSizeWrap.appendChild(elementSize);
                    elementBlock.appendChild(elementSizeWrap);
                }

                elementBlock.dataset.name = element.name;
                elementBlock.classList.add("folder");
                this.addListeners("folder", elementBlock);
            }

            elementBlock.dataset.dir = element.dir;

            this.elements.push(elementBlock);
            this.content.appendChild(elementBlock);
        }

        function generateThumb(folder) {
            var thumb = null,
                isFolder = folder || false;

            if (isFolder === false) {
                switch (element.category) {
                    case "image":
                        if (Fluid.Lib.is(element.thumb)) {
                            thumb = new Image();
                            thumb.src = _this.srcUrl + element.thumb;
                            thumb.classList.add("thumb", "img-responsive");
                        }
                        else{
                            thumb = document.createElement("i");
                            thumb.classList.add("fa", "fa-file-photo-o", "thumb");
                        }
                        break;
                    case "document":
                        thumb = document.createElement("i");
                        thumb.classList.add("fa", "fa-file-text-o", "thumb");
                        break;
                    case "music":
                        thumb = document.createElement("i");
                        thumb.classList.add("fa", "fa-music", "thumb");
                        break;
                    case "video":
                        thumb = document.createElement("i");
                        thumb.classList.add("fa", "fa-film", "thumb");
                        break;
                }
            }
            else {
                thumb = document.createElement("i");
                thumb.classList.add("fa", "fa-folder", "thumb");
            }

            if(thumb === null){
                thumb = document.createElement("i");
                thumb.classList.add("fa", "fa-question-circle-o", "thumb");
            }

            return thumb;
        }
    };
    Fluid.FileManager.prototype.setContentHeight = function () {
        var height;

        if (Fluid.Lib.is(this.content)) {
            height = Fluid.Lib.getHeight();

            this.content.style.height = height - 250 + "px";
        }
    };
    Fluid.FileManager.prototype.addListeners = function (type, elm) {
        var _this = this;

        switch (type) {
            case "prevButton":
                if (Fluid.Lib.is(elm)) {
                    elm.addEventListener("click", function () {
                        _this.paths.pop();
                        _this.updateContent();
                    }, false);
                }
                break;
            case "nextButton":
                if (Fluid.Lib.is(elm)) {
                    elm.addEventListener("click", function () {

                    }, false);
                }
                break;
            case "homeButton":
                if (Fluid.Lib.is(elm)) {
                    elm.addEventListener("click", function () {
                        _this.paths = [_this.conf.path];
                        _this.updateContent();
                    }, false);
                }
                break;
            case "contentResize":
                if (Fluid.Lib.is(elm)) {
                    window.addEventListener("resize", function () {
                        _this.setContentHeight();
                    }, false);
                }
                break;
            case "addFolderButton":
                if (Fluid.Lib.is(elm)) {
                    elm.addEventListener("click", function () {
                        _this.newFolderModal = new Fluid.Modal({
                            header: "Wprowadź nazwę",
                            content: "<span class='text-center'>Wprowadź nazwę nowego folderu</span>",
                            actions: {
                                events: "close",
                                parent: "header"
                            },
                            type: "prompt",
                            promptSendButtonText: "Dodaj",
                            onPromptSubmit: function (value) {
                                var toSend, url;

                                toSend = {
                                    name: value,
                                    dir: _this.getLastPath()
                                };
                                url = _this.url + "new-directory.html";

                                Fluid.Http.post({
                                    url: url,
                                    toSend: "data="+JSON.stringify(toSend),
                                    onResponse: function (response) {
                                        var result = JSON.parse(response);

                                        if (result.failed === false) {
                                            _this.updateContent();
                                            _this.newFolderModal.hide(true);

                                            if(value.length && value[0] === "."){
                                                new Fluid.Notify("Folder został utworzony, jednak może być niewidoczny, ponieważ pierwszym znakiem w nazwie jest kropka.", "warning");
                                            }
                                        }
                                        else {
                                            _this.newFolderModal.hide(true);
                                            new Fluid.Notify(result.message);
                                        }
                                    }
                                });
                            }
                        });

                        _this.newFolderModal.show();
                    }, false);
                }
                break;
            case "pasteButton":
                if (Fluid.Lib.is(elm)) {
                    elm.addEventListener("click", function () {
                        var __this = this;

                        if (_this.chosenData.length > 0) {
                            var toSend, url;

                            toSend = {
                                paths: _this.chosenData,
                                placeToPaste: _this.getLastPath()
                            };
                            if (this.type === "copy") {
                                url = _this.url + "copy.html";
                            }
                            else if (this.type === "move") {
                                url = _this.url + "move.html";
                            }

                            Fluid.Http.post({
                                url: url,
                                toSend: "data="+JSON.stringify(toSend),
                                onResponse: function (response) {
                                    var result = JSON.parse(response);

                                    if (result.failed === false) {
                                        _this.clearPaste(__this.type)
                                        _this.updateContent();
                                    }
                                    else {
                                        new Fluid.Notify(result.message);
                                    }

                                    _this.pasteButton = null;
                                    __this.parentNode.removeChild(__this);
                                }
                            });
                        }
                        else {
                            new Fluid.Notify("Żadne obiekty nie zostały zaznaczone.");
                        }
                    }, false);
                }
                break;
            case "folder":
                if (Fluid.Lib.is(elm)) {
                    elm.addEventListener("dblclick", function () {
                        Fluid.Browser.hideLists();
                        _this.paths.push(this.dataset.path);
                        _this.updateContent();
                    }, false);
                    elm.addEventListener("click", function () {
                        handleSelection(this);
                    }, false);
                    elm.addEventListener("contextmenu", function (e) {
                        var list;

                        e = e || window.event;

                        Fluid.Browser.hideLists();

                        list = new Fluid.List({
                            name: 'folderContextList',
                            append: ".wrapper",
                            canHide: true,
                            deleteOnHide: true,
                            hidden: false,
                            position: {x: e.clientX, y: e.clientY},
                            rows: [
                                {value: 0, element: 'Otwórz'},
                                {value: 1, element: 'Wytnij'},
                                {value: 2, element: 'Skopiuj'},
                                {value: 4, element: 'Usuń'},
                                {value: 5, element: 'Zmień nazwę'},
                            ],
                            rowsPrefix: '<a href="#">',
                            rowsSuffix: '</a>',
                            emitter: elm,
                            skipDefaultValue: false,
                            onSelect: function (list) {
                                switch (list.value) {
                                    case "0":
                                        _this.paths.push(list.emitter.dataset.path);
                                        _this.updateContent();
                                        break;
                                    case "1":
                                        copyOrMoveAction("move", list);
                                        break;
                                    case "2":
                                        copyOrMoveAction("copy", list);
                                        break;
                                    case "4":
                                        deleteElements(list);
                                        break;
                                    case "5":
                                        changeName(list, "folder");
                                        break;
                                }

                                list.hide();
                            }
                        });

                        _this.lists.folderContext = list;

                        elm.hasContextMenu = true;

                        e.preventDefault();
                    }, false);
                }
                break;
            case "file":
                if (Fluid.Lib.is(elm)) {
                    elm.addEventListener("dblclick", function () {
                        if(elm.dataset.type){
                            _this.currentData.forEach(function(element){
                                if(element.path === elm.dataset.path){
                                    _this.chosenObject = element;

                                    switch(elm.dataset.type){
                                        case "image":
                                            new Fluid.FileManager.Gallery(_this);
                                            break;
                                        case "music":
                                            new Fluid.FileManager.MusicPlayer(_this);
                                            break;
                                    }

                                    return true;
                                }
                            });
                        }
                    }, false);
                    elm.addEventListener("click", function () {
                        handleSelection(this);
                    }, false);
                    elm.addEventListener("contextmenu", function (e) {
                        var list;

                        e = e || window.event;

                        Fluid.Browser.hideLists();

                        list = new Fluid.List({
                            name: 'folderContextList',
                            append: ".wrapper",
                            canHide: true,
                            deleteOnHide: true,
                            hidden: false,
                            position: {x: e.clientX, y: e.clientY},
                            rows: [
                                {value: 0, element: 'Otwórz'},
                                {value: 1, element: 'Pobierz'},
                                {value: 2, element: 'Skopiuj'},
                                {value: 3, element: 'Wytnij'},
                                {value: 4, element: 'Zmień nazwę'},
                                {value: 5, element: 'Usuń'},
                                //{value: 6, element: 'Właściwości'}
                            ],
                            rowsPrefix: '<a href="#">',
                            rowsSuffix: '</a>',
                            skipDefaultValue: false,
                            emitter: elm,
                            onSelect: function (list) {
                                _this.chosenObject = null;

                                switch (list.value) {
                                    case "0":
                                        handlePreview(list);
                                        break;
                                    case "1":
                                        window.location = _this.url + "download.html?path=" + _this.srcUrl + list.emitter.dataset.path;
                                        break;
                                    case "3":
                                        copyOrMoveAction("move", list);
                                        break;
                                    case "2":
                                        copyOrMoveAction("copy", list);
                                        break;
                                    case "5":
                                        deleteElements(list);
                                        break;
                                    case "4":
                                        changeName(list, "file");
                                        break;
                                    case "6":
                                        new Fluid.Modal({
                                            header: "Właściwości - " + list.emitter.dataset.name,
                                            content: "<span class='text-center'>Tu będą właściwości pliku.</span>",
                                            actions: {
                                                events: "close",
                                                parent: "header"
                                            },
                                            view: "standard",
                                            animation: false,
                                            type: "modal"
                                        });
                                        break;
                                }

                                list.hide();
                            }
                        });

                        _this.lists.fileContext = list;

                        elm.hasContextMenu = true;

                        e.preventDefault();
                    }, false);

                    if(Fluid.Lib.isFunction(this.conf.onSelectItem)){
                        elm.addEventListener("dblclick", function(){
                            _this.conf.onSelectItem(elm, _this);
                        })
                    }
                }
                break;
            case "shortcuts":
                //to finish
                document.addEventListener("keydown", function (e) {
                    e = e || window.event;

                    if (e.keyCode === 17) {
                        _this.operationsButton = true;
                    }

                    if (_this.operationsButton === true) {

                        switch (e.keyCode) {
                            case 65:

                                break;
                        }
                    }

                }, false);
                document.addEventListener("keyup", function (e) {
                    e = e || window.event;

                    if (e.keyCode === 17) {
                        _this.operationsButton = false;
                    }
                }, false);
                break;
            case "managerWindow":
                _this.element.addEventListener("contextmenu", function (e) {
                    e = e || window.event;

                    e.preventDefault();

                    return false;
                }, false);
                break;
            case "fileUpload":
                if (Fluid.Lib.is(elm)){
                    elm.addEventListener("change", function () {
                        var files, fileData;

                        files = Fluid.Lib.toArray(this.files);

                        files.forEach(function (file) {
                            fileData = new FormData();

                            fileData.append("file", file);
                            fileData.append("extraData", JSON.stringify({
                                dir: _this.getLastPath()
                            }));

                            Fluid.Http.post({
                                url: _this.uploadForm.action,
                                toSend: fileData,
                                contentType: false,//"multipart/form-data",//"application/json",
                                onResponse: function(){
                                    _this.updateContent();
                                },
                                onProgress: function (e) {
                                    if (e.lengthComputable) {
                                        //completed = (e.loaded / e.total) * 100;
                                        //filePreviewObj.progress.style.width = completed + "%";
                                    }
                                }
                            });
                        });
                    }, false);
                }
                break;
        }

        function handlePreview(list){
            var type = list.emitter.dataset.type;

            _this.prepareChosenData(list);

            if(type){
                switch(type){
                    case "image":
                        new Fluid.FileManager.Gallery(_this);
                        break;
                    case "music":
                        new Fluid.FileManager.MusicPlayer(_this);
                        break;
                }
            }
        }
        function handleSelection(elm) {
            if (_this.operationsButton === true) {
                if (elm.classList.contains("selected") && Fluid.Lib.is(elm.selectedIndex)) {
                    elm.classList.remove("selected");

                    if (_this.selected.indexOf(elm.selectedIndex) !== -1) {
                        _this.selected.splice(elm.selectedIndex, 1);
                    }
                }
                else {
                    elm.classList.add("selected");
                    elm.selectedIndex = _this.selected.push({
                        name: elm.dataset.name,
                        dir: elm.dataset.dir,
                        path: elm.dataset.path,
                        element: elm
                    });
                }
            }
            else {
                _this.selected = [];

                if (_this.elements.length) {
                    _this.elements.forEach(function (elm) {
                        if (elm.classList.contains("selected")) {
                            elm.classList.remove("selected");
                        }
                    });
                }
            }
        }
        function changeName(list, type) {
            var content;

            switch(type){
                case "folder":
                    content = "<span class='text-center'>Wprowadź nową nazwę folderu</span>";
                    break;
                case "file":
                    content = "<span class='text-center'>Wprowadź nową nazwę pliku</span>";
                    break;
            }

            _this.changeNameModal= new Fluid.Modal({
                header: "Zmień nazwę",
                content: content,
                actions: {
                    events: "close",
                    parent: "header"
                },
                type: "prompt",
                promptSendButtonText: "Zmień",
                promptInputValue:  list.emitter.dataset.name,
                onPromptSubmit: function (value) {
                    var toSend, url, modal;

                    toSend = {
                        oldName: list.emitter.dataset.name,
                        newName: value,
                        dir: list.emitter.dataset.dir
                    };
                    url = _this.url + "rename.html";
                    //formData.append("data", JSON.stringify(toSend));

                    Fluid.Http.post({
                        url: url,
                        toSend: "data="+JSON.stringify(toSend),
                        onResponse: function (response) {
                            var result = JSON.parse(response);

                            if (result.failed === false) {
                                _this.updateContent();
                                _this.changeNameModal.hide(true);
                            }
                            else {
                                _this.changeNameModal.hide(true);
                                new Fluid.Notify(result.message)
                            }
                        }
                    });
                }
            });
        }
        function deleteElements(list) {
            var url;

            _this.prepareChosenData(list, "delete");

            url = _this.url + "delete.html";

            Fluid.Http.post({
                url: url,
                toSend: "data="+JSON.stringify(_this.chosenData),
                onResponse: function (response) {
                    var result = JSON.parse(response);

                    if (result.failed === false) {
                        _this.updateContent();
                    }
                    else {
                        //display modal with error
                    }
                }
            });
        }
        function copyOrMoveAction(type, list) {
            _this.clearPaste(type);
            _this.prepareChosenData(list, type);
            _this.createInterface(type + "Button");
        }
    };
    Fluid.FileManager.prototype.prepareChosenData = function (list, type) {
        var find = false, _this = this;

        this.chosenData = [];

        if (this.selected.length > 0) {
            this.selected.forEach(function (selected) {
                selected.element.classList.add(type);

                if (selected.name === list.emitter.dataset.name) {
                    find = true;
                }
            });

            this.chosenData = this.selected;

            if (find === false) {
                list.emitter.classList.add(type);

                this.chosenData.push({
                    dir: list.emitter.dataset.dir,
                    name: list.emitter.dataset.name
                });
            }
        }
        else {
            list.emitter.classList.add(type);

            this.chosenData.push({
                dir: list.emitter.dataset.dir,
                name: list.emitter.dataset.name
            });

            this.currentData.forEach(function(element){
                if(element.path === list.emitter.dataset.path){
                    _this.chosenObject = element;

                    return true;
                }
            });
        }
    };
    Fluid.FileManager.prototype.indexFilesByType = function(type){
        var filteredArr = [],
            _this = this,
            result = {
                complete: false,
                items: [],
                currentIndex: null,
                prevItem: null,
                nextItem: null
            };

        if(this.currentData.length){
            this.currentData.forEach(function(element){
                if(element.type === "file" && element.category === type){
                    filteredArr.push(element);
                }
            });
            filteredArr.forEach(function(element, index){
                result.items.push(element);

                if(element.path === _this.chosenObject.path){
                    result.currentIndex = index;
                    result.complete = true;

                    if(result.currentIndex - 1 >= 0){
                        result.prevItem = filteredArr[result.currentIndex - 1];
                    }

                    if(result.currentIndex + 1 <= (filteredArr.length - 1)){
                        result.nextItem = filteredArr[result.currentIndex + 1];
                    }

                    return true;
                }
            });
        }

        return result;
    };
    Fluid.FileManager.Gallery = function(fileManager){
        this.galleryPreview = null;
        this.fileManager = fileManager;

        this.createWrapper();
    };
    Fluid.FileManager.Gallery.prototype.createWrapper = function(){
        var modal, imagePreview, image, prevImageBtn, nextImageBtn;

        imagePreview = document.createElement("div");
        image = new Image();
        prevImageBtn = document.createElement("i");
        nextImageBtn = document.createElement("i");

        imagePreview.classList.add("modal-preview");
        image.src = this.fileManager.srcUrl + this.fileManager.chosenObject.path;
        image.classList.add("img-responsive");
        prevImageBtn.classList.add("prevImageBtn", "fa", "fa-angle-left");
        nextImageBtn.classList.add("nextImageBtn", "fa", "fa-angle-right");

        imagePreview.appendChild(image);
        imagePreview.appendChild(prevImageBtn);
        imagePreview.appendChild(nextImageBtn);

        this.addListener("prevImage", prevImageBtn);
        this.addListener("nextImage", nextImageBtn);
        this.addListener("imageLoad", image);
        this.galleryPreview = image;

        modal = new Fluid.Modal({
            dim: true,
            content: imagePreview,
            header: "Przeglądarka zdjęć",
            actions: {
                events: "close",
                parent: "header"
            },
        });

        modal.element.classList.add("modal-dark", "modalFMGallery");
    };
    Fluid.FileManager.Gallery.prototype.addListener = function(type, elm){
        var _this = this;

        switch(type){
            case "prevImage":
                if(Fluid.Lib.is(elm)){
                    elm.addEventListener("click", function(){
                        var indexedData = _this.fileManager.indexFilesByType("image");

                        changeImage(indexedData, "prev");
                    });
                }
                break;
            case "nextImage":
                if(Fluid.Lib.is(elm)){
                    elm.addEventListener("click", function(){
                        var indexedData = _this.fileManager.indexFilesByType("image");

                        changeImage(indexedData, "next");
                    });
                }
                break;
            case "imageLoad":
                if(Fluid.Lib.is(elm)){
                    elm.addEventListener("animationend", function(){
                        elm.classList.remove("smoothLoad");
                    });
                }

                break;
        }

        function changeImage(indexedData, type) {
            if (indexedData.complete === true) {
                _this.fileManager.currentData.forEach(function (element) {
                    if (indexedData[type  + "Item"] !== null && element.path === indexedData[type + "Item"].path) {
                        _this.fileManager.chosenObject = element;
                        _this.galleryPreview.src = _this.fileManager.srcUrl + element.path;
                        _this.galleryPreview.classList.add("smoothLoad");

                        return true;
                    }
                });

                return true;
            }
            else {
                return false;
            }
        }
    };
    Fluid.FileManager.MusicPlayer = function(fileManager){
        this.fileManager = fileManager;
        this.context = null;
        this.playPauseBtn = null;
        this.progressBar = null;
        this.trackName = null;
        this.player = null;

        this.loadMusic(this.fileManager.srcUrl + this.fileManager.chosenObject.path);
        this.addListener("shortcuts")
    };
    Fluid.FileManager.MusicPlayer.prototype.loadMusic = function(url){
        if(Audio){
            this.context = new Audio(url);
            this.addListener("audioObject", this.context);

            return true;
        }
    };
    Fluid.FileManager.MusicPlayer.prototype.createWrapper = function(type){
        var modal, audioPlayer, controlsBlock,
            iconWrap, icon, trackName,
            playPauseBtn, nextTrack, prevTrack,
            progressBarWrap, progressBar, _this = this;

        switch(type){
            case "player":
                controlsBlock = document.createElement("div");
                audioPlayer = document.createElement("div");
                playPauseBtn = document.createElement("span");
                nextTrack = document.createElement("span");
                prevTrack = document.createElement("span");
                progressBarWrap = document.createElement("span");
                progressBar = document.createElement("span");
                iconWrap = document.createElement("div");
                icon = document.createElement("span");
                trackName = document.createElement("span");

                audioPlayer.classList.add("modal-preview");
                controlsBlock.classList.add("controlsBlock");
                iconWrap.classList.add("iconWrap");
                trackName.classList.add("trackName");
                icon.classList.add("fa", "fa-music", "mainIcon");
                playPauseBtn.classList.add("fa", "fa-play", "playPauseBtn");
                nextTrack.classList.add("fa", "fa-forward", "nextTrackBtn");
                prevTrack.classList.add("fa", "fa-backward", "prevTrackBtn");
                progressBarWrap.classList.add("progressBarWrap");
                progressBar.classList.add("progressBar");

                controlsBlock.appendChild(prevTrack);
                controlsBlock.appendChild(playPauseBtn);
                controlsBlock.appendChild(nextTrack);
                progressBarWrap.appendChild(progressBar);
                iconWrap.appendChild(icon);
                iconWrap.appendChild(trackName);
                audioPlayer.appendChild(iconWrap);
                audioPlayer.appendChild(controlsBlock);
                audioPlayer.appendChild(progressBarWrap);

                modal = new Fluid.Modal({
                    dim: true,
                    content: audioPlayer,
                    header: "Odtwarzacz muzyki",
                    actions: {
                        events: "close",
                        parent: "header"
                    },
                    onClose: function(){
                        _this.context.pause();
                    }
                });

                modal.element.classList.add("modal-dark", "modalFMAudioPlayer");
                this.playPauseBtn = playPauseBtn;
                this.progressBar = progressBar;
                this.player = audioPlayer;
                this.trackName = trackName;
                this.addListener("playPauseBtn", this.playPauseBtn);
                this.addListener("nextTrack", nextTrack);
                this.addListener("prevTrack", prevTrack);
                break;
            case "nosupport":
                new Fluid.Notify("Niestety Twoja przeglądarka nie wspiera tego rozszerzenia utworu, lub wystąpił nieoczekiwany błąd.");
                break;
        }
    };
    Fluid.FileManager.MusicPlayer.prototype.changeTrack = function(element){
        this.context.pause();

        if(this.loadMusic(this.fileManager.srcUrl + element.path) === true){
            if(this.progressBar !== null){
                this.progressBar.style.left = "-5px";
            }
        }
    };
    Fluid.FileManager.MusicPlayer.prototype.addListener = function(type, elm) {
        var _this = this;

        switch(type){
            case "audioObject":
                if(Fluid.Lib.is(elm)){
                    elm.addEventListener("canplay", function(){
                        var url = decodeURI(elm.currentSrc);

                        elm.trackName = url.split('/').pop();

                        if(_this.player === null){
                            _this.createWrapper("player");
                        }

                        _this.trackName.innerHTML = elm.trackName;

                        _this.play();
                    });
                    elm.addEventListener("error", function(){
                        _this.createWrapper("nosupport");
                    });
                    elm.addEventListener("timeupdate", function(){
                        if(_this.progressBar !== null){
                            var progress = (_this.context.currentTime/_this.context.duration) * 100;

                            _this.progressBar.style.left = progress - 5 + "%";
                        }
                    });
                }
                break;
            case "playPauseBtn":
                if(Fluid.Lib.is(elm)) {
                    elm.addEventListener("click", function(){
                        if(_this.context.paused){
                            _this.play();
                        }
                        else{
                            _this.pause();
                        }
                    });
                }
                break;
            case "nextTrack":
                if(Fluid.Lib.is(elm)){
                    elm.addEventListener("click", function(){
                        var indexedData = _this.fileManager.indexFilesByType("music");

                        changeTrack(indexedData, "next");
                    });
                }
                break;
            case "prevTrack":
                if(Fluid.Lib.is(elm)){
                    elm.addEventListener("click", function(){
                        var indexedData = _this.fileManager.indexFilesByType("music");

                        changeTrack(indexedData, "prev");
                    });
                }
                break;
            case "shortcuts":
                document.addEventListener("keyup", function(e){
                    switch(e.keyCode){
                        case 27:
                            _this.pause();
                            break;
                    }
                });
                break;
        }

        function changeTrack(indexedData, type) {
            console.log(indexedData);

            if (indexedData.complete === true) {
                _this.fileManager.currentData.forEach(function (element) {
                    if (indexedData[type  + "Item"] !== null && element.path === indexedData[type + "Item"].path) {
                        _this.fileManager.chosenObject = element;
                        _this.changeTrack(element);

                        return true;
                    }
                });

                return true;
            }
            else {
                return false;
            }
        }
    };
    Fluid.FileManager.MusicPlayer.prototype.play = function() {
        if(this.playPauseBtn !== null){
            this.playPauseBtn.classList.remove("fa-play");
            this.playPauseBtn.classList.add("fa-pause");
        }

        this.context.play();

    };
    Fluid.FileManager.MusicPlayer.prototype.pause = function() {
        if(this.playPauseBtn !== null){
            this.playPauseBtn.classList.remove("fa-pause");
            this.playPauseBtn.classList.add("fa-play");
        }

        this.context.pause();
    };

    Fluid.Notify = function(message, type, autohide){
        this.info = {
            name: "Notify",
            id: 6
        };
        this.message = message;
        this.type = type || "error";
        this.autohide = autohide || 5000;

        if(this.message && this.message.length){
            this.init();
        }
        else{
            //error
        }

    };
    Fluid.Notify.prototype.init = function(){
        var message = document.createElement("div"),
            panel;

        message.innerHTML = this.message;
        message.classList.add("message");

        panel = new Fluid.Panel({
            position: "bottom",
            title: null,
            dim: false,
            create: true,
            contentToAdd: message,
            type: 'notify',
            hidden: true
        });

        panel.element.classList.add("panelNotify", this.type);
        panel.show();

        if(Number.isInteger(this.autohide)){
            setTimeout(function(){
                panel.hide();
            }, this.autohide)
        }
    };

    Fluid.List = function (conf, elm) {
        conf = conf || {};

        this.info = {
            name: "List",
            id: 16
        };
        this.defaultConf = {
            name: 'defaultList',
            direction: 'down',
            defaultValue: 0,
            skipDefaultValue: true,
            rows: [], //{value, element, withoutWrap}
            rowsPrefix: '',
            rowsSuffix: '',
            orderRows: true,
            append: null,
            position: {x: 0, y: 0},
            positionType: null,
            canHide: false,
            onSelect: function (list, element) {
            },
            skipOnSelect: false,
            emitter: null,
            emitterEvent: null,
            mode: 'normal', //select, normal
            hidden: false,
            deleteOnHide: false,
            injectedValue: null,
            disableHover: false,
        };
        this.conf = conf;
        this.element = null;
        this.rows = [];
        this.emitter = null;
        this.emitterEvent = null;
        this.value = null;
        this.hidden = null;
        this.heightToIncrease = 0;
        this.alreadyHeightIncreased = false;
        this.initialized = false;
        this.selectElement = null;

        Fluid.checkCaller(this);
        Fluid.addToElements(this);

        Fluid.Loader.mergeConfiguration(this);

        if (Fluid.Lib.is(elm)) {
            this.element = elm;
        }

        this.init();
        this.addListeners("hideOnCLick");

        return this;
    };
    Fluid.List.prototype.init = function () {
        var _this = this;

        if (!this.initialized) {
            checkRows();

            if (this.conf.createdBy === "loader") {
                //console.log(this.conf)
            }
            else {
                if(this.conf.append === null && this.element === null){
                    this.conf.append = Fluid.wrapper;
                    createList();
                }
                else if (this.conf.append !== null) {
                    createList();
                }
                else {
                    Fluid.Message(16, 1);
                }
            }

            if (Fluid.Lib.is(this.conf.emitter)) {
                addEmitter();
            }

            displayStuff();

            if(this.conf.disableHover === true){
                this.element.classList.add("disableHover")
            }

            this.element.classList.add("list");
            this.element.value = "";
            this.initialized = true;
        }

        function checkRows() {
            var firstRow = document.createElement("a"),
                secondRow = document.createElement("a"),
                thirdRow = document.createElement("a");

            if(!_this.conf.rows.length){
                firstRow.innerHTML = 'Wybierz z listy';
                secondRow.innerHTML = 'Opcja 1';
                thirdRow.innerHTML = 'Opcja 2';

                _this.conf.rows = [
                    {
                        value: 0,
                        element: firstRow
                    },
                    {
                        value: 1,
                        element: secondRow
                    },
                    {
                        value: 2,
                        element: thirdRow
                    }
                ]
            }

        }
        function createList() {
            var list = document.createElement("ul"),
                li, appendParent, select;

            addRows();
            appendList();
            configureMode();
            configureElement();

            _this.element = list;
            _this.addStaticPosition();

            function configureMode(){
                switch(_this.conf.mode){
                    case "normal":

                        break;
                    case "select":
                        select = document.createElement("div");

                        select.classList.add("select");
                        list.parentNode.insertBefore(select, list.parentNode.childNodes[ list.parentNode.childNodes.length - 1]);

                        if(_this.conf.injectedValue){
                            _this.conf.rows.forEach(function(row){
                                if (_this.conf.injectedValue == row.value) {
                                    select.innerHTML = row.element;
                                }
                            });

                            list.dataset.value= _this.conf.injectedValue ;
                            _this.value = _this.conf.injectedValue;
                            _this.conf.onSelect(_this);
                        }
                        else{
                            select.innerHTML = list.childNodes[0].innerHTML;
                        }

                        if(_this.conf.emitter === null){
                            _this.conf.emitter = select;
                        }

                        _this.selectElement = select;
                        _this.emitterEvent = "click";
                        _this.addListeners("selectMode", list);
                        break;
                }
            }

            function addRows() {
                if(_this.conf.orderRows === true){
                    _this.conf.rows.sort(function(a, b){
                        if(a.value < b.value) return -1;
                        if(a.value > b.value) return 1;

                        return 0;
                    });
                }

                _this.conf.rows.forEach(function (row) {
                    li = document.createElement("li");

                    if (Fluid.Lib.is(row.element)) {
                        switch (typeof(row.element)) {
                            case "string":
                                if(row.withoutWrap && row.withoutWrap === true){
                                    li.innerHTML = row.element;
                                }
                                else{
                                    li.innerHTML = _this.conf.rowsPrefix + row.element + _this.conf.rowsSuffix;
                                }
                                break;

                            case "object":
                                li.appendChild(row.element);
                                break;
                        }

                        if (Fluid.Lib.is(row.value)) {
                            li.dataset.value = row.value;
                        }
                        if (Fluid.Lib.is(row.extra)) {
                            li.dataset.extra = row.extra;
                        }

                        if (!row.noEvent) {
                            _this.addListeners("row", li);
                        }

                        list.appendChild(li);
                    }
                    else {
                        //error!
                    }
                });
            }

            function configureElement() {
                list.classList.add("list");
                list.dataset.name = _this.conf.name;

                list.style.visibility = "hidden";
                list.style.display = "block";

                list.prop = list.getBoundingClientRect();
                list.styles = window.getComputedStyle(list);

                list.style.display = "";
                list.style.visibility = "";
            }

            function appendList() {
                switch (typeof(_this.conf.append)) {
                    case "string":
                        appendParent = document.querySelector(_this.conf.append);

                        if (Fluid.Lib.is(appendParent)) {
                            appendParent.appendChild(list);
                        }
                        break;

                    case "object":
                        _this.conf.append.appendChild(list);
                        break;
                }
            }
        }
        function addEmitter() {
            if (typeof(_this.conf.emitter) === "object") {
                _this.emitter = _this.conf.emitter;
            }
            else if (typeof(_this.conf.emitter) === "string") {
                _this.emitter = document.querySelector(_this.conf.emitter);
            }

            if (Fluid.Lib.is(_this.conf.emitterEvent)) {
                _this.emitterEvent = _this.conf.emitterEvent;
            }

            _this.addListeners("emitter")
        }
        function displayStuff() {
            _this.hidden = _this.conf.hidden;

            if (_this.hidden === true) {
                _this.hide();
            }
            else {
                _this.show();
            }

        }
    };
    Fluid.List.prototype.addStaticPosition = function(){
        var dHeight = Fluid.Lib.getHeight(),
            dWidth = Fluid.Lib.getWidth(),
            pos = {
                x: 0,
                y: 0
            };

        if (Fluid.Lib.is([this.conf.position.x, this.conf.position.y])) {
            this.element.style.position = this.conf.mode === "select" ? "absolute" : "fixed";

            if (this.conf.position.y + this.element.prop.height >= dHeight) {
                pos.y = dHeight - this.element.prop.height - 20;
            }
            else {
                pos.y = this.conf.position.y;
            }

            if (this.conf.position.x + this.element.prop.width >= dWidth) {
                pos.x = dWidth - this.element.prop.width - 20;
            }
            else {
                pos.x = this.conf.position.x;
            }

            this.element.style.left = pos.x + "px";
            this.element.style.top = pos.y + "px";
        }
    };
    Fluid.List.prototype.addListeners = function (type, elm) {
        var _this = this, rows;

        switch (type) {
            case "row":
                if (Fluid.Lib.is(elm)) {
                    if(this.conf.skipOnSelect === false){
                        elm.addEventListener("click", function (e) {
                            var ev = e || window.event;

                            if (this.dataset.value) {
                                if(this.dataset.value == _this.conf.defaultValue){
                                    if(_this.conf.skipDefaultValue !== true){
                                        _this.value = this.dataset.value;
                                    }
                                    else{
                                        _this.value = "";
                                    }
                                }
                                else{
                                    _this.value = this.dataset.value;
                                }

                                _this.element.value = _this.value;
                            }

                            _this.conf.onSelect(_this, elm);

                            ev.preventDefault();
                        }, false);
                    }
                    else{
                        _this.conf.onSelect(_this, elm);
                    }
                }
                break;

            case "emitter":
                if (Fluid.Lib.is(this.emitter) && this.emitterEvent !== null) {
                    this.emitter.addEventListener(this.emitterEvent, function (e) {
                        var ev = e || window.event;

                        Fluid.Browser.hideLists();

                        if (_this.hidden === true) {
                            if(_this.conf.positionType === "afterEvent"){
                                _this.conf.position = {
                                    x: ev.clientX,
                                    y: ev.clientY,
                                };

                                _this.addStaticPosition();
                            }
                            _this.show();
                        }
                        else {
                            _this.hide();
                        }
                    }, false);
                }
                break;

            case "hideOnCLick":
                if(this.conf.canHide === true){
                    Fluid.Browser.handleOffEvents({
                        element: _this.element,
                        notClicked: function (element) {
                            if (_this.conf.canHide) {
                                _this.hide();
                            }
                            else {
                                _this.remove();
                            }

                        },
                        excludedElements: [_this.emitter]
                    });
                }

                break;
            case "selectMode":
                if (Fluid.Lib.is(elm)) {
                    rows = Fluid.Lib.toArray(elm.getElementsByTagName("li"));

                    if(rows.length){
                        rows.forEach(function(row){
                            row.addEventListener("click", function(){
                                _this.selectElement.innerHTML = row.innerHTML;
                                _this.hide();
                            });
                        });
                    }
                }

                break;
        }
    };
    Fluid.List.prototype.addSpecialRow = function(conf){
        var elementData, listHeight;

        //special element has fixed or absolute position
        if(conf.element){
            switch(conf.position){
                case "bottom":
                    this.rows.push(conf.element);
                    this.element.appendChild(conf.element);

                    elementData = Fluid.Lib.getStyle(conf.element, "height");

                    if(!elementData.height){
                        this.heightToIncrease = 40;
                    }
                    else{
                        this.heightToIncrease = parseInt(elementData.height, 10);
                    }

                    break;

                case "top":
                default:

                    break;
            }
        }
    };
    Fluid.List.prototype.show = function () {
        var openClass, elementData;

        openClass= this.conf.mode === "select" ? "listOpenSimple" : "listOpen";
        this.hidden = false;

        this.element.classList.remove("listClose");
        this.element.classList.add(openClass);

        if(this.heightToIncrease){
            elementData = this.element.getBoundingClientRect();

            if(this.conf.deleteOnHide === true || !this.alreadyHeightIncreased){
                // this.element.style.height = elementData.height + this.heightToIncrease + "px";
                this.element.style.maxHeight = "none";
                this.alreadyHeightIncreased = true;
            }
        }
    };
    Fluid.List.prototype.hide = function () {
        var openClass;

        openClass= this.conf.mode === "select" ? "listOpenSimple" : "listOpen";
        this.hidden = true;

        this.element.classList.remove(openClass);
        this.element.classList.add("listClose");

        if(this.conf.deleteOnHide === true){
            this.remove(true);
        }
    };
    Fluid.List.prototype.remove = function (withoutHide) {
        withoutHide = withoutHide || false;

        if(withoutHide === false){
            this.hide();
        }

        if (Fluid.Lib.is(this.element.parentNode)) {
            this.element.parentNode.removeChild(this.element);
        }

        delete this;
    };

    Fluid.Panel = function(conf, elm){
        conf = conf || {};

        this.info = {
            name: "Panel",
            id: 11
        };
        this.defaultConf = {
            position: "left",
            title: "Example Panel",
            animation: true,
            dim: true,
            id: null,
            class: [],
            touchShow: false,
            touchHide: true,
            create: false,
            contentToAdd: null,
            showEmitter: "#showPanelEmitter",
            hideEmitter: "#hidePanelEmitter",
            hidden: false,
            removeOnHide: false,
            nextPanelTimeLoad: 500,
            moveContentOnSlide: null,
            type: 'standard' //standard,notify
        };
        this.conf = conf;
        this.element = null;
        this.elementProp = {};
        this.hideBtn = null;
        this.showBtn = null;
        this.data = {
            panel: {
                left: "none",
                right: "none",
                top: "none",
                bottom: "none",
                translate: ""
            },
            hideBtn: {
                left: "none",
                right: "none",
                top: "none",
                bottom: "none",
                class: ""
            }
        };
        this.hidden = null;
        this.touchArea = 20;

        this.initialized = false;

        Fluid.checkCaller(this);
        Fluid.addToElements(this);

        Fluid.Loader.mergeConfiguration(this);

        if (Fluid.Lib.is(elm)) {
            this.element = elm;
            this.elementProp = elm.getBoundingClientRect();
            this.element.fluidObject = this;
        }

        this.hidden = this.conf.hidden === true;

        this.init();
        this.addListeners("init");

        return this;
    };
    Fluid.Panel.prototype.init = function(){
        var _this = this;

        if(!Fluid.Lib.is(this.element)){
            this.createPanel();

            if(this.conf.contentToAdd !== null){
                this.addContent();
            }
        }

        if(this.conf.id !== null){
            this.element.setAttribute("id", this.conf.id);
        }
        this.element.classList.add("panel", "panel" + this.conf.position[0].toUpperCase() + this.conf.position.substr(1));

        handleClass();

        this.handleTitle();
        this.handleDim();
        this.handleEmitters();
        this.prepareElementsData();
        this.setElementsData(true);

        if(this.conf.hidden === false && this.conf.dim === true){
            Fluid.Browser.dim.show();
        }

        this.initialized = true;

        function handleClass(){
            if(_this.conf.class.length){
                _this.conf.class.forEach(function(cl){
                    _this.element.classList.add(cl);
                })
            }
        }
    };
    Fluid.Panel.prototype.createPanel = function(){
        var panel = document.createElement("div"),
            hideBtn = document.createElement("span"),
            hideBtnId = "hideBtn" + generateId();

        hideBtn.setAttribute("id", hideBtnId);
        panel.appendChild(hideBtn);
        Fluid.wrapper.appendChild(panel);

        this.element = panel;
        this.conf.hideEmitter = "#" + hideBtnId;

        function generateId(){
            return Math.round((Math.random() * 1000 ) + 1);
        }
    };
    Fluid.Panel.prototype.addContent = function(){
        var content;

        switch(typeof(this.conf.contentToAdd)){
            case "string":
                content = document.createElement("div");
                content.classList.add("content");
                content.innerHTML = this.conf.contentToAdd;

                this.element.appendChild(content);

                break;
            case "object":
                this.element.appendChild(this.conf.contentToAdd);

                break;
        }
    };
    Fluid.Panel.prototype.handleEmitters = function() {
        var button;

        if(this.conf.hideEmitter){
            if(this.conf.hideEmitter instanceof HTMLElement){
                button = this.conf.hideEmitter
            }
            else{
                button = document.querySelector(this.conf.hideEmitter);
            }

            if(button){
                this.hideBtn = button;

                button.classList.add("fa", "hideBtn");
                this.addListeners("hide", button);
                this.addListeners("afterHide", this.element);
            }
            else{
                //create one ?
            }
        }

        if(this.conf.showEmitter){
            if(this.conf.showEmitter instanceof HTMLElement){
                button = this.conf.showEmitter;
            }
            else{
                button = document.querySelector(this.conf.showEmitter);
            }

            if(button){
                this.showBtn = button;

                button.classList.add("showBtn");
                this.addListeners("show", button);
            }
            else{
                //create one ?
            }

        }
    };
    Fluid.Panel.prototype.handleTitle = function() {
        var title = null;

        if(this.conf.title && this.conf.title.length > 0){
            title = document.createElement("div");
            title.classList.add("title");
            title.innerHTML = this.conf.title;

            this.element.insertBefore(title, this.element.childNodes[0]);
        }
    };
    Fluid.Panel.prototype.handleDim = function() {
        if(this.conf.dim === true){
            if(Fluid.Browser.dim === null){
                new Fluid.Dim();
            }
        }
    };
    Fluid.Panel.prototype.prepareElementsData = function() {
        this.updateDimensions();

        switch (this.conf.position) {
            case "left":
                this.data.panel.left = 0;
                this.data.panel.top = 0;
                this.data.panel.translate = "translateX(-" + this.elementProp.width + "px)";
                this.data.panel.width = null;
                this.data.panel.height = Fluid.Lib.getHeight() + "px";

                this.data.hideBtn.top = "10px";
                this.data.hideBtn.right = "15px";
                this.data.hideBtn.class = "fa-angle-left";
                break;

            case "right":
                this.data.panel.right = 0;
                this.data.panel.top = 0;
                this.data.panel.translate = "translateX(" + this.elementProp.width + "px)";
                this.data.panel.width = null;
                this.data.panel.height = Fluid.Lib.getHeight() + "px";

                this.data.hideBtn.top = "10px";
                this.data.hideBtn.left = "15px";
                this.data.hideBtn.class = "fa-angle-right";
                break;
            case "top":
                this.data.panel.left = 0;
                this.data.panel.top = 0;
                this.data.panel.translate = "translateY(-"+this.elementProp.height+"px)";
                this.data.panel.width = Fluid.Lib.getWidth() + "px";
                this.data.panel.height = null;

                this.data.hideBtn.bottom = "10px";
                this.data.hideBtn.right = "15px";
                this.data.hideBtn.class = "fa-angle-up";
                break;
            case "bottom":
                this.data.panel.left = 0;
                this.data.panel.bottom = 0;
                this.data.panel.translate = "translateY("+this.elementProp.height+"px)";
                this.data.panel.width = Fluid.Lib.getWidth() + "px";
                this.data.panel.height = null;

                this.data.hideBtn.top = "10px";
                this.data.hideBtn.right = "15px";
                this.data.hideBtn.class = "fa-angle-down";
                break;
        }
    };
    Fluid.Panel.prototype.setElementsData = function(firstUse) {
        var _this = this;

        firstUse = firstUse || false;

        this.element.style.left = this.data.panel.left;
        this.element.style.right = this.data.panel.right;
        this.element.style.top = this.data.panel.top;
        this.element.style.bottom = this.data.panel.bottom;

        if(this.data.panel.width !== null){
            this.element.style.width = this.data.panel.width;
        }

        if(this.data.panel.height !== null){
            this.element.style.height = this.data.panel.height;
        }

        if (this.conf.hideEmitter && this.hideBtn !== null) {
            this.hideBtn.classList.add(this.data.hideBtn.class);
            this.hideBtn.style.top = this.data.hideBtn.top;
            this.hideBtn.style.bottom = this.data.hideBtn.bottom;
            this.hideBtn.style.left = this.data.hideBtn.left;
            this.hideBtn.style.right = this.data.hideBtn.right;
        }

        if(this.hidden === true) {
            handleTransition(function () {
                Fluid.Lib.setTransform(_this.element, _this.data.panel.translate);
            }, firstUse);
        }

        function handleTransition(func, firstUse){
            var backupStyles,
                properties = ["transition-property", "transition-duration", "transition-timing-function", "transition-delay"];

            if(Fluid.Lib.isFunction(func)){
                backupStyles = Fluid.Lib.getStyle(_this.element, properties);

                properties.forEach(function(prop){
                    _this.element.style[Fluid.Lib.dashToUpper(prop)] = "none";
                });

                func();

                if(firstUse){
                    setTimeout(function(){
                        properties.forEach(function(prop){
                            _this.element.style[Fluid.Lib.dashToUpper(prop)] = backupStyles[prop];
                        });
                    }, 100)
                }
                else{
                    properties.forEach(function(prop){
                        _this.element.style[Fluid.Lib.dashToUpper(prop)] = backupStyles[prop];
                    });
                }
            }
        }
    };
    Fluid.Panel.prototype.updateDimensions = function(){
        this.elementProp = this.element.getBoundingClientRect();
    };
    Fluid.Panel.prototype.show = function(){
        var wasHidden,
            time = 100,
            _this = this;

        if(this.conf.type === "standard"){
            wasHidden = Fluid.Browser.hidePanels();
        }
        this.prepareElementsData();
        this.setElementsData();
        this.hidden = false;

        if(wasHidden){
            time = this.conf.nextPanelTimeLoad;
        }

        setTimeout(function(){
            Fluid.Lib.setTransform(_this.element, "translate(0px, 0px)");

            if(_this.conf.moveContentOnSlide !== null){
                _this.conf.moveContentOnSlide.style.transition = _this.element.style.transition;

                switch(_this.conf.position){
                    case "top":
                        Fluid.Lib.setTransform(_this.conf.moveContentOnSlide, "translateY("+_this.elementProp.height+"px)");
                        break;
                    case "bottom":
                        Fluid.Lib.setTransform(_this.conf.moveContentOnSlide, "translateY(-"+_this.elementProp.height+"px)");
                        break;
                    case "left":
                        Fluid.Lib.setTransform(_this.conf.moveContentOnSlide, "translateX("+_this.elementProp.width+"px)");
                        break;
                    case "right":
                        Fluid.Lib.setTransform(_this.conf.moveContentOnSlide, "translateX(-"+_this.elementProp.width+"px)");
                        break;
                }
            }

            if(_this.conf.dim === true) {
                Fluid.Browser.dim.show();
            }
        }, time);
    };
    Fluid.Panel.prototype.hide = function(){
        var _this = this;

        this.prepareElementsData();
        this.hidden = true;

        Fluid.Lib.setTransform(this.element, this.data.panel.translate);

        if(this.conf.moveContentOnSlide !== null){
            switch(_this.conf.position){
                case "top":
                    Fluid.Lib.setTransform(this.conf.moveContentOnSlide, "translateY(0px)");
                    break;
                case "bottom":
                    Fluid.Lib.setTransform(this.conf.moveContentOnSlide, "translateY(0px)");
                    break;
                case "left":
                    Fluid.Lib.setTransform(this.conf.moveContentOnSlide, "translateX(0px)");
                    break;
                case "right":
                    Fluid.Lib.setTransform(this.conf.moveContentOnSlide, "translateX(0px)");
                    break;
            }
        }

        if(this.conf.dim === true){
            Fluid.Browser.dim.hide();
        }
    };
    Fluid.Panel.prototype.addListeners = function(type, elm){
        var _this = this;

        if(type){
            switch(type){
                case "init":
                    window.addEventListener("resize", function(){
                        _this.prepareElementsData();
                        _this.setElementsData();
                    });
                    break;
                case "hide":
                    if(elm){
                        elm.addEventListener("click", function(){
                            if(_this.hidden === false){
                                _this.hide();
                            }
                        }, false);
                    }
                    break;
                case "show":
                    if(elm){
                        elm.addEventListener("click", function(){
                            if(_this.hidden === true){
                                _this.show();
                            }
                            else{
                                _this.hide();
                            }

                        }, false);
                    }
                    break;
                case "afterHide":
                    if(elm){
                        if(this.conf.removeOnHide === true){
                            elm.addEventListener("transitionend", function(e){
                                var ev = e || window.event;

                                if(ev.target === elm && _this.hidden === true){ //this is good
                                    _this.element.parentNode.removeChild(_this.element);
                                }
                            });
                        }
                    }
                    break;
            }
        }
    };
    Fluid.Panel.prototype.replaceContent = function(content){
        var elements = Fluid.Lib.toArray(this.element.querySelectorAll("*")),
            newElement;

        elements.forEach(function(element){
            if(!element.classList.contains("hideBtn")){
                element.parentNode.removeChild(element);
            }
        });

        switch(typeof(content)){
            case "string":
                newElement = document.createElement("div");
                newElement.innerHTML = content;

                this.element.appendChild(newElement);
                break;
            case "object":
                this.element.appendChild(content);
                break;
        }
    };

    Fluid.Http = function () {
        /*
         Params:
         url: "",
         contentType: "",
         onProgress: function(){},
         onResponse: function(){},
         toSend: (json) {}
         */

        this.info = {
            name: "Http",
            id: 2
        };
        this.debug = false;
        this.http = null;
    };
    Fluid.Http.prototype.post = function (conf) {
        var _this = this;

        this.http = new XMLHttpRequest();

        if (Fluid.Lib.is(conf.url)) {
            this.http.open("POST", conf.url, true);

            if(conf.responseType){
                this.http.responseType = conf.responseType;
            }

            if(Fluid.Lib.is(conf.contentType)){
                this.http.setRequestHeader("Content-Type", conf.contentType);
            }
            else{
                if(conf.contentType !== false){
                    this.http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                }
            }

            this.http.onreadystatechange = function () {
                if (_this.http.status === 200 && _this.http.readyState === 4) {
                    if (_this.debug) {
                        console.log(_this.http.responseText);
                    }

                    if (Fluid.Lib.is(conf.onResponse) && Fluid.Lib.isFunction(conf.onResponse)) {
                        conf.onResponse(_this.http.responseText);
                    }
                }
            };
            this.http.upload.addEventListener("progress", function (e) {
                if (Fluid.Lib.is(conf.onProgress) && Fluid.Lib.isFunction(conf.onProgress)) {
                    conf.onProgress(e);
                }
            }, false);

            if (!conf.toSend || conf.toSend.length === 0) {
                this.http.send();
            }
            else {
                if (_this.debug) {
                    console.log(conf.toSend);
                }

                this.http.send(conf.toSend);
            }
        }
    };
    Fluid.Http.prototype.get = function (conf) {
        var _this = this;

        this.http = new XMLHttpRequest();

        if (Fluid.Lib.is(conf.url)) {
            if(conf.responseType){
                this.http.responseType = conf.responseType;
            }

            if(conf.toSend && conf.toSend.length){
                conf.toSend = "?" + conf.toSend;
            }
            else{
                conf.toSend = "";
            }

            this.http.open("GET", conf.url + conf.toSend, true);
            this.http.onreadystatechange = function () {
                if (_this.http.status === 200 && _this.http.readyState === 4) {
                    if (_this.debug) {
                        console.log(_this.http.responseText);
                    }

                    if (Fluid.Lib.is(conf.onResponse) && Fluid.Lib.isFunction(conf.onResponse)) {
                        if(conf.responseType === "arraybuffer"){
                            conf.onResponse(_this.http.response);
                        }
                        else{
                            conf.onResponse(_this.http.responseText);
                        }
                    }
                }
            };

            if (!conf.toSend || conf.toSend.length === 0) {
                this.http.send();
            }
            else {
                if (_this.debug) {
                    console.log(conf.toSend);
                }

                this.http.send();
            }
        }
    };

    Fluid.Library = function () {
        this.info = {
            name: "Library",
            id: 1
        };
        this.errors = [];
    };
    Fluid.Library.prototype.is = function (obj) {
        var arg = Fluid.Lib.toArray(arguments),
            errors = 0;

        arg.forEach(function (obj) {
            if (obj === "undefined" || obj === undefined || obj === false || obj === "false" || obj === null || obj === "null") {
                errors++;
            }
        });

        if (errors === 0) {
            return true;
        }
        else {
            this.errors = errors;
            return false;
        }
    };
    Fluid.Library.prototype.toArray = function (obj) {
        var array = [];

        if (obj instanceof Array) {
            array = obj;
        }
        else if (obj instanceof Object) {
            for (prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    array.push(obj[prop]);
                }
            }
        }
        else if (typeof(obj) === "string") {
            array.push(obj);
        }

        return array;
    };
    Fluid.Library.prototype.setTransform = function(elm, property){
        elm.style.WebkitTransform = property;
        elm.style.MozTransform = property;
        elm.style.MsTransform = property;
        elm.style.OTransform = property;
        elm.style.transform = property;
    };
    Fluid.Library.prototype.dashToUpper = function(word){
        var newWord = "";

        if(word.length){
            for(var i = 0; i< word.length; i++){
                if(word[i] === "-"){
                    newWord += word[i + 1].toUpperCase();
                    i++;
                }
                else{
                    newWord += word[i];
                }

            }
        }

        return newWord;
    };
    Fluid.Library.prototype.getStyle = function (elm, props) {
        var elementStyles,
            resultStyles = {};

        elementStyles = window.getComputedStyle(elm, null);

        if(Fluid.Lib.isArray(props) === false){
            props = [props];
        }

        props.forEach(function(prop){
            resultStyles[prop] = elementStyles.getPropertyValue(prop);
        });

        if(resultStyles.length <= 0){
            return null;
        }
        else{
            return resultStyles;
        }
    };
    Fluid.Library.prototype.getUrlParam = function (param) {
        var params = {},
            hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

        param = param || null;

        for (var i = 0, hash = null; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            params[hash[0]] = hash[1];
        }

        if (param === null) {
            return params;
        }
        else {
            if (Fluid.Lib.is(params[param])) {
                return params[param];
            }
            else {
                //Fluid.Message();
            }
        }
    };
    Fluid.Library.prototype.strBoolToBool = function (str) {
        var result = str;

        if (typeof(str) === "string") {
            if (str === "true") {
                result = true;
            }
            else if (str === "false") {
                result = false;
            }
        }

        return result;
    };
    Fluid.Library.prototype.isFunction = function (func) {
        var getType = {};
        return func && getType.toString.call(func) === '[object Function]';
    };
    Fluid.Library.prototype.getHeight = function () {
        var body = document.body,
            bodyProp = body.getBoundingClientRect();

        return bodyProp.height;
    };
    Fluid.Library.prototype.getWidth = function () {
        var body = document.body,
            bodyProp = body.getBoundingClientRect();

        return bodyProp.width;
    };
    Fluid.Library.prototype.isArray = function (array) {
        return Array.isArray(array);
    };
    Fluid.Library.prototype.hasChild = function(haystack, needle){
        var children, response = false;

        if(Fluid.Lib.is([haystack, needle])){
            children = Fluid.Lib.toArray(haystack.childNodes);

            if(children.length > 0){
                children.forEach(function(node){
                    if(node.tagName.toLowerCase() === needle.toLowerCase()){
                        response = true;
                        return true;
                    }
                });
            }
        }

        return response;
    };
    Fluid.Library.prototype.setAttributes = function(element, attrs){
        if(Fluid.Lib.is([element, attrs])){
            for(var attr in attrs){
                element.setAttribute(attr, attrs[attr]);
            }
        }
    };
    Fluid.Library.prototype.iterateOverChildren = function(parent, onChild){
        //to rewrite??
        var children;

        if(parent.childNodes.length){
            children = Fluid.Lib.toArray(parent.childNodes);

            if(Fluid.Lib.isFunction(onChild)){
                if(children.length){
                    return children.forEach(function(child){
                        if(onChild(child) === true){
                            return true;
                        }
                        else{
                            return Fluid.Lib.iterateOverChildren(child, onChild);
                        }
                    });
                }
                else{
                    return false;
                }
            }
            else{
                console.warn("onChild param must be a function.")
                return false;
            }
        }
    };
    Fluid.Library.prototype.iterateOverParents = function(element, onParent){
        var parent;

        if(element.parentNode && element.parentNode !== document){
            parent = element.parentNode;

            if(Fluid.Lib.isFunction(onParent)){
                if(onParent(parent, element) === true){
                    return true;
                }
                else{
                    return Fluid.Lib.iterateOverParents(parent, onParent);
                }
            }
            else{
                console.warn("onParent param must be a function.");
                return false;
            }
        }
        else{
            return false;
        }
    };
    Fluid.Library.prototype.generateRandomKey = function(length){
        var ret = "";

        while (ret.length < length) {
            ret += Math.random().toString(16).substring(2);
        }

        return ret.substring(0, length);
    };
    Fluid.Library.prototype.generateRandomId = function(){
        var id = Math.floor((Math.random() * 2000) + 1000);

        if(Fluid.randomId.indexOf(id) === -1){
            Fluid.randomId.push(id);

            return id;
        }
        else{
            Fluid,Lib.generateRandomId();
        }
    };

    Fluid.Message = function (index, subIndex, mod, display, type) {
        var modName = null,
            type = type || "warn",
            info = {
                name: "Message",
            },
            messages = {};

        if (Fluid.Lib.is(mod)) {
            modName = mod.info.name || "Some Module";
        }

        if (display !== false) {
            display = true;
        }

        messages = {
            0: {
                0: Fluid.info.name + " are already started.",
                1: Fluid.info.name + " Your configuration is corruped."
            },
            1: {
                0: Fluid.info.name + " - " + Fluid.modules[1] + ": Error array is not empty.",
            },
            10: {
                0: Fluid.info.name + " - " + Fluid.modules[10] + ": You must provide configuration.",
                1: Fluid.info.name + " - " + Fluid.modules[10] + ": You can't show and hide modal in the same time.",
                2: Fluid.info.name + " - " + Fluid.modules[10] + ": You can't hide modal, if you haven't showed it before.",
                3: Fluid.info.name + " - " + Fluid.modules[10] + ": You can't show modal, if you haven't hidden it before.",
                4: Fluid.info.name + " - " + Fluid.modules[10] + ": You can't get ajax content and ordinary content in the same time.",
                5: Fluid.info.name + " - " + Fluid.modules[10] + ": Already initialized.",
            },
            11: {
                0: Fluid.info.name + " - " + Fluid.modules[11] + ": You must provide configuration.",
            },
            12: {
                0: Fluid.info.name + " - " + Fluid.modules[12] + ": You must provide configuration.",
            },
            15: {
                0: Fluid.info.name + " - " + Fluid.modules[15] + ": You must provide configuration.",
                1: Fluid.info.name + " - " + Fluid.modules[15] + ": You must provide some submit button.",
            },
            16: {
                0: Fluid.info.name + " - " + Fluid.modules[16] + ": You must provide configuration.",
                1: Fluid.info.name + " - " + Fluid.modules[16] + ": You must pass `append` property.",
            },
            17: {
                0: Fluid.info.name + " - " + Fluid.modules[17] + ": You must provide configuration.",
                6: Fluid.info.name + " - " + Fluid.modules[17] + ": Element wasn't created.",
            },
            18: {
                0: Fluid.info.name + " - " + Fluid.modules[18] + ": You must provide configuration.",
            },
            19: {
                0: Fluid.info.name + " - " + Fluid.modules[19] + ": You must provide configuration.",
                1: Fluid.info.name + " - " + Fluid.modules[19] + ": History popstate event is locked by another instance of Fluid Navigation.",
            },
            x: Fluid.info.name + " - " + modName + ": Cannot call " + modName + " as regular function.",
        };

        if (Fluid.Lib.is(index, subIndex)) {
            if (display) {
                console[type](messages[index][subIndex]);
            }
            else {
                return messages[index][subIndex];
            }
            return;
        }

        if (Fluid.Lib.is(index, mod)) {
            if (display) {
                console[type](messages[index]);
                return;
            }
            else {
                return messages[index];
            }
        }

        console.error(Fluid.info.name + " - " + info.name + ": You didn't provide required parameters.");
        return;
    };

    Fluid.TemplateClass = function(conf, elm){
        this.info = {
            name: "TemplateClass",
            id: 'xxxx'
        };
        this.defaultConf = {};
        this.conf = conf || {};
        this.element = null;
        this.initialized = false;

        Fluid.checkCaller(this);
        Fluid.addToElements(this);

        Fluid.Loader.mergeConfiguration(this);

        if(Fluid.Lib.is(elm)){
            this.element = elm;
        }

        this.init();

        return this;
    };
    Fluid.TemplateClass.prototype.init = function(){
        var _this = this;

        if (this.initialized === false) {
            if(this.element !== null){

            }
            else{
                //error
            }

            this.initialized = true;
        }
        else{
            //error
        }
    };
    Fluid.TemplateClass.prototype.addListener = function(type, elm, callback){
        var _this = this;

        if(Fluid.Lib.is(type)){
            switch (type){
                default:
                    if(Fluid.Lib.is(elm)){
                        elm.addEventListener("event", function(){});
                    }
            }
        }
        else{

        }
    };

    Fluid.init();
}());
