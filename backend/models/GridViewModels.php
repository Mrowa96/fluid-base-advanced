<?php
    namespace backend\models;

    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%models_list}}".
     *
     * @property integer $id
     * @property integer $namespace
     */
    class GridViewModels extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%grid_view_models}}';
        }

        public function rules()
        {
            return [
                [['namespace'], 'required'],
                [['namespace'], 'string', 'max' => 128],
            ];
        }
    }
