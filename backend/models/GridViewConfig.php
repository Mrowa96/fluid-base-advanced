<?php
    namespace backend\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\Html;

    /**
     * This is the model class for table "{{%grid_view_config}}".
     *
     * @property integer $user_id
     * @property integer $model_id
     * @property string $property
     * @property bool $value
     * @property string $type
     * @property string $class
     * @property integer $position
     */
    class GridViewConfig extends ActiveRecord
    {

        public static function tableName()
        {
            return '{{%grid_view_config}}';
        }

        public function rules()
        {
            return [
                [['user_id', 'model_id', 'property'], 'required'],
                [['user_id', 'model_id', 'value', 'position'], 'integer'],
                [['property', 'class', 'type'], 'string', 'max' => 128],
            ];
        }

        public static function prepareData($modelNamespace)
        {
            $result = [];

            if(class_exists($modelNamespace)){
                /** @var GridViewModels $model */
                $model = GridViewModels::find()->where(['namespace' => $modelNamespace])->one();

                if($model === null){
                    $model = new GridViewModels();
                    $model->namespace = $modelNamespace;
                    $model->save();
                }

                if(self::issetForUser($model) === false){
                    self::createColumns($model);
                }

                $girdViewProperties = GridViewConfig::find()->where([
                    'model_id' => $model->id,
                    'user_id' => Yii::$app->user->id
                ])->orderBy('position')->all();

                foreach($girdViewProperties AS $prop) {
                    if ($prop->value == 1) {

                        $value = function ($data, $prop) use ($prop) {
                            if(mb_strpos($prop->property, ".") !== false){
                                $property = self::accessSubproperty($data, $prop->property);
                            }
                            else{
                                $property = $data->{$prop->property};
                            }

                            switch ($prop->type) {
                                case "image":
                                    return Html::img($property, [
                                        'class' => $prop->class . ' img-' . $prop->property
                                    ]);

                                    break;
                                case "text":
                                    return Html::tag("span", $property, [
                                        'class' => $prop->class,
                                    ]);

                                    break;
                            }
                        };

                        $result[] = [
                            'attribute' => $prop->property,
                            'format' => 'html',
                            'value' => $value
                        ];
                    }
                }
            }

            return $result;
        }
        public static function prepareColumns($modelNamespace)
        {
            $result = [];

            if(Yii::$app->user->can("configure grid view") && class_exists($modelNamespace)) {
                $model = GridViewModels::find()->where(['namespace' => $modelNamespace])->one();

                if($model){
                    /** @var GridViewModels $modelObject */
                    $modelObject = new $model->namespace();

                    if($modelObject){
                        $girdViewProperties = GridViewConfig::find()->where([
                            'user_id' => Yii::$app->user->id,
                            'model_id' => $model->id
                        ])->orderBy('position')->all();


                        foreach($girdViewProperties AS $prop){
                            $result[$prop->property] = [
                                'key' => $prop->property,
                                'label' => $modelObject->getAttributeLabel($prop->property),
                                'set' => $prop->value
                            ];
                        }
                    }
                }
            }
            
            return $result;
        }
        public static function saveColumns($columnsData, $modelNamespace){
            if(class_exists($modelNamespace)){
                $model = GridViewModels::find()->where(['namespace' => $modelNamespace])->one();

                if($model){
                    $columns = GridViewConfig::find()->where([
                        'user_id' => Yii::$app->user->id,
                        'model_id' => $model->id
                    ])->all();

                    foreach($columns AS $column){
                        foreach($columnsData AS $data){
                            if($column->property == $data->key){
                                $object = GridViewConfig::findOne($column->id);
                                $object->value = $data->value;
                                $object->save();
                            }
                        }
                    }
                }

                return true;
            }

            return false;
        }

        private static function issetForUser($model){
            $userId = Yii::$app->user->id;
            $exampleRow = GridViewConfig::find()->where([
                'user_id' => $userId,
                'model_id' => $model->id
            ])->one();

            if($exampleRow){
                return true;
            }

            return false;
        }
        private static function createColumns($model){
            $userId = Yii::$app->user->id;
            $modelObject = new $model->namespace();

            if($modelObject){
                if(method_exists($modelObject, "getGridViewExample")){
                    $columns = $modelObject->getGridViewExample();

                    foreach($columns AS $index => $column){
                        /** @var GridViewConfig $columnObject */
                        $columnObject = new GridViewConfig();

                        if(isset($column['property'])){
                            $columnObject->user_id = $userId;
                            $columnObject->model_id = $model->id;
                            $columnObject->property = $column['property'];

                            if(isset($column['value'])){
                                $columnObject->value = $column['value'];
                            }

                            if(isset($column['type'])){
                                $columnObject->type = $column['type'];
                            }

                            if(isset($column['class'])){
                                $columnObject->class = $column['class'];
                            }

                            $columnObject->position = isset($column['position']) ? $column['position'] : $index;

                            $columnObject->save();
                        }
                    }

                    return true;
                }
                else{
                    throw new \Exception("Model " . $model->namespace . ' has to got getGridViewExample method.');
                }
            }
            else{
                throw new \Exception("Problem occured during createing " . $model->namespace . "object.");
            }
        }
        private static function accessSubproperty($object, $accessString) {
            $parts = explode('.', $accessString);
            $tmp = $object;

            while(count($parts)) {
                $tmp = $tmp->{array_shift($parts)};
            }

            return $tmp;
        }
    }
