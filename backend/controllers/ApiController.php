<?php
    namespace backend\controllers;

    use Yii;
    use yii\helpers\Json;
    use backend\models\GridViewConfig;
    use common\models\Language;

    class ApiController extends BaseController{

        public function beforeAction($action)
        {
            $this->enableCsrfValidation = false;

            return parent::beforeAction($action);
        }

        public function actionGetStats()
        {
            return Json::encode([
                'months' => [
                    1 => 23,
                    2 => 3,
                    3 => 9,
                    4 => 5,
                    5 => 18,
                    6 => 30,
                    7 => 35,
                    8 => 25,
                    9 => 19,
                    10 => 27,
                    11 => 43,
                    12 => 50,
                ]
            ]);
        }

        public function actionGetModelData($name){
            $results = [];

            if(class_exists($name)){
                $results = GridViewConfig::prepareColumns($name);
            }

            echo Json::encode($results);
            exit;
        }

        public function actionSaveGridViewSettings(){
            $data = Yii::$app->request->post('data');
            $response = ['status' => 0, 'message' => Yii::t("system", "An unexpected error occurred, please contact with administrator.")];

            if($data){
                $data = Json::decode($data, false);

                if(class_exists($data->model)){
                    if(!Yii::$app->user->can("configure grid view")){
                        $response = ['status' => 0, 'message' => Yii::t("system", "You are not allowed to save this settings.")];
                    }
                    else if(GridViewConfig::saveColumns($data->keys, $data->model)){
                        $response = ['status' => 1, 'message' => Yii::t("system", "Settings have been successfully saved.")];
                    }
                }
            }
            echo Json::encode($response);
            exit;
        }

        public function actionGetTranslations()
        {
            return Json::encode([
                'status' => true,
                'language' => Yii::$app->language,
                'translations' => Language::groupByCategories("backend")
            ]);
        }
    }
