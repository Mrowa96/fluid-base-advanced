<?php
    namespace backend\controllers;

    use Yii;
    use yii\data\ActiveDataProvider;
    use yii\web\NotFoundHttpException;
    use common\models\Language;

    class LanguageController extends BaseController
    {
        public function actionIndex($module)
        {
            $dataProvider = new ActiveDataProvider([
                'query' => Language::find()->where(['module' => $module])
            ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider
            ]);
        }

        public function actionCreate()
        {
            /** @var Language $model */
            $model = new Language();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirectWithMessage(['index', 'module' => $model->module], $this->getMessage('okCreate'));
            }

            return $this->render('create', [
                'model' => $model
            ]);
        }
        public function actionUpdate($id)
        {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirectWithMessage(['index', 'module' => $model->module], $this->getMessage('okUpdate'));
            }

            return $this->render('update', [
                'model' => $model
            ]);
        }
        public function actionManage($id)
        {
            $language = Language::findOne($id);

            if(Yii::$app->request->isPost && Language::saveMessages($id)) {
                return $this->redirect(['index', 'module' => $language->module]);
            }
            else{
                return $this->render('manage', [
                    'data' => Language::prepareForManage($id),
                ]);
            }
        }
        public function actionDelete($id)
        {
            $model = $this->findModel($id);

            if($model->delete()){
                return $this->redirectWithMessage(['index', 'module' => $model->module], $this->getMessage('okDelete'));
            }
            else{
                return $this->redirectWithMessage(['index', 'module' => $model->module], $this->getMessage('noDelete'), "error");
            }
        }
        protected function findModel($id){
            /** @var Language $model */
            if (($model = Language::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }