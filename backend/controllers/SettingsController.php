<?php
    namespace backend\controllers;

    use Yii;
    use common\models\Settings;
    use common\models\Settings\Information;
    use common\models\Settings\Sms;
    use common\models\Settings\Smtp;
    use common\models\Settings\System;
    use common\models\Language;

    class SettingsController extends BaseController{

        public function actionIndex()
        {
            if(Yii::$app->user->can("display settings")){
                $model = new Settings();;
                $data = [
                    'model' => $model,
                    'themes' => Settings::getThemes(),
                    'languages' => Language::prepareForForm("backend"),
                    'information' => (new Information())->prepareForForm(),
                    'smtp' => (new Smtp())->prepareForForm(),
                    'system' => (new System())->prepareForForm(),
                    'sms' => (new Sms())->prepareForForm()
                ];

                if(Yii::$app->request->isPost){
                    if(Yii::$app->user->can("save settings")){
                        if ($model->loadData(Yii::$app->request->post()) && $model->saveData()) {
                            return $this->redirectWithMessage(['index'],
                                Yii::t("system", "Settings have been successfully saved."));
                        }

                        return $this->renderWithMessage('index',
                            Yii::t("system", "An unexpected error occurred, please contact with administrator."),
                            $data, "error");
                    }
                    else{
                        $this->redirectWithMessage(['index'],
                            Yii::t("system", "Error occured, you don't have administrator permission."));
                    }
                }
                else {
                    return $this->render('index', $data);
                }
            }
            else{
                $this->redirectWithMessage(['site/index'],
                    Yii::t("system", "You do not have correct permissions to access that site."));
            }
        }
    }
