<?php
    namespace backend\controllers;

    use Yii;
    use yii\helpers\Json;
    use fluid\fileManager\Directory;
    use fluid\fileManager\File;
    use common\models\Settings;
    use common\models\Settings\System;

    class FileManagerController extends BaseController{

        private $excludeActions = ['index','download', 'get-url'];
        private $relativePath = "";
        private $path = null;
        private $data = [];
        private $response = [];
        private $fatalResponse = [
            'failed' => true,
            'message' => 'Wystąpił nieoczekiwany błąd.'
        ];

        public function beforeAction($action){
            $this->response = $this->fatalResponse;

            $fileManagerEnabled = Settings::getOne("file_manager", System::MODULE);

            if(!$fileManagerEnabled){
                $this->redirect(["site/index"]);
            }

            if(!in_array($action->id, $this->excludeActions)){
                $this->enableCsrfValidation = false;
                $this->path = Yii::getAlias("@frontend") . '/web';

                if(Yii::$app->request->post("data")){
                    $this->data = Json::decode(Yii::$app->request->post("data"));
                }
                else if($action->id !== "add"){
                    exit;
                }
            }

            return parent::beforeAction($action);
        }

        public function actionIndex(){
            return $this->render('index');
        }
        public function actionAdd(){
            $params = Json::decode(Yii::$app->request->post("extraData"));
            $fileData = $_FILES['file'];
            $filePath = $this->path.$params['dir'] . DS . $fileData['name'];
            $file = new File($filePath);

            try{
                $file->saveUploadedFile($fileData);
                $this->response['failed'] = false;
            }
            catch(\Exception $ex){
                switch($ex->getCode()){
                    case 1:
                        $message = "Plik jest za duży.";
                        break;
                    case 2:
                        $message = "Plik jest za duży.";
                        break;
                    case 3:
                        $message = "Plik został przesłany tylko częściowo.";
                        break;
                    case 4:
                        $message = "Żaden plik nie został wysłany.";
                        break;
                    case 6:
                        $message = "Wystąpił błąd po stronie serwera.";
                        break;
                    case 7:
                        $message = "Brak uprawnień zapisu";
                        break;
                    case 8:
                        $message = "Nieznany błąd.";
                        break;
                    default:
                        $this->response['message'] = 'Nieznany błąd.';
                        break;
                }
            }

            echo Json::encode($this->response);
            exit;
        }
        public function actionContent(){
            if(isset($this->data['path']) && !empty($this->data['path'])) {
                $this->data['path'] = $this->handleLastChar($this->data['path']);

                $this->relativePath .= $this->data['path'];
                $this->path .= $this->data['path'];

                if (!empty($this->relativePath) && !empty($this->path)) {
                    $results = array_diff(scandir($this->path), ['.', '..']);
                    $this->response = [];
                    $folders = [];
                    $files= [];

                    foreach ($results AS $result) {
                        if ($result[0] !== ".") {
                            if (Directory::is($this->path . $result)) {
                                $folder = new Directory($this->path . $result);

                                $folderData = [
                                    'name' => $folder->name,
                                    'dir' => $this->data['path'],
                                    'path' => $this->data['path']. $folder->name,
                                    'size' => $folder->size,
                                    'type' => 'folder'
                                ];

                                $folders[] = $folderData;
                            }
                            else if (File::is($this->path . $result)) {
                                $file = new File($this->path . $result);

                                if(mb_strpos($file->basename, ".thumb") === false){
                                    $fileData = [
                                        'name' => $file->name,
                                        'basename' => $file->basename,
                                        'ext' => $file->extension,
                                        'dir' => $this->data['path'],
                                        'path' => Yii::getAlias("@frontendLink") . $this->data['path'] . $file->basename,
                                        'category' => $file->category,
                                        'size' => $file->size,
                                        'thumb' => ($file->thumbnail !== null) ? Yii::getAlias("@frontendLink") . $this->data['path'] . $file->thumbnail->basename : null,
                                        'type' => 'file'
                                    ];

                                    $files[] = $fileData;
                                }
                            }
                        }
                    }

                    $this->response = array_merge($folders, $files);

                    if(empty($this->response)){
                        $this->response = $this->fatalResponse;
                    }
                }
            }

            echo Json::encode($this->response);
            exit;
        }
        public function actionRename(){
            if(isset($this->data['newName']) && !empty($this->data['newName'])){
                $path = $this->path . $this->data['dir'] . $this->data['oldName'];

                if(File::is($path)){
                    $object = new File($path);
                }
                else if(Directory::is($path)){
                    $object = new Directory($path);
                }

                try{
                    if(isset($object) && $object->writable){
                        if($object->rename($this->data['newName'])){
                            $this->response = ['failed' => false];
                        }
                    }
                }
                catch(\Exception $ex){
                    switch($ex->getCode()){
                        case 1:
                            $message = "Wystąpił nieoczekiwany błąd.";
                            break;
                        case 2:
                            $message = "Obiekt o podanej nazwie już istnieje.";
                            break;
                        default:
                            $message = "Wystąpił nieoczekiwany błąd.";
                            break;
                    }

                    $this->response['message'] = $message;
                }
            }
            else{
                $this->response['message'] = "Nazwa nie może być pusta.";
            }

            echo Json::encode($this->response);
            exit;
        }
        public function actionDelete(){
            if (!empty($this->data)) {
                foreach ($this->data AS $data) {
                    if (isset($data['dir']) && isset($data['name'])) {
                        $finalPath = $this->path.$data['dir'].$data['name'];

                        if(File::is($finalPath)){
                            $object = new File($finalPath);
                        }
                        else if(Directory::is($finalPath)){
                            $object = new Directory($finalPath);
                        }

                        if(isset($object) && $object->writable){
                            try{
                                $object->remove();
                                $this->response = ['failed' => false];
                            }
                            catch(\Exception $ex){
                                switch($ex->getCode()){
                                    default:
                                        $this->response['message'] = 'Wystąpił nieoczekiwany błąd.';
                                        break;
                                }
                            }
                        }
                        else{
                            $this->response['message'] = 'Wystąpił nieoczekiwany błąd.';
                        }
                    }
                }
            }

            echo Json::encode($this->response);
            exit;
        }
        public function actionDownload(){
            if(Yii::$app->request->get("path")){
                $file = Yii::$app->request->get("path");

                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($file).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                readfile($file);

                exit;
            }
        }
        public function actionNewDirectory(){
            if(isset($this->data['name']) && !empty($this->data['name']) && isset($this->data['dir'])){
                $path = $this->path.$this->data['dir'].DS.$this->data['name'];

                try{
                    $dir = new Directory($path);
                    $dir->create();
                    $this->response = ['failed' => false];
                }
                catch(\Exception $ex){
                    switch($ex->getCode()){
                        case 0:
                            $message = "Folder o takiej nazwie już istnieje.";
                            break;
                        default:
                            $message = "Wystąpił nieznany błąd.";
                            break;
                    }

                    $this->response['message'] = $message;
                }
            }

            echo Json::encode($this->response);
            exit;
        }
        public function actionCopy(){
            if(isset($this->data['paths']) && !empty($this->data['paths'])) {
                $placeToPaste = $this->data['placeToPaste'];
                $paths = $this->data['paths'];

                if(!empty($paths)){
                    foreach($paths AS $path){
                        if(isset($path['dir']) && isset($path['name'])){
                            $oldPath = $this->path.$path['dir'].$path['name'];
                            $newPath = $this->path.$placeToPaste.DS.$path['name'];

                            try{
                                $object = null;

                                if(File::is($oldPath)){
                                    $object = new File($oldPath);
                                }
                                else if(Directory::is($oldPath)){
                                    $object = new Directory($oldPath);
                                }

                                if($object !== null){
                                    $object->copy($newPath);
                                    $this->response = ['failed' => false];
                                }
                            }
                            catch(\Exception $ex){
                                switch($ex->getCode()){
                                    case 0:
                                        $message = "Obiekt o takiej nazwie już istnieje.";
                                        break;
                                    default:
                                        $message = $ex->getMessage();
                                        break;
                                }

                                $this->response['message'] = $message;
                            }
                        }
                    }
                }
            }

            echo Json::encode($this->response);
            exit;
        }
        public function actionMove(){
            if(isset($this->data['paths']) && !empty($this->data['paths'])) {
                $placeToPaste = $this->data['placeToPaste'];
                $paths = $this->data['paths'];

                if(!empty($paths)){
                    foreach($paths AS $path){
                        if(isset($path['dir']) && isset($path['name'])){
                            $oldPath = $this->path.$path['dir'].$path['name'];
                            $newPath = $this->path.$placeToPaste.DS.$path['name'];

                            try{
                                $file = new File($oldPath);
                                $file->move($newPath);
                                $this->response = ['failed' => false];
                            }
                            catch(\Exception $ex){
                                switch($ex->getCode()){
                                    case 0:
                                        $message = "Obiekt o takiej nazwie już istnieje.";
                                        break;
                                    default:
                                        $message = "Wystąpił nieznany błąd.";
                                        break;
                                }

                                $this->response['message'] = $message;
                            }
                        }
                    }
                }
            }

            echo Json::encode($this->response);
            exit;
        }
        public function actionGetUrl(){
            $this->response = ['url' => Yii::getAlias("@web")];

            echo Json::encode($this->response);
            exit;
        }

        private function handleLastChar($string){
            if($string[mb_strlen($string) - 1] !== "/"){
                return $string."/";
            }
            else{
                return $string;
            }
        }
    }