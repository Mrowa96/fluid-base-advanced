<?php
    namespace backend\controllers;

    use common\models\Settings;
    use common\models\Settings\System;
    use common\utilities\Language;
    use Yii;
    use common\forms\Login as LoginForm;
    use common\models\User;

    class SiteController extends BaseController{

        public function beforeAction($action)
        {
            if($action->id === "save-grid-view-settings"){
                $this->enableCsrfValidation = false;
            }

            return parent::beforeAction($action);
        }

        public function actionIndex()
        {
            $this->view->registerJsFile("/scripts/stats.js");

            return $this->render('index', [
                'users' => User::find()->where(['status' => User::STATUS_ACTIVE])->all(),
            ]);
        }

        public function actionLogin()
        {
            $this->layout = "auth";

            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }

            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->redirect(['index']);
            }
            return $this->render('login', [
                'model' => $model,
            ]);
        }

        public function actionLogout()
        {
            Yii::$app->user->logout();

            $session = Yii::$app->session;
            $session->remove("adminAccess");

            return $this->goHome();
        }

        public function actionChangeLang($symbol)
        {
            if(Settings::getOne('language_level', System::MODULE) === "cookie"){
                $language = new Language();
                $language->setCookieLanguage($symbol);
            }

            return $this->goBack();
        }
    }
