<?php
    namespace backend\controllers;

    use common\utilities\Language;
    use Yii;
    use yii\base\Exception;
    use yii\filters\AccessControl;
    use yii\helpers\ArrayHelper;
    use yii\web\Controller;
    use backend\models\Menu;
    use common\models\Settings;
    use common\utilities\ClassicMenu;

    class BaseController extends Controller{

        protected $messages;

        public function behaviors()
        {
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'actions' => ['login'],
                            'roles' => ['?'],
                        ],
                        [
                            'allow' => true,
                            'roles' => ['admin', 'worker'],
                        ],
                    ]
                ],
            ];
        }

        public function actions()
        {
            return [
                'error' => [
                    'class' => 'yii\web\ErrorAction',
                ],
            ];
        }

        public function beforeAction($action)
        {
            if(Yii::$app->user->id){
                foreach(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id) AS $role => $data){
                    if($role === "client"){
                        throw new Exception("You don't have access");
                    }
                }
            }

            $language = new Language();
            $language->manage();

            $session = Yii::$app->session;
            $sidebarMenu = new ClassicMenu();
            $sidebarMenu->addData(Menu::fetchForMenu())->addHeader(Yii::t("system", "Navigation"));

            if($session->has("messageCounter")){
                $counter = $session->get("messageCounter");

                if($counter < 1){
                    if($session->has("message")){
                        $session->remove("message");
                    }
                    if($session->has("type")){
                        $session->remove("type");
                    }
                }
                else{
                    $session->set("messageCounter", 0);
                }
            }

            $this->messages = [
                'noView' => "Object cannot be viewed.",
                'noCreate' => "Object cannot be created.",
                'noUpdate' => "Object cannot be updated.",
                'noDelete' => "Object cannot be deleted.",
                'okCreate' => "Object was created successfully.",
                'okUpdate' => "Object was updated successfully.",
                'okDelete' => "Object was deleted successfully.",
            ];

            $this->view->title = Settings::getOne('site_title', Settings\System::MODULE);
            $this->view->params['theme'] = Settings::getOne('theme', Settings\System::MODULE);
            $this->view->params['sidebarMenu'] = $sidebarMenu;

            return parent::beforeAction($action);
        }

        public function redirectWithMessage(array $url, $message, $type = "success", $statusCode = 302)
        {
            Yii::$app->session->set("message", $message);
            Yii::$app->session->set("type", $type);
            Yii::$app->session->set("messageCounter", 1);

            return parent::redirect($url, $statusCode);
        }

        public function renderWithMessage($view, $message, $params = [], $type = "success")
        {
            $this->view->params['message'] = $message;
            $this->view->params['type'] = $type;

            return parent::render($view, $params);
        }

        public function getMessage($index)
        {
            if(isset($this->messages[$index])){
                return Yii::t("system", $this->messages[$index]);
            }
            else{
                return Yii::t("system", "Message not found");
            }
        }

        public function getRoles()
        {
            $roles = Yii::$app->authManager->getRoles();

            return ArrayHelper::map($roles, 'name', 'description');
        }
    }
