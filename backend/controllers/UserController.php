<?php
    namespace backend\controllers;

    use backend\models\GridViewConfig;
    use Yii;
    use common\models\User;
    use yii\helpers\ArrayHelper;
    use yii\web\NotFoundHttpException;
    use yii\web\Response;
    use yii\widgets\ActiveForm;
    use backend\models\UserCreate;
    use backend\models\UserUpdate;
    use backend\searches\User as UserSearch;

    class UserController extends BaseController{

        public function actionIndex()
        {
            $searchModel = new UserSearch();
            $gridViewColumns = GridViewConfig::prepareData("common\\models\\User");
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'gridViewColumns' => $gridViewColumns
            ]);
        }
        public function actionView($id)
        {
            if(Yii::$app->user->can("view self user") || Yii::$app->user->can("view users")){
                $model = $this->findModel($id);

                if(Yii::$app->user->can("view self user") && $model->id != Yii::$app->user->getId()){
                    return $this->redirectWithMessage(['index'],
                        Yii::t("system", "You are not allowed to view this object."), "warning");
                }

                return $this->render('view', [
                    'model' => $model,
                ]);
            }
        }
        public function actionCreate()
        {
            if(Yii::$app->user->can("create user")){
                $model = new UserCreate();
                $roles = Yii::$app->authManager->getRoles();
                $roles = ArrayHelper::map($roles, 'name', 'description');
                $avatars = User::getAvatars();

                if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;

                    return ActiveForm::validate($model);
                }

                if ($model->load(Yii::$app->request->post()) && $model->create()) {
                    return $this->redirectWithMessage(['view', 'id' => $model->user->id],
                        Yii::t("user", "Object was created successfully."));
                }
                else {
                    return $this->render('create', [
                        'model' => $model,
                        'roles' => $roles,
                        'avatars' => $avatars,
                    ]);
                }
            }
            else{
                return $this->redirectWithMessage(['index'],
                    Yii::t("user", "You are not allowed to create new user."), "warning");
            }
        }
        public function actionUpdate($id)
        {
            $user = Yii::$app->user;

            if($user->can('update users') || $user->can('update self user')){
                $model = new UserUpdate($id);
                $roles = Yii::$app->authManager->getRoles();
                $roles = ArrayHelper::map($roles, 'name', 'description');
                $avatars = User::getAvatars();

                if($user->can('update self user') && $model->id != $user->getId()){
                    return $this->redirectWithMessage(['index'],
                        Yii::t("system", "You are not allowed to edit this object."), "warning");
                }

                if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;

                    return ActiveForm::validate($model);
                }

                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirectWithMessage(['view', 'id' => $model->id],
                        Yii::t("system", "Object was updated successfully."));
                }
                else {
                    return $this->render('update', [
                        'model' => $model,
                        'roles' => $roles,
                        'avatars' => $avatars,
                    ]);
                }
            }
            else{
                return $this->redirectWithMessage(['index'],
                    Yii::t("system", "You are not allowed to edit this object."), "warning");
            }
        }
        public function actionDelete($id)
        {
            $user = Yii::$app->user;

            if($user->can("delete users") || $user->can("delete self user")){
                $model = $this->findModel($id);

                if($model->deletable === 1){
                    $userString = $model->username . "(" . $model->name . ")";

                    if($user->can("delete self user") && $model->id != $user->getId()){
                        return $this->redirectWithMessage(['index'],
                            Yii::t("system", "You are not allowed to delete this object."), "warning");
                    }

                    if($model->delete()){
                        if(Yii::$app->user->identity->getId() == $model->id){
                            Yii::$app->user->logout();
                        }

                        return $this->redirectWithMessage(['index'],
                            Yii::t("user", "Account {account} was successfully deleted.", ['account' => $userString]));
                    }
                }
                else{
                    return $this->redirectWithMessage(['index'],
                        Yii::t("system", "This object cannot be deleted."), "warning");
                }
            }
        }
        protected function findModel($id){
            if (($model = User::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
