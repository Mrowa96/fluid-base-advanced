<?php
    namespace backend\assets;

    use yii\web\AssetBundle;
    use \yii\web\View;

    class AppAsset extends AssetBundle
    {
        public $basePath = '@webroot';
        public $baseUrl = '@web';
        public $css = [
            "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
            'styles/_font-awesome/font-awesome.min.css',
            'styles/_project/app.css',
            'styles/layout.css',
            'styles/skins.css',
            'styles/_fluid-elements/app.css',
        ];
        public $js = [
            "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js",
            'scripts/_yii/cookie.js',
            'scripts/_fluid-elements/fluid-core.js',
            'scripts/_fluid-elements/fluid-instance.js',
            'scripts/_chartjs/Chart.min.js',
            'scripts/admin.js',
        ];
        public $jsOptions = ['position' => View::POS_HEAD];
        public $depends = [
            'yii\web\YiiAsset',
        ];
    }