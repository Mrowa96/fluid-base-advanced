<?php
    use yii\helpers\Html;
    use common\utilities\GridView;
    use fluid\elements\TabbedView;

    $this->title = Yii::t("navigation", "Languages");
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-content">
        <?php
        $tabbedView = new TabbedView();
        $tabbedView->addControl(Yii::t("language","All languages"), "all")
            ->addControl(Html::a("+ " . Yii::t("language","Add language"), ['create'], [
                'class' => 'controlBtn controlBtn-success',
            ]))->addSection(GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    'name',
                    'symbol',
                ],
                'buttons' => [
                    'manage' => function($url, $model){
                        return '<a href="'.$url.'" title="'.Yii::t("system","Manage").'" class="action"></a>';
                    },
                    'update' => function($url, $model){
                        return '<a href="'.$url.'" title="'.Yii::t("system","Edit").'" class="action"></a>';
                    },
                    'delete' => function($url, $model){
                        return '<a href="'.$url.'" title="'.Yii::t("system","Delete").'" class="action confirm"></a>';
                    },
                ],
                'layout'=>"{items}\n{pager}",]), "all")
            ->render();
        ?>
    </div>
</div>
