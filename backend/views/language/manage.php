<?php
    use yii\helpers\Html;
    use common\utilities\GridView;
    use fluid\elements\TabbedView;

    $this->title = Yii::t("navigation", "Languages");
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Languages'), 'url' => ['index']];
?>
<div class="box language-manage">
    <div class="box-content">
        <?php if(!empty($data)): ?>
            <form method="POST" id="languageForm">
                <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken();?>">

                <?php foreach($data AS $lang => $translations): ?>
                    <section id="category_<?= $lang; ?>" class="row category">
                        <div class="col-lg-12 col-md-12">
                            <h4><?= ucfirst($lang); ?></h4>
                        </div>
                        <?php foreach($translations as $translation): ?>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="<?= $translation['id']; ?>"><?= $translation['source']; ?></label>
                                    <div>
                                        <input type="text" id="<?= $translation['id'];?>" name="<?= $translation['id']; ?>" value="<?= $translation['message']; ?>" class="form-control">
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </section>
                <?php endforeach; ?>

                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="form-group" id="updateBtnWrap">
                            <div class="text-center">
                                <?= Html::submitButton(Yii::t('system', 'Update'), ['class' => 'btn btn-primary', 'id' => 'addProductBtn']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        <?php else: ?>
            <div class="noData">
                <p class="text-center"><?= Yii::t("language", "No translations found");?></p>
            </div>
        <?php endif; ?>
    </div>
</div>