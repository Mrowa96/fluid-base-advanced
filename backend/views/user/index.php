<?php
    use yii\helpers\Html;
    use common\utilities\GridView;
    use fluid\elements\TabbedView;

    $this->title = Yii::t("navigation", "Users");
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-content">
        <?php
            array_unshift($gridViewColumns, ['class' => 'yii\grid\SerialColumn']);
            $gridViewColumns[] = [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function($url){
                        return '<a href="'.$url.'" title="'.Yii::t("system","View").'" class="action default"></a>';
                    },
                    'update' => function($url){
                        return '<a href="'.$url.'" title="'.Yii::t("system","Edit").'" class="action"></a>';
                    },
                    'delete' => function($url, $model){
                        if($model->deletable){
                            return '<a href="'.$url.'" title="'.Yii::t("system","Delete").'" class="action confirm"></a>';
                        }
                    },
                ]
            ];

            $tabbedView = new TabbedView();
            $tabbedView->addControl(Yii::t("user","All users"), "all")->addSection(GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridViewColumns,
                'layout'=>"{items}\n{pager}",]), "all")
                ->addControl(Html::a("+ " . Yii::t("user","Add user"), ['create'], ['class' => 'controlBtn controlBtn-success']))
                ->render();
        ?>
    </div>
</div>
