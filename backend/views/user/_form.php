<?php
    use common\models\Language;
    use common\models\Settings;
    use common\models\Settings\System;
    use yii\helpers\Html;
    use common\utilities\ActiveForm;
?>
<div class="user-form">
    <?php $form = ActiveForm::begin([
        'id' => 'user-form',
        'enableAjaxValidation' => true,
    ]); ?>
    <?= $form->field($model, 'username') ?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'password_repeat')->passwordInput() ?>
    <?php if($model->isNewRecord): ?>
        <?= $form->field($model, 'role')
            ->dropDownList(
                $roles,
                ['prompt'=> Yii::t("system", "Choose account type")]
            );
        ?>
    <?php endif; ?>
    <?php if(Settings::getOne('language_level', System::MODULE) === "user"): ?>
        <?= $form->field($model, 'language')
            ->dropDownList(
                Language::prepareForForm("backend"),
                ['prompt'=> Yii::t("settings", "Choose language")]
            );
        ?>
    <?php endif; ?>
    <div class="form-group" id="avatarsBlock">
        <?= $form->field($model, 'avatar')->hiddenInput() ?>

        <ul class="avatars-list">
            <?php if(!empty($avatars)): ?>
                <?php foreach($avatars AS $avatar): ?>
                    <li>
                        <img src="<?= Yii::getAlias("@dataLink") . '/' . $avatar; ?>" alt="Avatar" class="img-responsive" data-url="<?= $avatar; ?>">
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>

        <script>
            window.addEventListener("load", function(){
                FI.Lib.selectAvatar({
                    avatars: document.querySelector("#avatarsBlock").querySelectorAll(".avatars-list")[0],
                    input: document.querySelector("#avatarsBlock").getElementsByTagName("input")[0]
                });
            }, false);
        </script>

    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t("system", "Create") : Yii::t("system", "Update"), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
