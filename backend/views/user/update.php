<?php
    $this->title = Yii::t("user", "Update user");
    $this->params['breadcrumbs'][] = ['label' => Yii::t("navigation", "Users"), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-form">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
            'roles' => $roles,
            'avatars' => $avatars,
        ]) ?>
    </div>
</div>
