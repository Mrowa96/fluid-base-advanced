<?php
    use yii\helpers\Html;
    use common\utilities\ActiveForm;
    use fluid\elements\TabbedView;

    $this->title = Yii::t("navigation", "Settings");
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings box box-form">
    <div class="box-content">
        <div class="settings-form">
            <?php $form = ActiveForm::begin([
                'id' => 'settings-form',
            ]); ?>
            <?php
            $tabbedView = new TabbedView();
            $tabbedView->addControls([
                [
                    'data' => Yii::t("settings", "System"),
                    'section' => 'system'
                ],
                [
                    'data' => Yii::t("settings", "Information"),
                    'section' => 'information'
                ],
                [
                    'data' => Yii::t("settings", "SMTP"),
                    'section' => 'smtp'
                ],
                [
                    'data' => Yii::t("settings", "SMS"),
                    'section' => 'sms'
                ],
            ])->addSections([
                [
                    'data' => $this->render("_system", ['form' => $form, 'model' => $system, 'themes' => $themes, 'languages' => $languages]),
                    'section' => 'system'
                ],
                [
                    'data' => $this->render("_information", ['form' => $form, 'model' => $information]),
                    'section' => 'information'
                ],
                [
                    'data' => $this->render("_smtp", ['form' => $form, 'model' => $smtp]),
                    'section' => 'smtp'
                ],
                [
                    'data' => $this->render("_sms", ['form' => $form, 'model' => $sms]),
                    'section' => 'sms'
                ],
                ])->render();
            ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t("system", "Update"), ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>