<div class="smtp">
    <?= $form->field($model, 'host'); ?>
    <?= $form->field($model, 'login'); ?>
    <?= $form->field($model, 'password')->passwordInput(); ?>
    <?= $form->field($model, 'encryption'); ?>
    <?= $form->field($model, 'port'); ?>
</div>