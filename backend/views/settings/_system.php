<?php
    use common\models\Settings;
    use common\models\Settings\System;
?>
<div class="system">
    <?= $form->field($model, 'site_title'); ?>
    <?= $form->field($model, 'footer_text'); ?>
    <?= $form->field($model, 'theme')->dropDownList(
        $themes,
        ['prompt'=>'Wybierz motyw']
    ); ?>
    <div class="form-group">
        <div class="form-message form-message-primary">
            <?= Yii::t("settings", "Language level is only applied for panel."); ?>
        </div>
    </div>
    <?= $form->field($model, 'language_level')->dropDownList(System::getLanguageLevel(), [
        'prompt' => Yii::t("settings", "Choose language level")
    ]) ?>
    <?php if(Settings::getOne('language_level', System::MODULE) === "global"): ?>
        <?= $form->field($model, 'language')->dropDownList($languages, [
            'prompt' => Yii::t("settings", "Choose language")
        ]) ?>
    <?php endif; ?>

    <?= $form->field($model, 'file_manager')->checkbox(); ?>
</div>