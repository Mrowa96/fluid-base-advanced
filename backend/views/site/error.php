<?php
    use yii\helpers\Html;

    $this->title = $name;
?>
<div class="error">
    <?= nl2br(Html::encode($message)) ?>
</div>

