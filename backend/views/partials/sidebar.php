<?php
    use yii\helpers\Url;
    $user = Yii::$app->user->identity;
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <a href="<?= Url::toRoute(["user/view", 'id' => Yii::$app->user->identity->id]); ?>">
                    <img src="<?= $user->avatar;?>" class="img-circle" alt="User Image">
                </a>
            </div>
            <div class="pull-left info">
                    <p>
                        <a href="<?= Url::toRoute(["user/view", 'id' => Yii::$app->user->identity->id]); ?>">
                            <?= $user->username;?>
                        </a>
                    </p>
                    <a href="mailto:<?= $user->email;?>">
                        <i class="fa fa-envelope text-success"></i>
                        <?= $user->email; ?>
                    </a>
            </div>
        </div>
        
        <?php if(isset($this->params['sidebarMenu'])): ?>
            <?= $this->params['sidebarMenu']->render(); ?>
        <?php endif; ?>
    </section>
</aside>