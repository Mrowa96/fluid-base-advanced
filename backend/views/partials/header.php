<?php
    use common\models\Language;
    use common\models\Settings;
    use common\models\Settings\System;
    use yii\helpers\Url;

    $user = Yii::$app->user->identity;
    $languages = Language::prepareForForm("backend");
?>

<header class="main-header">

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle hidden-lg hidden-md" data-toggle="offcanvas" role="button">
            <span class="sr-only"><?= Yii::t("system", "Toggle navigation"); ?></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $user->avatar; ?>" class="user-image" alt="User Image">
                        <span class="hidden-xs">
                            <?= $user->username;?>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated-dropdown-menu">
                        <li class="user-header">
                            <img src="<?= $user->avatar; ?>" class="img-circle" alt="User Image">
                            <p>
                                <?= $user->username; ?>
                                <small><?= Yii::t("system", "Joined"); ?> <?= $user->created_at; ?></small>
                            </p>
                        </li>

                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?= Url::toRoute(["user/view", 'id' => Yii::$app->user->identity->id]); ?>" class="btn btn-default btn-flat"><?= Yii::t("system", "Account"); ?></a>
                            </div>
                            <div class="pull-right">
                                <a href="<?= Url::toRoute("/logout"); ?>" class="btn btn-default btn-flat"><?= Yii::t("system", "Logout"); ?></a>
                            </div>
                        </li>
                    </ul>
                </li>
                <?php if(Settings::getOne('language_level', System::MODULE) === "cookie"): ?>
                    <li class="dropdown lang lang-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-language"></i>
                        </a>
                        <ul class="dropdown-menu animated-dropdown-menu list">
                            <?php foreach($languages AS $symbol => $language):?>
                                <li><a href="<?= Url::toRoute(['site/change-lang', 'symbol' => $symbol]);?>"><?= $language; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                <?php endif; ?>
                <li>
                    <a href="<?= Yii::getAlias("@frontendLink");?>">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
            </ul>
        </div>

    </nav>
</header>