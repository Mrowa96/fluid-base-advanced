<?php
    use common\models\Settings;
    use common\models\Settings\System;

/** @var Settings $footerText */
$footerText = Settings::getOne('footer_text', System::MODULE, false);
?>

<footer class="main-footer">
    <span><?= ($footerText->value) ? $footerText->value : $footerText->default; ?></span>
</footer>