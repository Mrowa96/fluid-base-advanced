<?php
    use backend\assets\AppAsset;
    use common\utilities\Message;
    use yii\helpers\Html;

    AppAsset::register($this);
?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>

    <html lang="<?= Yii::$app->language ?>">
        <head>
            <meta charset="<?= Yii::$app->charset ?>">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
            <?= Html::csrfMetaTags() ?>

            <title><?= Html::encode($this->title) ?></title>
            <?php $this->head() ?>
        </head>
        <body class="hold-transition skin-purple-light">
            <?php $this->beginBody() ?>
                <div class="wrapper auth">
                    <header class="header">
                        <a href="<?= Yii::getAlias("@frontendLink"); ?>" style="color:#666"> <i class="fa fa-home"></i></a>
                        <span class="title">Logowanie</span>
                    </header>

                    <div class="content">
                        <?php
                        if(isset($content)){
                            echo $content;
                        }
                        ?>
                    </div>

                    <?= Message::widget(['view' => $this]); ?>
                    <?= $this->render("@app/views/partials/footer"); ?>
                </div>
            <?php $this->endBody() ?>
        </body>
    </html>
<?php $this->endPage() ?>


