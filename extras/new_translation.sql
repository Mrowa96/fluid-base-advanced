-- Backend translation

INSERT INTO
  fba_i18n_source_backend (category, message)
VALUES(
	'category',
  'message'
);

INSERT INTO
  fba_i18n_message (source_id, i18n_id, `translation`)
VALUES(
	LAST_INSERT_ID(),
  1, -- 1 - polski, 2 - angielski
  'translation'
);

-- Frontend translation

INSERT INTO
  fba_i18n_source_frontend (category, message)
VALUES(
  'category',
  'message'
);

INSERT INTO
  fba_i18n_message (source_id, i18n_id, `translation`)
VALUES(
	LAST_INSERT_ID(),
  1, -- 3 - polski
  'translation'
);