Fluid Base Advanced
============================

DIRECTORY STRUCTURE
-------------------

```
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    forms/               contains models which are forms
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    searches/            contains models which are searches
    utilities/           contains utility classes
    var/                 contains temporary data
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
common
    config/              contains shared configurations
    forms/               contains models which are forms
    models/              contains model classes used in both backend and frontend
    utilities/           contains utility classes
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
extras/                  contains extra data like example vhost or db example        
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    forms/               contains models which are forms
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    utilities/           contains utility classes
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
initializer/             contains initialization classes and configuration files
vendor/                  contains dependent 3rd-party packages

```

REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.5.0.

INSTALLATION
------------
If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following commands:

~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.1.1"
php composer.phar install

~~~


CONFIGURATION
-------------
#### Vhost

Create your vhost file, it can be based on example vhost, which you can find in extras directory.

#### Database

Set up login, password and a host in config/db.php (if not exists, just copy db-example.php and rename it), for example

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=fluid-base-advanced',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

ACCESS
------
#### Admin account
Login: admin
Password: q@Werty

#### Worker account
Login: worker
Password: worker