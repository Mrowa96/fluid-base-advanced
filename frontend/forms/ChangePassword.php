<?php
    namespace frontend\forms;

    use Yii;
    use yii\base\Model;
    use common\models\User;

    class ChangePassword extends Model{

        public $currentPassword;
        public $newPassword;
        public $repeatNewPassword;

        public function rules()
        {
            return [
                [['currentPassword', 'newPassword', 'repeatNewPassword'], 'filter', 'filter' => 'trim'],
                [['currentPassword', 'newPassword', 'repeatNewPassword'], 'required'],
                ['newPassword', 'comparePasswords'],
                ['currentPassword', 'currentPasswordMatch'],
            ];
        }
        public function attributeLabels()
        {
            return [
                'currentPassword' => Yii::t('client', 'Current password'),
                'newPassword' => Yii::t('client', 'New password'),
                'repeatNewPassword' => Yii::t('client', 'Repeat new password'),
            ];
        }

        public function comparePasswords()
        {
            if($this->newPassword !== $this->repeatNewPassword){
                $this->addError($this->newPassword , Yii::t("client", "Passwords does not match"));
                $this->addError($this->repeatNewPassword, Yii::t("client", "Passwords does not match"));
            }
        }
        public function currentPasswordMatch($attribute)
        {
            /**
             * @var User $user
             */
            $user = Yii::$app->getUser()->getIdentity();

            if(!$user->validatePassword($this->currentPassword)){
                $this->addError($attribute, Yii::t("client", "Current password is wrong"));
            }
        }

        public function changePassword()
        {
            try{
                /**
                 * @var User $user
                 */
                $user = Yii::$app->getUser()->getIdentity();

                print_r($user->hashPassword($this->newPassword));die();
                $user->setPassword($this->newPassword);

                return $user->save();
            }
            catch(\Exception $ex){
                $this->addError($ex->getMessage());
            }
        }
    }
