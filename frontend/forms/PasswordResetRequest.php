<?php
    namespace frontend\forms;

    use Yii;
    use yii\base\Model;
    use common\models\User;
    use yii\helpers\Url;
    use backend\models\MailMessage;
    use common\models\Settings;
    use common\utilities\Mailer;

    class PasswordResetRequest extends Model
    {
        public $email;

        public function rules()
        {
            return [
                ['email', 'filter', 'filter' => 'trim'],
                ['email', 'required'],
                ['email', 'email'],
                ['email', 'exist',
                    'targetClass' => '\common\models\User',
                    'filter' => ['status' => User::STATUS_ACTIVE],
                    'message' => Yii::t("app", 'There is no user with such email.')
                ],
            ];
        }

        public function sendEmail()
        {
            /**
             * @var User $user
             * @var Mailer $mailer
             * @var MailMessage $message
             */
            $mailer = Yii::$app->mailer;
            $user = User::findOne(['status' => User::STATUS_ACTIVE, 'email' => $this->email]);
            $message = MailMessage::find()->where(['name' => 'password_reset_request'])->one();

            if (!$user || !$message) {
                return false;
            }

            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }

            if (!$user->save()) {
                return false;
            }

            $message->replaceTag(
                "reset_password_url",
                Yii::getAlias("@frontendLink") . Url::toRoute(['site/reset-password']) . '?token=' . $user->password_reset_token
            );

            return $mailer->compose()
                ->setFrom([$mailer->getHost() => Settings::getOne('site_title', Settings\System::MODULE) . ' robot'])
                ->setTo($this->email)
                ->setSubject($message->title)
                ->setHtmlBody($message->content)
                ->send();
        }
    }
