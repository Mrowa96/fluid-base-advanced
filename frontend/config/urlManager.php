<?php
    return [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'baseUrl' => Yii::getAlias("@frontendLink"),
        'rules' => [
            'login' => 'site/login',
            'signup' => 'site/signup',
            'logout' => 'site/logout',
            'request-password-reset' => 'site/request-password-reset',
            'reset-password' => 'site/reset-password',
        ],
        'suffix' => '.html'
    ];