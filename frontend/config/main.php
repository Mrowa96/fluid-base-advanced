<?php
    return [
        'id' => 'fba-front',
        'language' => 'pl_PL',
        'basePath' => dirname(__DIR__),
        'bootstrap' => ['log'],
        'controllerNamespace' => 'frontend\controllers',
        'components' => [
            'log' => [
                'traceLevel' => YII_DEBUG ? 3 : 0,
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
            'errorHandler' => [
                'errorAction' => 'site/error',
            ],
            'urlManager' => require_once 'urlManager.php',
            'urlManagerBackend' => array_merge(
                require_once dirname(__DIR__) . '/../backend/config/urlManager.php',
                ['class' => 'yii\web\urlManager']
            ),
            'i18n' => [
                'translations' => [
                    '*' => [
                        'class' => 'frontend\utilities\i18n',
                        'forceTranslation' => true,
                    ],
                ],
            ],
        ],
        'params' => [
            'user.passwordResetTokenExpire' => 3600,
        ]
    ];
