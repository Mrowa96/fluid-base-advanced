<?php
    namespace frontend\controllers;

    use Yii;
    use yii\base\InvalidParamException;
    use yii\web\BadRequestHttpException;
    use yii\filters\AccessControl;
    use frontend\forms\PasswordResetRequest;
    use frontend\forms\ResetPassword;
    use frontend\forms\Signup;
    use frontend\forms\Contact;
    use common\forms\Login;

    class SiteController extends BaseController{
        public function behaviors()
        {
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'only' => ['logout', 'signup'],
                    'rules' => [
                        [
                            'actions' => ['signup'],
                            'allow' => true,
                            'roles' => ['?'],
                        ],
                        [
                            'actions' => ['logout'],
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
            ];
        }
        public function actions()
        {
            return [
                'error' => [
                    'class' => 'yii\web\ErrorAction',
                ],
            ];
        }

        public function actionIndex()
        {
            return $this->render('index');
        }
        public function actionContact()
        {
            $model = new Contact();

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                    Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
                }
                else {
                    Yii::$app->session->setFlash('error', 'There was an error sending email.');
                }

                return $this->refresh();
            }
            else {
                return $this->render('contact', [
                    'model' => $model,
                ]);
            }
        }

        public function actionLogin()
        {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }

            $model = new Login();

            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            else {
                return $this->render('login', [
                    'model' => $model,
                ]);
            }
        }
        public function actionLogout()
        {
            Yii::$app->user->logout();

            return $this->goHome();
        }
        public function actionSignup()
        {
            $model = new Signup();

            if ($model->load(Yii::$app->request->post())) {
                if ($user = $model->signup()) {
                    if (Yii::$app->getUser()->login($user)) {
                        return $this->goHome();
                    }
                }
            }

            return $this->render('signup', [
                'model' => $model,
            ]);
        }
        public function actionRequestPasswordReset()
        {
            $model = new PasswordResetRequest();

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if ($model->sendEmail()) {
                    Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                    return $this->goHome();
                }
                else {
                    Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
                }
            }

            return $this->render('requestPasswordResetToken', [
                'model' => $model,
            ]);
        }
        public function actionResetPassword($token)
        {
            try {
                $model = new ResetPassword($token);
            }
            catch (InvalidParamException $e) {
                throw new BadRequestHttpException($e->getMessage());
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
                Yii::$app->session->setFlash('success', 'New password was saved.');

                return $this->goHome();
            }

            return $this->render('resetPassword', [
                'model' => $model,
            ]);
        }
    }
