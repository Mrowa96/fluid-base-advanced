<?php
    namespace frontend\assets;

    use yii\web\AssetBundle;

    class AppAsset extends AssetBundle
    {
        public $basePath = '@webroot';
        public $baseUrl = '@web';
        public $css = [
            'styles/_font-awesome/font-awesome.min.css',
            'styles/_project/app.css',
        ];
        public $js = [
        ];
        public $depends = [
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapAsset',
        ];
    }
