<?php
    namespace frontend\models;

    use Yii;
    use yii\db\ActiveRecord;
    use common\models\LanguageMessage;

    /**
     * This is the model class for table "{{%i18n_source_frontend}}".
     *
     * @property integer $id
     * @property string $category
     * @property string $message
     *
     * @property LanguageMessage[] $i18nMessages
    */
    class LanguageSource extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%i18n_source_frontend}}';
        }

        public function rules()
        {
            return [
                [['message'], 'string'],
                [['category'], 'string', 'max' => 255],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('language_source', 'ID'),
                'category' => Yii::t('language_source', 'Category'),
                'message' => Yii::t('language_source', 'Message'),
            ];
        }

        public function getMessages()
        {
            return $this->hasMany(LanguageMessage::className(), ['id' => 'id']);
        }
    }
